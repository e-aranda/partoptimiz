#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Install script for toptimiz3D (parallel version)
"""
import sys
if sys.version_info < (3,0):
    print("Wrong Python version. You need Python 3")
    exit(1)
    
import importlib
import argparse
import os
import shutil
import tempfile
import platform
import subprocess

def printred(s):
    print('\033[91m' + s + '\033[0m')

def printgreen(s):
    print('\033[92m' + s + '\033[0m')

def printyellow(s):
    print('\033[93m' + s + '\033[0m')

    
    
sourcemake = """
{var}


FC = mpicxx

FFLAGS = -O3 -std=c++11 $(CPPFLAGS)
OTHER =  $(LDFLAGS) -lmfem -lHYPRE -lmetis {umfs} {petsc}

DIR=$(BASEDIR)/src
BIN=$(BASEDIR)/bin

%.o : %.cpp
	$(FC) $(FFLAGS) -c -o $@ $<

nlopt : $(DIR)/MAIN.o $(DIR)/Parfunctions.o $(DIR)/FUN_NLOPT.o $(ADD0) $(ADD1)
	$(FC) $(FFLAGS) $^ -o $(BIN)/program $(OTHER) -lnlopt	

oc : $(DIR)/MAIN.o $(DIR)/Parfunctions.o $(DIR)/FUN_OC.o $(ADD0) $(ADD1)
	$(FC) $(FFLAGS) $^ -o $(BIN)/program $(OTHER)		

hiop : $(DIR)/MAIN.o $(DIR)/Parfunctions.o $(DIR)/FUN_HIOP.o $(ADD0) $(ADD1)
	$(FC) $(FFLAGS) $^ -o $(BIN)/program $(OTHER) -lhiop

mma : $(DIR)/MAIN.o $(DIR)/Parfunctions.o $(DIR)/FUN_PETSC.o $(DIR)/MMA.o $(ADD0) $(ADD1)
	$(FC) $(FFLAGS) $^ -o $(BIN)/program $(OTHER)		

clean :
	rm -f *.o
"""    

# arguments (directory for installation)

parser = argparse.ArgumentParser()
parser.add_argument('--dir', nargs='?', default=None, 
                    help = "Optional installation directory")
parser.add_argument('--ld', nargs='+', default=None, 
                    help = 'LDDFLAGS for configure: example --ld="-L/path/to/1 -L/path/to/2"')

parser.add_argument('--cpp', nargs='+', default=None, 
                    help = 'CPPFLAGS for configure: example --cpp="-I/path/to/1 -I/path/to/2"')

#parser.add_argument('--suitedir', nargs='+', default=None, 
#                    help = 'SuiteSparse dir for compilation of MFEM: example --suitedir=/path/to/SuiteSparse')

parser.add_argument('--suitesparse', default=False, action="store_true",
                    help = 'Tell configuration script that MFEM is compiled with SuiteSparse')

parser.add_argument('--petsc', default=False, action="store_true",
                    help = 'Tell configuration script that MFEM is compiled with PETSC')

parser.add_argument('--petsclib', nargs='?', default=None, 
                    help = 'Directory for petsc lib')

parser.add_argument('--nolauncher', default=False, action="store_true",
                    help = 'No ask for desktop launcher')

parser.add_argument('--yes', default=False, action="store_true",
                    help = 'Yes answer to questions')

args = parser.parse_args()


############################ 
# check platform
if platform.system().lower() != "linux" and platform.system().lower() != "darwin":
    print("Sorry, platform " + platform.system() + " not supported")
    exit(1)
else:
    print("Platform: " + platform.system()+'\n')


############################

############################    
# Check Python modules installed
    
############################    
# Check modules installed
install = True
    
try:
    import vtk
    printgreen("vtk is installed in version " + vtk.vtkVersion.GetVTKVersion())
    if int(vtk.vtkVersion.GetVTKVersion().split('.')[0]) < 6:
        printred("vtk version is not correct. Minimum version required is 6")
        install = False
except ImportError:
    printred("VTK module is required")
    install = False
try:
    import wx
    printgreen("WxPython is installed in version " + wx.version().split()[0])
    if int(wx.version()[0]) != 4:
        printred("WxPython version is not correct. Version required: 4")
        install = False
except ImportError:
    printred("WxPython module is required")
    install = False
       
for x in ["numpy","pexpect","requests","dictdiffer"]:
    try:
        importlib.import_module(x)
        printgreen(x + " is installed in version " + importlib.import_module(x).__version__)
    except:
        print(x + " module is required")
        install = False        

if not install:
    printred("\nToptimiz3D cannot be installed")
    printred("Check problems above")
    exit(1)
    

############################    
# Base folder 
    
initfolder = os.path.dirname(os.path.abspath(__file__))    
print('')
os.chdir(initfolder)


############################################
# preconfiguration: processing arguments
############################################
lflags = cflags = ''

os.chdir(os.path.join(initfolder,'config','preconf'))
if args.ld:
    lflags = 'LDFLAGS="' + args.ld[0] + '"'
if args.cpp:
    cflags = 'CPPFLAGS="' + args.cpp[0] + '"'    


if args.dir:
    folder = args.dir
else:
    folder =  os.path.join(os.path.expanduser('~'),'toptimiz3D')
mypath = 'MYPATH=' + os.path.join(folder,'bin')

command = './configure ' + lflags + ' ' + cflags + ' ' + mypath 
print(command)
err = subprocess.call(command,shell=True) 

if err:
    printred("Pre-Configuration Error! Check config.log")
    exit(1)

# preconfiguration results
results = []
with open("config.log",'r') as f:
    for line in f:
        if 'topt_' in line:
            results.append(line.strip())               
        
for x in results[:]:
    if 'yes' in x:
        results.remove(x)

missing_lib = []     
missing_head = []   
missing_prog = []

for x in results:
    if 'lib' in x:
        missing_lib.append(x.split('=')[0].split('_')[-1])
    if 'header' in x:
        missing_head.append(x.split('=')[0].split('_')[-1])
        continue
    if 'prog' in x:
        missing_prog.append(x.split('=')[0].split('_')[-1])
     
# checking what to install        
b_glvis = True
bgl = 0

# checking for glvis
for x in missing_prog:
    if 'glvis' in x:
        bgl += 1
    elif  'other' in x:
        bgl += 1
if bgl == 2:
    b_glvis = False        

b_mfem = True
b_hypre = True
b_metis = True
b_nlopt = True

for x in missing_lib:
    if 'mfem' in x:
        b_mfem = False
    if 'HYPRE' in x:
        b_hypre = False
    if 'nlopt' in x:
        b_nlopt = False
    if 'metis' in x:
        b_metis = False
b_eigen = True
for x in missing_head:
    if 'eigen' in x:
        b_eigen = False
        
errormessage = ''
        
if b_metis and 'metis' in x:
    errormessage += '\nError: METIS library is installed but headers are not found. Check installation.'
if b_hypre and 'HYPRE' in x:
    errormessage +='\nError: HYPRE library is installed but headers are not found. Check installation.'
if b_mfem and 'mfem' in x:
    errormessage +='\nError: MFEM library is installed but headers are not found or there is an error.'
if b_nlopt and 'nlopt' in x:
    errormessage += '\nError: NLOPT library is installed but headers are not found. Check installation. Aborting ...'
    
printgreen('\n\n\nSummary:\n******************************')
if b_mfem:
    print('MFEM is installed')
else:
    printred('MFEM is not installed')
if b_nlopt:
    print('NLOPT is installed')
else:    
    printred('NLOPT is not installed')        
if b_eigen:
    print('EIGEN is installed')
else:    
    printred('EIGEN is not installed')    
if b_glvis:
    print('GLVIS is installed')
else:    
    printred('GLVIS is not installed')
if b_hypre:
    print('HYPRE is installed')
else:
    printred('HYPRE is not installed')
if b_metis:
    print('METIS is installed')
else:
    printred('METIS is not installed')

if not (b_nlopt and b_eigen and b_glvis and b_hypre and b_metis and b_mfem):
    exit(1)
    
if errormessage:
    print(errormessage)
    exit(1)


#if b_gsl:
#    print('GSL is installed')
#else:    
#    print('GSL is going to be installed')    
printgreen('******************************\n\n\n')

# asking = True
# if b_mfem and not b_glvis:
#     while asking:
#         srcmfem = input("Warning: MFEM is installed but GLVIS is not. We need to know the MFEM \
#           installation folder (Enter X to abort)\n")
#         if srcmfem == "X" or srcmfem == "x":
#             exit(0)
#         elif not os.path.exists(srcmfem):
#             print("Directory does not exist. Try again")
#         else:
#             asking = False
    

########################################
# base folders
#######################################


 
if not os.path.exists(folder):
    try:
        print('Destination folder: '+folder)    
        os.makedirs(folder)
    except OSError as e:
        print(e.args)
        printred("Installation directory can not be created. Please run again this script with a valid destination folder")
        exit(1)
else:
    print("Attention! installation directory [" + folder + "] exists. Content could be overwritten.")
    
if args.yes:
    asking = False
else:
    asking = True
while asking:
    answer = input('Continue? [Y/n] ')
    if answer in 'yY':
        asking = False
    elif answer in 'nN':
        asking = False
        exit(0)
    else:
        print("Please answer 'y' or 'n'")


os.chdir(initfolder)
# Create folder for .py files
codefolder = os.path.join(folder,'code')
if not os.path.exists(codefolder):
    try:
        os.makedirs(codefolder)
    except OSError as e:
        raise Exception(e.args)

# Copy VERSION and LICENSE files
for x in ('LICENSE','VERSION','README.md'):        
    dst = os.path.join(folder,x)
    print("Copying " + x)
    try:
        shutil.copyfile(x,dst)
    except IOError as e:
        raise Exception(e.args)

# Copying Python files and icons
print("Coping Python code ... \n")
for x in os.listdir('code'):
    src = os.path.join('code',x)
    dst = os.path.join(codefolder,x)
    if not os.path.isdir(src):
        print("Copying " + x + " ...")
        try:
            shutil.copyfile(src,dst)
        except IOError as e:
            raise Exception(e.args)
    else:
        try:
            os.makedirs(dst)
            print("Directory " + x + " created")
        except OSError as e:
            print("Enter in " + x + " ...")
        for y in os.listdir(src):
            print("  Copying " + y + " ...")
            ini = os.path.join(src,y)
            dst = os.path.join(folder,ini)
            try:
                shutil.copyfile(ini,dst)
            except IOError as e:
                raise Exception(e.args)

# Example folder
print("Copying Examples ...\n")        
examplefolder = os.path.join(folder,'examples')
if not os.path.exists(examplefolder):
    try:
        os.makedirs(examplefolder)
        print("Directory examples created")
    except OSError as e:
        raise Exception(e.args)

# Copying examples
for x in os.listdir('examples'):
    src = os.path.join('examples',x)
    dst = os.path.join(examplefolder,x)
    if not os.path.isdir(src):
        print("   Copying " + x + " ...")
        try:
            shutil.copyfile(src,dst)
        except IOError as e:
            raise Exception(e.args)

# C++ folder
print("Copying C++ files ... \n")        
cppfolder = os.path.join(folder,'src')
if not os.path.exists(cppfolder):
    try:
        os.makedirs(cppfolder)
        print("Directory src created")
    except OSError as e:
        raise Exception(e.args)

# Copying C++ files
for x in os.listdir('src'):
    src = os.path.join('src',x)
    dst = os.path.join(cppfolder,x)
    if not os.path.isdir(src):
        print("   Copying " + x + " ...")
        try:
            shutil.copyfile(src,dst)
        except IOError as e:
            raise Exception(e.args)            

# Manual folder
"""
print("Copying manual file ... \n")        
docfolder = os.path.join(folder,'doc')
if not os.path.exists(docfolder):
    try:
        os.makedirs(docfolder)
        print("Directory doc created")
    except OSError as e:
        raise Exception(e.args)

# Copying manual file
for x in os.listdir('doc'):
    src = os.path.join('doc',x)
    dst = os.path.join(docfolder,x)
    if not os.path.isdir(src):
        print("   Copying " + x + " ...")
        try:
            shutil.copyfile(src,dst)
        except IOError as e:
            raise Exception(e.args)            
"""
print('')

carpetas = ['bin']

# bin lib include folders
for x in carpetas:
    newfolder = os.path.join(folder,x)
    if not os.path.exists(newfolder):
        try:
            os.makedirs(newfolder)
            print("Directory",x,"created")
        except OSError as e:
            raise Exception(e.args)
############################################
    

# Platform adjustment
if platform.system().lower() == "darwin":
#    confipopt = ' --with-blas="-framework Accelerate"'
    shared = "-DBUILD_SHARED_LIBS=OFF" 
else:
#    confipopt = ''
    shared = ''


# petsc
cxxflag = ''
if args.petsc:
    if args.petsclib:
         petscstr = '-lpetsc -Wl,-rpath=' + args.petsclib.replace('-L','')
         cxxflag = ' CXXFLAGS="-Wl,-rpath=' + args.petsclib.replace('-L','') +'"'
    else:
        printred("Installation with petsc has been chosen but there is no Petsc Lib Directory")
        exit(1)
else:
    petscstr = ''
    
    
############################################        
# Compilation of meshtovtk
#if b_ipopt:
#    extra += ' --enable-ipopt'
    
command = './configure --prefix=' + folder + ' ' + lflags + ' ' + cflags + cxxflag +  ' ' + mypath 
print(command)
os.chdir(os.path.join(initfolder,'config'))
err = subprocess.call(command,shell=True) 



if err:
    printred("Configuration Error! Check config.log")
    exit(1)
else:
    er = subprocess.call("make",shell=True)
    if er:
        printred("Compilation Error!")
        exit(1)
    else:
        subprocess.call("make install",shell=True)
        print('Meshtovtk installed')



############################################
    
############################################     
# Creating makefile        

# correct path for eigen        
with open("preconf/config.log",'r') as f:
    for line in f:
        if 'ac_cv_header' in line and 'yes' in line and 'eigen' in line.lower():
            eig = line
            break
    else:
        print("Error: eigen headers are not found")
        exit(1)
eigpath = eig.split('header')[-1].split('=')[0].split('_')
eig = os.path.join(*eigpath)

with open(os.path.join(folder,'src','ParConic.hpp'),'r') as f:
    functions = f.read()
        
with open(os.path.join(folder,'src','ParConic.hpp'),'w') as f:
    f.write(functions.replace('???',eig))

    
ldcam = subprocess.getoutput("grep LDFLAGS Makefile")
cpcam = subprocess.getoutput("grep CPPFLAGS Makefile")

varss = ldcam + '\n' + cpcam + '\n' + 'BASEDIR = ' + folder 

if args.suitesparse:
    umf = '-lklu -lumfpack -lblas -llapack'
else:
    umf = '-lblas -llapack'

       

makef = os.path.join(folder,'src','makefile')
with open(makef,'w') as f:
    f.write(sourcemake.format(var=varss,umfs=umf,petsc=petscstr))

        
#################################################


#################################################

os.chdir(initfolder)

# Creating desktop launcher
if args.nolauncher:
    asking = False
else:
    asking = True
while asking:
    if args.yes:
        answer = 'y'
    else:
        answer = input('Do you want to create a Desktop Launcher? [Y/n] ')

    if answer == '' or answer in 'yY':
        if platform.system().lower() == "linux":
            print("Creating Desktop Launcher for Linux")
            # Determine the name of Desktop folder
            try:
                desktopfolder = subprocess.check_output(['xdg-user-dir', 'DESKTOP'])[:-1]
            except:
                desktopfolder = os.path.join(os.path.expanduser('~'),'Desktop')

            lanzador = \
"""
[Desktop Entry]
Name=Toptimiz3d
Exec=bash -i {pathtolauncher}
Terminal=false
StartupNotify=true
Type=Application
Icon={icon}
"""

            launcher = \
"""
#!/usr/bin/bash
cd {home}
{path}
"""
            
            # create executable desktop file
            appfile = os.path.join(desktopfolder.decode('utf-8'),'Toptimiz3D.desktop')
            appcommand = subprocess.getoutput('which python3') + ' ' + os.path.join(folder,'code','toptimiz3d.py')
            applauncher = os.path.join(folder,'code','icons','launcher.sh')
            with open(applauncher,'w') as f:
                f.write(launcher.format(home=os.path.expanduser('~'),path=appcommand))
            dst = os.path.join(folder,'code','icons','topicon.gif')
            
            with open(appfile,'w') as f:
                f.write(lanzador.format(pathtolauncher=applauncher,icon=dst))
            os.chmod(appfile,0o755)
            os.chmod(applauncher,0o755)

        elif platform.system().lower() == "darwin":
            print("Creating Desktop Launcher for MacOS")
            desktopfolder = os.path.join(os.path.expanduser('~'),'Desktop')
            shellscript = 'do shell script "{path} ; launchctl setenv PATH $PATH; cd ' + os.path.join(folder,'code') + ' ;  {command}"'

            appfile = os.path.join(desktopfolder,'Toptimiz3D.app')
            special = "$'" + appfile + r"/Icon\r'"
            setpath = 'PATH='+ subprocess.getoutput('which python')[:-7] +':$PATH '
            appcommand = 'pythonw ' + os.path.join(folder,'code','toptimiz3d.py')
            with open('shellscript.scpt','w') as f:
                f.write(shellscript.format(path=setpath,command=appcommand))
            os.system('osacompile -o ' + appfile + ' shellscript.scpt')
            # set icon to code folder
            icon = os.path.join('code','icons','topicon.gif')
            fileicon = os.path.join('code','icons','fileicon.sh')
            os.system('bash ' + fileicon + ' set ' + appfile + ' ' + icon)

        asking = False
    elif answer in 'nN' or answer == '':
        asking = False
    else:
        print("Please, answer y or n")



print("\nInstallation is done")

