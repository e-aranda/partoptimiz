
import wx
import os
import myhelpfiles
import wx.lib.scrolledpanel as scrolled

class ImgPanel(scrolled.ScrolledPanel):
    def __init__(self, parent,title):
        super(ImgPanel, self).__init__(parent, 
                                       style = wx.SUNKEN_BORDER)
        filepath = myhelpfiles.helpers.get(title,'dummy.png')
        CURRENT_DIR = os.path.dirname(__file__)
        helpfile = os.path.join(CURRENT_DIR, os.path.join('doctex',filepath))   
        Img = wx.Image(helpfile, wx.BITMAP_TYPE_PNG)        
        Image = wx.StaticBitmap(self)       
        Image.SetBitmap(wx.Bitmap(Img))

        self.imgSizer = wx.BoxSizer(wx.VERTICAL)        
        self.imgSizer.Add(Image, 1, wx.EXPAND)
        self.SetSizer(self.imgSizer)

        self.SetAutoLayout(1)
        self.SetupScrolling()    
        self.IsRectReady = False
        self.newRectPara=[0,0,0,0]

class  WinFrame(wx.Frame):
    """
    Create a frame to put a graphic with a scrollbar
    """
    def __init__(self, parent, title):
        """
        Create a new frame with an image
        given in filepath
        """
        super(WinFrame, self).__init__(parent, 
                                       title=title + " help", size = (480,640),
                                       style=wx.DEFAULT_FRAME_STYLE ^ wx.RESIZE_BORDER)

        self.imgPanel = ImgPanel(self,title)
        self.frameSizer = wx.BoxSizer(wx.HORIZONTAL)        
        self.frameSizer.Add(self.imgPanel, 1, wx.EXPAND)        
        self.SetAutoLayout(True)
        self.SetSizer(self.frameSizer)
        self.Layout()      

        self.Centre()
        self.Show(True)        

