"""
Created on Tue Jan 22 10:30:26 2019

@author: earanda
"""


import wx
import paneltitle


# ---------------------------------------------------------------------------   
class PanelSettings(wx.Panel):
    """
    Panel for create settings variables
    """
    def __init__(self,parent,padre,title):
        wx.Panel.__init__(self, parent, style=wx.BORDER_SUNKEN)
        m_sizer = wx.BoxSizer(wx.VERTICAL)
        self.padre = padre
        title = paneltitle.MyPanelTitle(self,title)
        m_sizer.Add(title,0, wx.TOP|wx.EXPAND , 1)
        
        paneles = []
        sizers = []
        static = []
        self.static_e = {}

        texto = wx.StaticText(self,-1,"Parameters for solving elasticity system",style=wx.ALIGN_CENTER_VERTICAL)
        m_sizer.Add(texto,0,wx.TOP|wx.EXPAND, 1)
        # creating panels
        for i,x in enumerate(["ITER","TOL"]):
            paneles.append(wx.Panel(self, style=wx.BORDER_SUNKEN))
            sizers.append(wx.BoxSizer(wx.HORIZONTAL))
            static.append(wx.StaticText(paneles[i],-1,x))
            self.static_e[x] = wx.TextCtrl(paneles[i],-1,str(self.padre.settings[x]),\
                                             style=wx.TE_PROCESS_ENTER|wx.TE_RICH)
            sizers[i].Add(static[i],1,wx.ALIGN_TOP|wx.ALIGN_CENTER_VERTICAL, 1)
            sizers[i].Add(self.static_e[x],1,wx.ALIGN_TOP|wx.ALIGN_CENTER_VERTICAL, 1)
            m_sizer.Add(paneles[i], 0, wx.TOP|wx.EXPAND , 1)
            paneles[i].SetSizer(sizers[i])
            # bindins
            self.Bind(wx.EVT_TEXT, lambda event, val=x: self.set_val(event,val), self.static_e[x])
        
        # panel for visualization
        panel = wx.Panel(self,-1)
        b_sizer = wx.BoxSizer(wx.VERTICAL)
        
        
        self.vis = wx.CheckBox(panel,-1,"Visualization (while running)")
        b_sizer.Add(self.vis, 0, wx.ALL|wx.EXPAND, 5)
        
        self.show = wx.CheckBox(panel,-1,"Show final result")
        b_sizer.Add(self.show, 0, wx.ALL|wx.EXPAND, 5)

        self.createvtk = wx.CheckBox(panel,-1,"Create parallel VTK's")
        b_sizer.Add(self.createvtk, 0, wx.ALL|wx.EXPAND, 5)
        
        panel_np = wx.Panel(self, style=wx.BORDER_SUNKEN)
        sizer_np = wx.BoxSizer(wx.HORIZONTAL)
        static_np = wx.StaticText(panel_np,-1,"Number of processors")
        static_e_np = wx.TextCtrl(panel_np,-1,str(self.padre.settings['nps']),
                                  style=wx.TE_PROCESS_ENTER|wx.TE_RICH)
        sizer_np.Add(static_np,1,wx.ALIGN_TOP|wx.ALIGN_CENTER_VERTICAL, 1)
        sizer_np.Add(static_e_np,1,wx.ALIGN_TOP|wx.ALIGN_CENTER_VERTICAL, 1)
        panel_np.SetSizer(sizer_np)

        m_sizer.Add(panel,0,wx.TOP|wx.EXPAND,1)    
        m_sizer.Add(panel_np, 0, wx.ALL|wx.EXPAND , 5)
        panel.SetSizer(b_sizer)

        # bindins
        self.Bind(wx.EVT_TEXT, lambda event, val='nps': self.set_val(event,val), static_e_np)
        
            
        # Main sizer
        self.SetSizer(m_sizer)
    
        self.Bind(wx.EVT_CHECKBOX, lambda event, var='visualization':
                  self.OnTick(event,var), self.vis)
        self.Bind(wx.EVT_CHECKBOX, lambda event, var='showfinalresult':
                  self.OnTick(event,var), self.show)
        self.Bind(wx.EVT_CHECKBOX, lambda event, var='createvtks':
                  self.OnTick(event,var), self.createvtk)

        
    def set_val(self,event,x):
        self.padre.settings[x] = event.EventObject.GetValue()
        
    def OnTick(self,event,var):
        if event.EventObject.GetValue():
            self.padre.settings[var] = True
        else:
            self.padre.settings[var] = False

        
