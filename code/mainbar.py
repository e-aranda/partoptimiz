#!/usr/bin/env python
# -*- coding: utf-8 -*-


import wx



class MainBar(wx.Panel):

    def __init__(self, parent):
    
        wx.Panel.__init__(self, parent)
#        self.SetScrollRate(1,0)

        self.sizer = wx.BoxSizer(wx.HORIZONTAL)
        self.SetSizer(self.sizer)
        self.widgets = []
        
    def add(self, widget):
        self.widgets.append(widget)
        
    def actual_add(self):
        """ 
        add current queue and erase queue to allow multiple invocations 
        """
        for w in self.widgets:
            self.sizer.Add(w, 0, wx.ALIGN_CENTER_VERTICAL)
        self.SetSize(self.GetBestSize())
        del self.widgets[:]
        
        self.sizer.Layout()
        self.Layout()

