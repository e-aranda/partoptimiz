# -*- coding: utf-8 -*-
"""
Created on Tue May 23 13:12:33 2017

@author: earanda

Basic c++ codes
"""

adaptacion = None

codigo = {}

codigo['$dummy$'] = ""

codigo['constants'] = """
#ifndef MY_CONSTANTS
#define MY_CONSTANTS

const int spdim = {dimension};
static int p = {simp}; 
static double E1 = {young};
static double E0 = {young_void};
static double nu = {poisson};
const double E10 = E1-E0;
const double lambda = {lamb};
const double mu = 1/(2.*(1.+nu));

static double q = 1;

const int n_f = {multiload_dim};
const double lambdacoef[{multiload_dim}] = {{ {multiload_val} }};

static double TOL = {TOL};
static int ITER = {ITER};

#endif
"""


codigo['$include$'] = """
#include <nlopt.hpp> // for nlopt
"""

codigo['$displacements$'] = """
    ParGridFunction *Udisp;
    Udisp = new ParGridFunction(p1espace);
    *Udisp = 0.0; // define in this way to set b.c. 
"""

codigo['$displacements$_ML'] = """
    ParGridFunction *Vdisp[n_f];
    for (int i = 0; i<n_f; i++)
    {
        Vdisp[i] = new ParGridFunction(p1espace);
        *Vdisp[i] = 0.0; // define in this way to set b.c. 
    }    
"""

codigo['$RHSdef$'] = """
    ParLinearForm *b;
    b = new ParLinearForm(p1espace);
"""

codigo['$RHSdef$_ML'] = """
    ParLinearForm *b[n_f];
    for (int i = 0; i<n_f; i++)
        b[i] = new ParLinearForm(p1espace);
"""

codigo['$assamblingb$_ML'] = """
    for (int i=0;i<n_f;i++)
        b[i]->Assemble();
"""

codigo['$assamblingder$_ML'] = """
    for (int i=0;i<n_f;i++)
        der[i]->Assemble();
"""

codigo['$initialfilter$'] = """
    filtro = new {namefilter};
"""

codigo['$prevcoef$'] = """
    ParGridFunction *Rhohat; 
    Rhohat = new ParGridFunction(p0espace);
    CoeffPsip *Psip = new CoeffPsip(*Rhohat,beta,eta);
"""

codigo['$coeffs$'] = """
    CoeffDerivative *derivative[n_f];
    for (int i=0; i<n_f; i++)
        derivative[i] = new CoeffDerivative(*Rhobar,*Vdisp[i],{heav});
"""

codigo['$defderivative$'] = """
    ParLinearForm *der;
    der = new ParLinearForm(p0espace);
    der->AddDomainIntegrator(new DomainLFIntegrator(derivative));
"""

codigo['$defderivative$_ML'] = """
    ParLinearForm *der[n_f];
    for (int i=0; i<n_f; i++)
    {
       der[i] = new ParLinearForm(p0espace);
       der[i]->AddDomainIntegrator(new DomainLFIntegrator(*derivative[i]));
    }
"""

codigo['$betabucle$'] = """
loopbeta++;
if ( (filtro->beta < filtro->maxbeta) && ( loopbeta >= filtro->betaloop ) && ({iter} >= filtro->iterbeta) )
       	 {{
       	   filtro->beta *= filtro->betaup;
       	   if (myid == 0) cout << "\\nBeta changed: " << filtro->beta << endl;
       	   Psip->UpdateConstant(filtro->beta);
       	   filtro->UpdateConstant(filtro->beta);
       	   loopbeta = 0;
       	 }}
"""



codigo['$bucleHeaviside$'] = """
    bool tes1 = true;
    bool tes2 = false;
    opt.set_maxeval(filtro->iterbeta);
    while (tes1)
    {{
        if (tes2) tes1 = false;

        {metodo}

        if (filtro->beta < filtro->maxbeta)
        {{
          filtro->beta *= filtro->betaup;
          if (myid == 0)
          {{
            cout << "\\nBeta changed: " << filtro->beta << endl;
            opt.set_maxeval(filtro->betaloop);
          }}
          Psip->UpdateConstant(filtro->beta);
          filtro->UpdateConstant(filtro->beta);
        }}
        else
        {{
          if (myid == 0) 
          {{
            iter = std::abs(maxiter - obj.siter) + 1;

            opt.set_maxeval(iter);
          }}
          tes2 = true;
        }}
        
     }}      
"""

codigo['$initialsystem$'] = """
    double global_compliance=1., compliance;
    compliance = Elasticity(a,b,Udisp,ess_tdof_list);
    MPI_Allreduce(&compliance, &global_compliance, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
"""

codigo['$initialsystem$_MEC'] = """
    double global_compliance=1., compliance;
    compliance = ElasticityMec(a,b,MECC,Udisp,ess_tdof_list);
    MPI_Allreduce(&compliance, &global_compliance, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
"""

codigo['$initialsystem$_ML'] = """
    double global_compliance = 0.;
    for (int i=0; i<n_f; i++)
      {
	double g_compliance=0., compliance;
	compliance = Elasticity(a,b[i],Vdisp[i],ess_tdof_list);
	MPI_Allreduce(&compliance, &g_compliance, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
	global_compliance += lambdacoef[i]*g_compliance;
      }
"""

codigo['$method$_OC'] = """
    {clase}
    {metodo}
"""

codigo['$method$_NLOPT'] = """
    // preparing data to collect par vectors
    int par_n = Rhobar->Size();
    // dimensions of densities in each process
    int *array_par_n = new int[num_procs];
    MPI_Allgather(&par_n,1,MPI_INT, array_par_n, 1, MPI_INT,MPI_COMM_WORLD);
    // collocation of par vectors
    int *disp = new int[num_procs];
    disp[0] = 0;
    for (int i=1;i<num_procs;i++)
        disp[i] = disp[i-1] + array_par_n[i-1];

    {clase}

    // problem dimension
    unsigned n;
    unsigned iter;
    if (myid == 0)
    {{
    	n = meshel;
    	iter = maxiter;
    }}
    else
    {{
    	n = 1;
    	iter = 10000;
    }}

    // vectors for nlopt
    vector<double> lb(n,0.), ub(n,1.), x(n,0.), dummy;
    // initialization of bounds and starting point
    // function in case of passive zone
    vector <double> parlb(par_n), parx(par_n);
    for (int i=0; i<par_n; i++)
    {{
        parlb[i] = xx.Elem(i);
    	parx[i] = max(initialization->Elem(i),parlb[i]);
    }}

    // gathering parx in x, parlb in lb
    MPI_Gatherv(parx.data(), par_n, MPI_DOUBLE, x.data(), array_par_n, disp, MPI_DOUBLE, 0, MPI_COMM_WORLD); 	
    MPI_Gatherv(parlb.data(), par_n, MPI_DOUBLE, lb.data(), array_par_n, disp, MPI_DOUBLE, 0, MPI_COMM_WORLD); 

    // nlopt problem definition
    nlopt::opt opt(nlopt::LD_MMA, n);        
    opt.set_lower_bounds(lb);
    opt.set_upper_bounds(ub);
    opt.set_ftol_rel(tol);
    opt.set_maxeval(iter);
    opt.set_min_objective(wrapper, &obj);
    opt.add_inequality_constraint(conswrapper, &obj, 1e-8);
    
    nlopt::result result;
    double minf;
    {metodo}
"""

codigo['$method$_HIOP'] = """

    int fn = p0espace->GetTrueVSize();
    Vector xmin(fn), xmax(fn);
    
    // lower bound
    ParGridFunction auxub(p0espace);
    for (int i=0; i<xx.Size();i++)
      auxub.Elem(i) = max(initialization->Elem(i), xx.Elem(i));
    p0espace->GetRestrictionMatrix()->Mult(xx,xmin);
    // upper bound
    xmax = 1.;
    
    {clase}
    
    obj.SetSolutionBounds(xmin,xmax);
    
    HiopNlpOptimizer *optsolver = new HiopNlpOptimizer(MPI_COMM_WORLD);
    
    optsolver->SetOptimizationProblem(obj);
    optsolver->SetMaxIter(maxiter);
    optsolver->SetRelTol(tol);
    optsolver->SetAbsTol(tol);
    optsolver->SetPrintLevel(0);
    Vector y_in(fn), y_out(fn);
    p0espace->GetRestrictionMatrix()->Mult(auxub,y_out);     
    {metodo}
"""

codigo['$method$_PETSC'] = """
   for (int i=0;i<initialization->Size(); i++)
        initialization->Elem(i) = max(initialization->Elem(i), xx.Elem(i));
   {clase}
   {metodo}
"""







codigo['$metodo$_OC'] = "obj.OCiteration(maxiter,tol);"

codigo['$metodo$_NLOPT'] = """ 
    try
    {{
        opt.optimize(x, minf);
        // sending message for stop rest of problems
        cout << "Process " << myid << " has finished" << endl;
 	int i = 1;
        MPI_Request myRequest;
 	for (int p=1;p<num_procs;p++)
            MPI_Isend(&i, 1, MPI_INT, p, 100, MPI_COMM_WORLD, &myRequest);
        obj.{funcion}(x,dummy);		
    }}
    catch (std::exception &e)
    {{	
    	cout << "Process " << myid << " stopped: " <<endl;
    }}
"""

codigo['$metodo$_HIOP'] = """
	y_in = y_out;
	optsolver->Mult(y_in,y_out);
"""


codigo['$metodo$_PETSC'] = "obj.{funcion}(maxiter,tol);"


codigo['$preafter$_NLOPT'] = """
	// recover solution
        MPI_Scatterv(x.data(), array_par_n, disp, MPI_DOUBLE, xx.GetData(), xx.Size(), MPI_DOUBLE, 0, MPI_COMM_WORLD);
	filtro->Filtering{withHW}(&xx,*Rhobar);
	// updating displacements
	compliance = Elasticity(a,b,Udisp,ess_tdof_list);
	MPI_Reduce(&compliance, &global_compliance, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);        
"""

codigo['$preafter$_NLOPT_Vol'] = """
	// recover solution
        MPI_Scatterv(x.data(), array_par_n, disp, MPI_DOUBLE, xx.GetData(), xx.Size(), MPI_DOUBLE, 0, MPI_COMM_WORLD);
"""

codigo['$preafter$_NLOPT_Mec'] = """
	// recover solution
        MPI_Scatterv(x.data(), array_par_n, disp, MPI_DOUBLE, xx.GetData(), xx.Size(), MPI_DOUBLE, 0, MPI_COMM_WORLD);
	filtro->Filtering{withHW}(&xx,*Rhobar);
	// updating displacements
	compliance = ElasticityMec(a,b,MECC,Udisp,ess_tdof_list);;
	MPI_Reduce(&compliance, &global_compliance, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);        
"""

codigo['$preafter$_NLOPT_ML'] = """
	// recover solution
        MPI_Scatterv(x.data(), array_par_n, disp, MPI_DOUBLE, xx.GetData(), xx.Size(), MPI_DOUBLE, 0, MPI_COMM_WORLD);
"""

codigo['$preafter$_HIOP'] = """
       p0espace->GetProlongationMatrix()->Mult(y_out,xx);
       filtro->Filtering{withHW}(&xx,*Rhobar);
       // updating displacements
       compliance = Elasticity(a,b,Udisp,ess_tdof_list);
       MPI_Allreduce(&compliance, &global_compliance, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
"""


codigo['$preafter$_HIOP_Mec'] = """
       p0espace->GetProlongationMatrix()->Mult(y_out,xx);
       filtro->Filtering{withHW}(&xx,*Rhobar);
       // updating displacements
       compliance = ElasticityMec(a,b,MECC,Udisp,ess_tdof_list);
       MPI_Allreduce(&compliance, &global_compliance, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
"""

codigo['$preafter$_HIOP_Vol'] = ""
codigo['$preafter$_HIOP_ML'] = ""
codigo['$preafter$_PETSC_Vol'] = ""
codigo['$preafter$_PETSC_Mec'] = ""



codigo['$preafter$_OC'] = ""
codigo['$preafter$_PETSC'] = ""
codigo['$preafter$_PETSC_ML'] = ""



codigo['$showresult$'] = """
    obj.GlVisit(obj.siter,{value},"Final Result -- Iterations: ","{objetivo}");
    if (myid == 0) cout << "Final Result -- Iterations: " <<  obj.siter << ",  {objetivo}: " << {value} << endl;
"""

codigo['$del$'] = """
    delete[] array_par_n;
    delete[] disp;
"""


codigo['$deletearray$'] = """
    delete Udisp;
    delete b;
    delete der;
"""

codigo['$deletearray$_ML'] = """
    for (int i=0;i<n_f;i++)
    {
        delete b[i];
        delete der[i];
        delete Vdisp[i];
    }
"""

codigo['$savesol$'] = """

    // Auxiliary GridFunction to compute stress
    ParGridFunction *gridstress;
    gridstress = new ParGridFunction(p0espace);
    *gridstress = 0.;

    // Coefficient for computing relaxed stress
    {VonMises} *sigmavm;    
    sigmavm = new {VonMises}(*Udisp);

    RelaxStress *stressr;
    stressr = new RelaxStress(*Rhobar,*sigmavm);
    gridstress->ProjectCoefficient(*stressr);

    const char *extension = ".gf";
    ostringstream name;
    name << \"{solpure}\" << setfill('0') << setw(6) << myid << extension;
    ofstream ff(name.str().c_str());
    {saving};
"""


codigo['$savesol$_ML'] = """
    const char *extension = ".gf";
    ostringstream name;
    name << \"{solpure}\" << setfill('0') << setw(6) << myid << extension;
    ofstream ff(name.str().c_str());
    {saving};
"""


codigo['$saveVTK$'] = """
    const char *output = \"{namevtk}\";
    VTKwriter writer(myid,num_procs,mesh_file,output,Rhobar,gridstress,Udisp); 
    writer.Creator();
"""


codigo['$saveVTK$_ML'] = """
    const char *output = \"{namevtk}\";
    VTKwriter writer(myid,num_procs,mesh_file,output,Rhobar); 
    writer.CreatorML();
"""

codigo['$saveParVTK$'] = """
    MyMesh *mallavtk = new MyMesh(pmesh);
    ostringstream namevt;
    namevt << \"{namevtk}\" << setfill('0') << setw(6) << myid << ".vtk";
    ofstream ffvtk(namevt.str().c_str());

    mallavtk->VTKParConverter(ffvtk,Rhobar,gridstress,Udisp);
"""

codigo['$saveParVTK$_ML'] = """
    MyMesh *mallavtk = new MyMesh(pmesh);
    ostringstream namevt;
    namevt << \"{namevtk}\" << setfill('0') << setw(6) << myid << ".vtk";
    ofstream ffvtk(namevt.str().c_str());

    mallavtk->VTKConverter(ffvtk,Rhobar);
"""

codigo['$thresholding$'] = """
    string name1 = \"{name1}\";
    string name2 = \"{name2}\";
    Thresholding thre(myid,40,Rhobar,vol,global_medida,Volfrac,name1,name2);
"""

codigo['$thresholding$_vol'] = """
    string name1 = \"{name1}\";
    string name2 = \"{name2}\";
    Thresholding thre(myid,Rhobar,vol,global_medida,obj.global_volumen,name1,name2,40);
"""

codigo['$maxstress$'] = """
    double maxstress = gridstress->Max();
    double gmax;
    MPI_Allreduce(&maxstress, &gmax, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
    if (myid == 0)
      {
        cout << "-------- Max. Stress: " << gmax << endl;
        cout << "--------------------------------" << endl;
      }
"""

codigo['$filtering$'] = """
    filtro->Filtering(Rho,*Rhobar); 
"""

codigo['$filtering$_WH'] = """
    filtro->FilteringWH(Rho,*Rhobar); 
"""

codigo['$extra$_WH'] = """
    filtro->Filtering(Rho,*Rhohat);
""" 
codigo['$extra$'] = ""


codigo['$density$'] = """
    // self-weight
    b->Update();
    b->Assemble(); 
"""

codigo['$vectorD_NLOPT$'] = ""    

codigo['$vectorD_NLOPT$_WH'] = """
    filtro->Filtering(Rho,*Rhohat);
    dconres->Update();
    dconres->Assemble();
    filtro->FilMult(*dconres,D);
    D *= Volconstant;
"""    

codigo['$vectorD_OC$'] = ""    

codigo['$vectorD_OC$_WH'] = """
    dconres->Update();
    dconres->Assemble();
    filtro->FilMult(*dconres,D);
"""

codigo['$vectorD_HIOP$'] = ""

codigo['$vectorD_HIOP$_WH'] = """
  ParGridFunction *Rho = new ParGridFunction(fespace);
  fespace->GetProlongationMatrix()->Mult(x, *Rho);
  // filter
  filtro->Filtering(Rho,*Rhohat);
  dconres->Update();
  dconres->Assemble();
  Vector gd(Rho->Size()), auxgrad(width);
  filtro->FilMult(*dconres,gd);
  fespace->Dof_TrueDof_Matrix()->MultTranspose(gd,auxgrad);
  for (int i=0;i<width;i++) grad(0,i) = Volconstant*auxgrad(i);
  delete Rho;
"""

codigo['$vectorY_HIOP$'] = """
  y = grad;
"""

codigo['$vectorY_HIOP$_WH'] = """
  ParGridFunction *Rho = new ParGridFunction(fespace);
  fespace->GetProlongationMatrix()->Mult(x, *Rho);
  // filter
  filtro->Filtering(Rho,*Rhohat);
  dconres->Update();
  dconres->Assemble();
  Vector gd(Rho->Size()), auxgrad(n);
  filtro->FilMult(*dconres,gd);
  fespace->Dof_TrueDof_Matrix()->MultTranspose(gd,auxgrad);
  for (int i=0;i<n;i++) y(i) = Volconstant*auxgrad(i);
  delete Rho;
"""



codigo['$vectorD_PETSC$'] = ""    

codigo['$vectorD_PETSC$_WH'] = """
    dconres->Update();
    dconres->Assemble();
    filtro->FilMult(*dconres,dres);
    dres *= Volconstant;
    DFDX->SetVector(dres,0);
    VecCopy(*DFDX, *dgdx);
    
"""




codigo['$vectorY_PETSC$'] = ""    

codigo['$vectorY_PETSC$_WH'] = """
    dconres->Update();
    dconres->Assemble();
    filtro->FilMult(*dconres,df0dx);
    df0dx *= Volconstant;
    df->SetVector(df0dx,0);
    
"""
