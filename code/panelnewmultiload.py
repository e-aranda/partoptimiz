"""
Created on Thu Jul 27 11:58:59 2017

@author: earanda
"""

import wx
import paneltitle
import os
import wx.grid as  gridlib
import panelentry
        
class MyPanelMultiLoads(panelentry.MyPanelEntry):
    """
    Panel containing weights and loads (matrix and force)
    """
    def __init__(self,parent,padre,titulo,var):
        wx.Panel.__init__(self, parent, style=wx.BORDER_SUNKEN)    
        self.padre = padre
        self.var = var
        
        # Sizers
        self.m_sizer = wx.BoxSizer(wx.VERTICAL)
        
        # title panel
        title = paneltitle.MyPanelTitle(self,titulo)

        # add button panel
        addbot = wx.Panel(self)        
        tot = wx.StaticText(addbot,-1,"Add new group")
        CURRENT_DIR = os.path.dirname(__file__)
        pngfile = os.path.join(CURRENT_DIR, os.path.join('icons','list-add.png'))   
        self.png = wx.Image(pngfile, wx.BITMAP_TYPE_PNG).ConvertToBitmap()
        bot = wx.BitmapButton(addbot, -1, self.png)
        bot.SetBackgroundColour(wx.Colour(240, 240, 240))       
        bot.Bind(wx.EVT_BUTTON, self.addgroupwith)
        
        b_sizer = wx.BoxSizer(wx.HORIZONTAL)
        # adding to sizers
        b_sizer.Add(tot,2,wx.RIGHT|wx.ALIGN_CENTER_VERTICAL,1) 
        b_sizer.Add(bot,1, wx.RIGHT,5)
        # set sizer        
        addbot.SetSizer(b_sizer)
        
        
        self.paneles = []
        self.panel_sizer = []

        self.weights = []
        self.adds = []
        self.loads = []
        self.label = []
        self.botones = []
        
        delfile = os.path.join(CURRENT_DIR, os.path.join('icons','trash.png'))   
        self.borrar = wx.Image(delfile, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
         
        # add Robin's
        self.i = 0 # number of Robin conditions

        self.paneles.append(wx.Panel(self))
        # add panel labels 
        n1 = wx.StaticText(self.paneles[0],-1,"Weight")
        n2 = wx.StaticText(self.paneles[0],-1,"Add Load")
        n3 = wx.StaticText(self.paneles[0],-1,"Delete Group")
        
        self.panel_sizer.append(wx.BoxSizer(wx.HORIZONTAL))

        self.panel_sizer[0].Add(n1,1,wx.ALL|wx.ALIGN_CENTER_VERTICAL,5)
        self.panel_sizer[0].Add(n2,1,wx.ALL|wx.ALIGN_CENTER_VERTICAL,5)
        self.panel_sizer[0].Add(n3,1,wx.ALL|wx.ALIGN_CENTER_VERTICAL,5)
        
        self.paneles[0].SetSizer(self.panel_sizer[0])

        self.m_sizer.Add(title,0, wx.TOP|wx.EXPAND,1)
        self.m_sizer.Add(addbot,0, wx.ALIGN_RIGHT,15)
        self.m_sizer.Add(self.paneles[0], 0, wx.TOP|wx.EXPAND, 5)
        
        self.SetSizer(self.m_sizer)
#        self.addrow(None)
        


    def addgroup(self):
        
        self.i += 1
        self.paneles.append(wx.Panel(self))
        self.panel_sizer.append(wx.BoxSizer(wx.HORIZONTAL))

        self.weights.append( (wx.TextCtrl(self.paneles[-1],-1,\
                            '',size=(10,-1),\
                            style=wx.TE_PROCESS_ENTER|wx.TE_RICH)))

        self.botones.append(wx.BitmapButton(self.paneles[-1], -1, self.borrar))
        self.botones[-1].SetBackgroundColour(wx.Colour(240, 240, 240))  
        self.adds.append(wx.BitmapButton(self.paneles[-1], -1, self.png))

        self.panel_sizer[-1].Add(self.weights[-1], 1,flag=wx.ALL,border=5)

        self.panel_sizer[-1].Add(self.adds[-1],1,flag=wx.ALL,border=5)
        self.panel_sizer[-1].Add(self.botones[-1],1,flag=wx.ALL,border=5)
        self.paneles[-1].SetSizer(self.panel_sizer[-1])
        
        self.m_sizer.Add(self.paneles[-1], 0, wx.TOP|wx.EXPAND, 0)


        self.paneles.append(wx.Panel(self))
        self.panel_sizer.append(wx.BoxSizer(wx.HORIZONTAL))
                
        self.loads.append(wx.grid.Grid(self.paneles[-1]))
        self.loads[-1].CreateGrid(1,4)
        
        self.loads[-1].SetColLabelValue(0, "fx")
        self.loads[-1].SetColLabelValue(1, "fy")
        self.loads[-1].SetColLabelValue(2, "fz")
        self.loads[-1].SetColLabelValue(3, "Labels")


        self.panel_sizer[-1].Add(self.loads[-1],1,\
                                 wx.EXPAND|wx.ALL|wx.ALIGN_TOP|wx.ALIGN_CENTER_VERTICAL, 10)

        self.paneles[-1].SetSizer(self.panel_sizer[-1])
    
        self.m_sizer.Add(self.paneles[-1], 0, wx.TOP|wx.EXPAND, 0)
        self.Layout()
        
        self.bindboton()

        self.set_dim(self.padre.values['dimension'])

    def addgroupwith(self,event):
        self.addgroup()
        self.padre.values[self.var].append(['',[['','','','']]])

    def bindboton(self):
        for i,xx in enumerate(self.loads):
            self.botones[i].Bind(wx.EVT_BUTTON, 
                        lambda event, x=i: self.delgroup(event,x))
            xx.Bind(gridlib.EVT_GRID_CELL_CHANGED, 
                       lambda event, x=i: self.OnCellLoadsChange(event,x))
            self.weights[i].Bind(wx.EVT_TEXT, 
                      lambda event, ind=i: self.set_rval(event,ind))
            self.adds[i].Bind(wx.EVT_BUTTON, 
                              lambda event, x=i: self.addrow(event,x))
            xx.Bind(gridlib.EVT_GRID_LABEL_RIGHT_CLICK,
                    lambda event, x=i: self.showPopupMenu(event,x))



    def addrow(self,event,j):
        """
        Add a row to a grid in a group
        """
        self.loads[j].AppendRows(1)
        row = self.loads[j].GetNumberRows()-1
        if self.padre.values['dimension'] == 2:
            self.loads[j].SetCellBackgroundColour(row, 2, (225,225,225))
            self.loads[j].SetReadOnly(row,2,True)
        self.padre.values[self.var][j][1].append(['','','',''])
        self.Refresh()
        self.Layout()
            

    def delgroup(self,event,j):
        
        self.i -= 1
        self.loads[j].Destroy()
        self.weights[j].Destroy()
        self.botones[j].Hide()
        self.adds[j].Hide()

        self.weights.pop(j)
        self.loads.pop(j)
        self.botones.pop(j)
        self.adds.pop(j)
        
        self.bindboton()
        self.Refresh()
        self.Layout()
        
        self.padre.values[self.var].pop(j)
        
    def OnCellLoadsChange(self, evt, j):
        """
        Update value in group load
        """
        self.padre.values[self.var][j][1][evt.GetRow()][evt.GetCol()] = \
            self.loads[j].GetCellValue(evt.GetRow(), evt.GetCol())     
        
    def set_rval(self,event,i):
        """
        Event for edition of data entries. Set values in MainFrame
        Connected to MainFrame
        """
        self.padre.values[self.var][i][0] = event.EventObject.GetValue()
        event.EventObject.SetForegroundColour(wx.BLACK)
        
    def set_dim(self,dim):
        """
        Change access to 3rd coordinate in case of changing dim
        """
        if dim == 2:
            for j in range(self.i):
                for i in range(self.loads[j].GetNumberRows()):
                    self.loads[j].SetCellValue(i,2,'')
                    self.loads[j].SetCellBackgroundColour(i, 2, (225,225,225))
                    self.loads[j].SetReadOnly(i,2,True)
        else:
            for j in range(self.i):
                for i in range(self.loads[j].GetNumberRows()):
                    self.loads[j].SetCellBackgroundColour(i, 2, (255,255,255))
                    self.loads[j].SetReadOnly(i,2,False)
                self.loads[j].Refresh()



    def showPopupMenu(self, event,i):
        """
        Create and display a popup menu on right-click event
        on label cell to delete it
        Connected to MainFrame through delrow
        """            
        menu = wx.Menu()
        itemdel = wx.MenuItem(menu,-1, "Delete Row")
        self.Bind(wx.EVT_MENU, lambda event, x=event.GetRow(), y=i: \
          self.delrow(event,x,y), itemdel)
        menu.Append(itemdel)
        self.PopupMenu(menu)
        menu.Destroy()        


        
    def delrow(self,event,x,y):
        """
        Delete a row and the corresponding values in MainFrame
        Connected to MainFrame
        """
        self.loads[y].DeleteRows(x)
        if (len(self.padre.values[self.var][y][1])>x):
            self.padre.values[self.var][y][1].pop(x)
        print(self.padre.values[self.var])
            
