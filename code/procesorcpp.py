"""
Created on Sat Jul 15 14:04:05 2017

@author: earanda
"""

import dictcode as d

from extfun import is_number,checkempty
import os
from subprocess import Popen
import compilewindow
from importlib import reload
import re

def showmessage(s, t):
    print(s, t)

class Processor(object):
    def __init__(self, padre, compilation, values):
        self.padre = padre
        self.v = values
        self.basepath = self.padre.basepath
        self.mainpath = self.padre.mainpath
        if compilation:
            reload(d)
            self.procesar()
            if self.v['methodFILTER'] == 'Conic':
                add0 = 'ADD0=$(DIR)/ParConic.o'
            else:
                add0 = ''
            if self.v['methodOPT'] == 'MMA':
                prob = 'nlopt'
            elif self.v['methodOPT'] == 'OC':
                prob = 'oc'
            elif self.v['methodOPT'] == 'HIOP':
                prob = 'hiop'
            elif self.v['methodOPT'] == 'PETSC':
                prob = 'mma'
            if self.padre.settings['showfinalresult'] or self.padre.settings['createvtks']:
                add1 = 'ADD1=$(DIR)/vtkcreator.o'
            else:
                add1 = ''
            rescomp = self.compilar(prob,add0,add1)
            if rescomp:
                showmessage("Error", "Compilation Error")
                self.padre.Destroy()
            else:
                print("Compilation done!")
        else:
            print("No compilation is needed")
        self.caller = self.runner()

    def procesar(self):
        """
        Processing values and building c++ files
        """
        maindir = os.path.dirname(os.path.abspath(__file__))
        srcdir = os.path.join(maindir, '../src/')
        
        #------------------------------------------------------------------------------
        # file constants.hpp

        ###############################################################################
        # Plane conditions hypothesis
        if self.v['tensionplana']:
            lamda = 'nu/(1.-nu*nu)'
            vonmises = 'VonMises2D'
        else:
            lamda = 'nu/((1+nu)*(1-2*nu))'
            if self.v['dimension'] == 2:
                vonmises = 'VonMises2Ddp'
            else:
                vonmises = 'VonMises3D'
        ###############################################################################
        
        # Weights for Multiload Problem        
        if self.v['problem'] == 'Multiload':
            pesos = self.computepesos(self.v['bd_multi'])
        else:
            pesos = ('1', '0.')
                        
        fileconstants = d.codigo['constants'].format(dimension=self.v['dimension'],
                                                     simp=self.v['simp'],
                                                     young=self.v['young'],
                                                    young_void=self.v['young_void'],
                                                     poisson=self.v['poisson'],
                                                     lamb=lamda,
                                                     multiload_dim=pesos[0],
                                                     multiload_val=pesos[1],
                                                     TOL=self.padre.settings['TOL'],
                                                     ITER=self.padre.settings['ITER'])
        
        # write file constants.hpp        
        filec = os.path.join(srcdir,'constants.hpp')
        with open(filec,'w') as fichero:
            fichero.write(fileconstants)            

        #------------------------------------------------------------------------------


        
        #------------------------------------------------------------------------------
        # file mainprogram.cpp     
        
        # analytic functions (headers and definitions)        
        self.analytic_header = ''
        self.analytic_defs = ''

        mesh = os.path.abspath(os.path.normpath((os.path.join(os.path.dirname(__file__),'..',self.v['path'],self.v['meshfile']))))


        #############################################################################    
        # dirichlet conditions
        dirichlet = ''
        for y,x in enumerate(self.padre.clamps):
            if self.v['clamped_tick'][x]:
                if checkempty(self.v['clamped'][x]):
                    dirichlet += self.dirichlet(x,y)
                    dirichlet += '\n'
                else:
                    showmessage("Error","Empty Dirichlet entry. Check your data")
                    return
        #############################################################################            



        #############################################################################    
        # linear forms and matrix form
        # inner loads
        linearform = ''
        for y, x in enumerate(self.v['in_loads']):
            if checkempty(''.join(x[:-1])):
                linearform += self.linearforms(x,y,'f','fff')
                linearform += 'b->AddDomainIntegrator(new VectorDomainLFIntegrator(f{y}));\n\t'.format(y=y)
        # boundary loads    
        for y, x in enumerate(self.v['bd_loads']):
            if checkempty(''.join(x[:-1])):
                linearform += self.linearforms(x,y,'g','ggg')
                linearform += 'b->AddBoundaryIntegrator(new VectorBoundaryLFIntegrator(g{y}));\n\t'.format(y=y)        
        
        # robin condition
        matrixform = ''        
        for y,x in enumerate(self.v['robin']):
            # load
            if checkempty(''.join(x[1])):
                z = x[1].copy()
                z.append(x[2])
                linearform += self.linearforms(z,y,'h','hhh')
                linearform += 'b->AddBoundaryIntegrator(new VectorBoundaryLFIntegrator(h{y}));\n'.format(y=y)                    
    
            # matrix
            if checkempty(''.join(sum(x[0],[]))):
                matrixform += self.matrixforms(x[0],x[2],y)
                if matrixform:
                    matrixform += 'a->AddBoundaryIntegrator(new VectorMassIntegrator(Mrobin{y}));\n'.format(y=y)
                else:
                    showmessage("Error","Something wrong in Robin conditions")
                    return
            else:
                showmessage("Error","Matrix of Robin conditions empty")
                return
        #############################################################################    


        #############################################################################    
        # Heaviside Projection and Filter
        if self.v['betaPROJECTION']:
            var = '*Psip'
            prevcoef = d.codigo['$prevcoef$']
            withHW = "WH"
            if self.v['methodFILTER'] == "Conic":
                namefilter = "ConicFilter(pmesh,radius,beta,eta,maxbeta,betaup,iterbeta,betaloop)"
            else:
                namefilter = "HelmholtzFilter(pmesh,radius,fec,fec0,beta,eta,maxbeta,betaup,iterbeta,betaloop)" 
        else:
            var = 'one'
            prevcoef = ''
            withHW = ''
            if self.v['methodFILTER'] == "Conic":
                namefilter = "ConicFilter(pmesh,radius)"
            else:
                namefilter = "HelmholtzFilter(pmesh,radius,fec,fec0)"

        dconres = "dconres->AddDomainIntegrator(new DomainLFIntegrator({heav}));".format(heav=var)                
        #############################################################################    


        
        #############################################################################    
        # self-weight hypothesis
        if self.v['density_tick']:
            if not self.v['density']:
                showmessage("Error","No density in Self-Weight")
                return
            gravity = 'const double density = -9.81*(' + self.v['density'] + ');\n'
            dim1 = int(self.v['dimension']) - 1
            gravity_force = "\
            VectorArrayCoefficient Density(dim);\n\
            Density.Set({d},new DensityCoeff(*Rhobar,density));\n\
            b->AddDomainIntegrator(new VectorDomainLFIntegrator(Density));\n".format(d=dim1)
            gravity_der = "DerDensityCoeff selfweight(*Udisp," + var + ",density);\n\
            der->AddDomainIntegrator(new DomainLFIntegrator(selfweight));\n"            
        else:
            gravity = ''
            gravity_force = ''
            gravity_der = ''
            d.codigo['$density$'] = ""
        #############################################################################    

        

        #############################################################################            
        # initialization        
        if self.v['init_file']:
            rutainit = os.path.normpath(os.path.join(self.mainpath,self.v['init_file']))
            initialization = "ostringstream init_name;\n\tinit_name << \"{name}\" << setfill('0') << setw(6) << myid << \".gf\" ;\
            \n\tifstream initfile(init_name.str().c_str());\n\tinitialization = new ParGridFunction(pmesh,initfile);\n".format(name=rutainit)
        else:
            initialization = "initialization = new ParGridFunction(p0espace);\n\t*initialization = Initcte;"
        #############################################################################    


        
        #############################################################################                
        # passive zone
        passive = 'ParGridFunction xx(p0espace);\n'
        if self.v['passive_tick']:
            if checkempty(self.v['passive']):
                passive += "\
                Vector passive(pmesh->attributes.Max());\n\
                passive = 0.0;\n"
                for x in self.v['passive'].split():
                    passive += 'passive(' + str(int(x)-1) + ') = {cotainf};\n'
                passive += "\
                PWConstCoefficient passive_func(passive);\n\
                xx.ProjectCoefficient(passive_func);\n"
            else:
                showmessage("Error","No label in passive zone")
                return
        else:
            passive += '\txx = 0.;\n'

        cotainf = "1.0"
        #############################################################################                



        #############################################################################                
        # optimization methods
        if self.v['methodOPT'] == "MMA":
            suffix = "_NLOPT"
            include = "#include <nlopt.hpp> // for nlopt"
            delco = d.codigo['$del$']
            if self.v['problem'] == "Mechanism":
                clase = "ToptimizNLOPT obj(p0espace,a,b,der,vol,dconres,Rhobar,ess_tdof_list,filtro,visualization,Udisp,mec,Qdisp,Mobj,Volconstant,array_par_n,disp"
                problem = "ToptimizNLOPT*"
                d.codigo['$funcion$'] = "myfuncMec"
            elif self.v['problem'] == "Multiload":
                clase = "ToptimizNLOPTML obj(p0espace,a,b,der,vol,dconres,Rhobar,ess_tdof_list,filtro,visualization,Vdisp,Mobj,Volconstant,array_par_n,disp"
                problem = "ToptimizNLOPTML*"
                d.codigo['$funcion$'] = "myfunc"
            else:
                clase = "ToptimizNLOPT obj(p0espace,a,b,der,vol,dconres,Rhobar,ess_tdof_list,filtro,visualization,Udisp,Mobj,Volconstant,array_par_n,disp"
                problem = "ToptimizNLOPT*"
        else:
            problem = ''
            include = ''
            delco = ''
            d.codigo['$funcion$'] = ""

        if self.v['methodOPT'] == "OC":
            suffix = "_OC"
            saving = "obj.Rho->Save(ff)"
            if self.v['problem'] == "Compliance":
                clase = "ToptimizOC obj(p0espace,a,b,der,vol,dconres,Rhobar,initialization,\
                ess_tdof_list,filtro,visualization,Udisp,Mobj,Volconstant,zeta,neta"
            elif self.v['problem'] == "Multiload":
                clase = "ToptimizOCML obj(p0espace,Rhobar,Vdisp,a,b,der,vol,dconres,\
                Mobj,Volconstant,ess_tdof_list,initialization,filtro,visualization,zeta,neta"
            else:
                showmessage("Error","Optimality Conditions is not implemented for " + self.v['problem'] + " problem")
                return
        else:
            saving = "xx.Save(ff)"

        if self.v['methodOPT'] == "HIOP":
            suffix = "_HIOP"
            if self.v['problem'] == "Mechanism":
                clase = "ToptimizHIOPMec obj(p0espace,Rhobar,filtro,a,b,der,vol,dconres,mec,\
                ess_tdof_list,Udisp,Qdisp,Volconstant,Mobj,visualization"
            elif self.v['problem'] == "Multiload":
                clase = "ToptimizHIOPML obj(p0espace,Rhobar,filtro,a,b,der,vol,dconres,\
                ess_tdof_list,Vdisp,Volconstant,Mobj,visualization"
            elif self.v['problem'] == "Volume":
                clase = "ToptimizHIOPVol obj(p0espace,Rhobar,ess_tdof_list,filtro,a,b,der,vol,dconres,\
                Udisp,Volconstant,Mobj,visualization"
            elif self.v['problem'] == "Compliance":
                clase = "ToptimizHIOP obj(p0espace,Rhobar,filtro,a,b,der,vol,dconres,ess_tdof_list,\
                Udisp,Volconstant,Mobj,visualization"

        if self.v['methodOPT'] == "PETSC":            
            suffix = "_PETSC"
            include = '#include "MMA.h"'
            cotainf = "0.99"
            if self.v['problem'] == "Mechanism":
                clase = "ToptimizPETSCMec obj(p0espace,Rhobar,filtro,a,b,der,vol,dconres,mec,Qdisp,ess_tdof_list,initialization,\
              Udisp,&xx,Volconstant,Mobj,visualization"
            elif self.v['problem'] == "Multiload":
                clase = "ToptimizPETSCML obj(p0espace,a,b,der,vol,dconres,Rhobar,ess_tdof_list,filtro,initialization,&xx,visualization,Vdisp,Mobj,Volconstant"
            else:
                clase = "ToptimizPETSC obj(p0espace,Rhobar,filtro,a,b,der,vol,dconres,ess_tdof_list,initialization,\
              Udisp,&xx,Volconstant,Mobj,visualization"

                
                #############################################################################                
        metodo = '$metodo$' + suffix
        method = '$method$' + suffix
        preaf = '$preafter$' + suffix 
#############################################################################    
        # Mechanism  problems
        if self.v['problem'] == 'Mechanism':
            objetivo = "Objective"
            # objective function for mechanism
            mechanism = "ParLinearForm *mec;\n\tmec = new ParLinearForm(p1espace);\n"
            for y,x in enumerate(self.v['obj_mechanism']):
                if checkempty(''.join(x[:-1])):
                    mechanism += self.linearforms(x,y,'mecobj','mecs')
                    mechanism += 'mec->AddBoundaryIntegrator(new VectorBoundaryLFIntegrator(mecobj{y}));\n'.format(y=y)        
                else:
                    showmessage("Error","Empty mechanism objective")
                    return
            mechanism += "mec->Assemble();\n\tHypreParVector *MECC = mec->ParallelAssemble();"
            initialsystem = "$initialsystem$_MEC"
            initcompliance = 'if (myid==0) cout << "Initial Objective: " << global_compliance << endl;'
            qdisp = "ParGridFunction *Qdisp;\n\tQdisp = new ParGridFunction(p1espace);\n*Qdisp = 0.0;" 
            addpreaf = "_Mec"
            if self.v['methodOPT'] == "PETSC":
                value = 'obj.global_compliance'
            else:
                value = 'global_compliance'
            if self.v['betaPROJECTION']:
                coefficient = "CoeffDerivativeAdj derivative(*Rhobar,*Udisp,*Qdisp,*Psip);\n"
            else:
                coefficient = "CoeffDerivativeAdj derivative(*Rhobar,*Udisp,*Qdisp,one);\n"
        else:
            mechanism = ''
            qdisp = ''
             
            if self.v['betaPROJECTION']:
                coefficient =  "\tCoeffDerivative derivative(*Rhobar,*Udisp,*Psip);\n"    
            else:
                coefficient = "\tCoeffDerivative derivative(*Rhobar,*Udisp,one);\n"
        #############################################################################

        
        #############################################################################    
        # Multiload problem
        if self.v['problem'] == 'Multiload':
            objetivo = "Weighted Compliance"
            coefficient = d.codigo['$coeffs$'].format(heav=var)
            linearform = self.multiload(self.v['bd_multi'],linearform)
            initcompliance = 'if (myid==0) cout << "Initial Weighted Compliance: " \
            << global_compliance << endl;'
            assamblingder = d.codigo['$assamblingder$_ML']
            assamblingb = d.codigo['$assamblingb$_ML']
            initialsystem = '$initialsystem$_ML'
            rhsdef = '$RHSdef$_ML'
            displacements = '$displacements$_ML'
            defderivative = '$defderivative$_ML'
            deletearray = '$deletearray$_ML'
            savecode = '$savesol$_ML'
            addpreaf = '_ML'
            parvtk = '$saveParVTK$_ML'
            sVTK = '$saveVTK$_ML'
            maxstress = ''
            value = 'obj.global_compliance'
        else:
            assamblingder = 'der->Assemble();'
            assamblingb = '\tb->Assemble();'
            rhsdef = '$RHSdef$'
            displacements = '$displacements$'
            defderivative = '$defderivative$'
            deletearray = '$deletearray$'
            savecode = '$savesol$'
            parvtk = '$saveParVTK$'
            sVTK = '$saveVTK$'
            maxstress = d.codigo['$maxstress$']
        #############################################################################    


        #############################################################################        
        # Volume problem    
        if self.v['problem'] == 'Volume':
            objetivo = "Volume"
            projectRhobar = ''
            initcompliance = 'if (myid==0) cout << "Best Compliance: " << global_compliance << endl;'
            initialsystem = "$initialsystem$"
            mobjs = "double Mobj = Volfrac/global_compliance;\n"        
            volconstant = "double Volconstant = 1./global_medida;\n"
            d.codigo['$funcion$'] = 'myfuncVol'
            d.codigo['$restriccion$'] = 'myconstraintVol'
            dthread = '$thresholding$_vol'
            value = 'obj.global_volumen'
            addpreaf = "_Vol"
        else:
            projectRhobar = '*Rhobar = Volfrac;'
            mobjs = "double Mobj = abs(1./global_compliance);\n"
            volconstant = "double Volconstant = 1./(Volfrac*global_medida);\n"
            d.codigo['$restriccion$'] = 'myconstraint'
            dthread = '$thresholding$'
            if self.v['methodOPT'] == "OC":
                volconstant = "double Volconstant = Volfrac*global_medida;\n"
        #############################################################################




        #############################################################################                
        # Compliance problem           
        if self.v['problem'] == "Compliance":
            objetivo = "Compliance"
            initialsystem = "$initialsystem$"
            initcompliance = 'if (myid==0) cout << "Initial Compliance: " << global_compliance << endl;'
            d.codigo['$funcion$'] = "myfunc"
            addpreaf = ""
            if self.v['methodOPT'] == "OC" or self.v['methodOPT'] == "PETSC":
                value = 'obj.global_compliance'
            else:
                value = 'global_compliance'
        #############################################################################                

            

        #############################################################################
        # Thresholding
        thres1 = os.path.join(self.basepath,self.v['namefile'] + '-threshold.vtk')
        thres2 = os.path.join(self.basepath,self.v['namefile'] + '-threshold.vtk.table')
        
        threshold = d.codigo[dthread].format(name1=thres1,name2=thres2)
        #############################################################################
                
    

        ############################################################################
        # saving solutions
        # add creation of single vtk for visualization into application                    
        solpure = os.path.join(self.basepath,self.v['resultadopure'])
        namevtk = os.path.join(self.basepath,self.v['namefile']+'.vtk')
        namevtkpar = os.path.join(self.basepath,self.v['namefile'])

        savesol = d.codigo[savecode].format(VonMises=vonmises,solpure=solpure,saving=saving)

        if self.padre.settings['showfinalresult']:
            vtkwriter = d.codigo[sVTK].format(namevtk=namevtk)
        else:
            vtkwriter = ''
        
        # add exporting  of vtk files per processor for visualization with
        # paraview
        if self.padre.settings['createvtks']:
            vtkprocess = d.codigo[parvtk].format(namevtk=namevtkpar)
        else:
            vtkprocess = ''

        if self.padre.settings['createvtks'] or self.padre.settings['showfinalresult']:
            include += '\n#include "vtkcreator.hpp"'
        ############################################################################
        if self.v['methodOPT'] != "OC":
            preaf += addpreaf

        upmetodo = d.codigo[metodo].format(funcion=d.codigo['$funcion$'])
        if self.v['methodOPT'] == 'MMA' and self.v['betaPROJECTION']:
            upmetodo = d.codigo['$bucleHeaviside$'].format(metodo=upmetodo)

        if self.v['methodOPT'] == 'PETSC':
            if self.v['problem'] == "Volume":
                iteracion = "IterationVol"
            else:
                iteracion = "Iteration"
            upmetodo = d.codigo[metodo].format(funcion=iteracion)

            
        # --------------------------------------------------------        
        # processing files
        filetoprocess = "fun" + suffix + '.cpp'
        if self.v['methodOPT'] == "MMA":
            namesch = ["$filtering$","$vectorD_NLOPT$","$extra$"]
            namessh = ["$density$","$problem$","$funcion$","$restriccion$"]
            betabucle = ""
        elif self.v['methodOPT'] == "OC":
            namesch = ["$filtering$","$vectorD_OC$","$extra$"]
            namessh = ["$density$"]
            betabucle = d.codigo['$betabucle$'].format(iter='k')
        elif self.v['methodOPT'] == "HIOP":
            namesch = ["$filtering$","$vectorD_HIOP$","$vectorY_HIOP$","$extra$"]
            namessh = ["$density$"]
            betabucle = d.codigo['$betabucle$'].format(iter='siter')
        elif self.v['methodOPT'] == "PETSC":
            namesch = ["$filtering$","$vectorD_PETSC$","$vectorY_PETSC$","$extra$"]
            namessh = ["$density$"]
            betabucle = d.codigo['$betabucle$'].format(iter='siter')
        else:
            showmessage("Error","There is no method")
            return

        rnamesch = []
        if self.v['betaPROJECTION']:
            if self.v['methodOPT'] == "MMA":
                clase +=',NULL,Rhohat);\n'
            else:
                clase +=',Psip,Rhohat);\n'
            delwh = 'delete Rhohat;\n delete Psip;'
            for x in namesch:
                y = x + '_WH'
                rnamesch.append(y)
        else:
            clase += ');\n'
            delwh = ''
            rnamesch = namesch[:]
            betabucle = ''
        
        d.codigo['$problem$'] = problem
        
        # create funMETHOD        
        archivo = os.path.join(srcdir,filetoprocess)

        with open(archivo,'r') as myfile:
            content = myfile.read()
        for x in namessh:
            content = content.replace(x,d.codigo[x])
        for x,y in zip(namesch,rnamesch):
            content = content.replace(x,d.codigo[y])
        content = content.replace('$betabucle$',betabucle)

        namef = 'FUN' + suffix + '.cpp'
        newarchivo = os.path.join(srcdir,namef)
        with open(newarchivo,'w') as myfile:
            myfile.write(content)
        # --------------------------------------------------------        


            
        # process main.cpp
        marchivo = os.path.join(srcdir,'mainprogram.cpp')
        with open(marchivo,'r') as myfile:
            content = myfile.read()

        
            
        # replacement from procesor
        replacers = {'$include$':include,
                     '$headers$': self.analytic_header,
                     '$mesh$': mesh,
                     '$gravity$': gravity,
                     '$displacements$': d.codigo[displacements],
                     '$dirichlet$': dirichlet,
                     '$RHSdef$': d.codigo[rhsdef],
                     '$assamblingb$': assamblingb,
                     '$assamblingder$': assamblingder,
                     '$dconres$': dconres,
                     '$linearform$': linearform,
                     '$gravity_force$': gravity_force,
                     '$mechanism$': mechanism,
                     '$matrixform$': matrixform,
                     '$initialsystem$': d.codigo[initialsystem],
                     '$prevcoef$': prevcoef,
                     '$coefficients$': coefficient,
                     '$defderivative$' : d.codigo[defderivative],
                     '$projectRhobar$': projectRhobar,
                     '$initcompliance$': initcompliance,
                     '$initialfilter$': d.codigo['$initialfilter$'].format(namefilter=namefilter),
                     '$mobjs$': mobjs, '$volconstant$': volconstant,
                     '$qdisp$': qdisp,
                     '$gravity_der$': gravity_der,
                     '$passive$': passive.format(cotainf=cotainf),
                     '$initialization$': initialization,
                     '$method$' : d.codigo[method].format(clase=clase,metodo=upmetodo),
                     '$preafter$': d.codigo[preaf].format(withHW=withHW),
                     '$showresult$': d.codigo['$showresult$'].format(objetivo=objetivo,value=value),
                     '$savesol$': savesol,
                     '$vtkwriter$': vtkwriter,
                     '$parvtk$': vtkprocess,
                     '$threshold$': threshold,
                     '$maxstress$': maxstress,
                     '$delete$': delwh,
                     '$deletearray$' : d.codigo[deletearray],
                     '$del$' : delco,
                     '$analyticfuns$': self.analytic_defs}
        
        for x in replacers.keys():
            content = content.replace(x,replacers[x])
         
        mainprog = os.path.join(srcdir,'MAIN.cpp')
        
        with open(mainprog,'w') as myfile:
            myfile.write(content)
        
        
        
        
        
        
    def dirichlet(self,x,y):
        cadena = self.labels(self.v['clamped'][x])
        cadena += 'p1espace->GetEssentialTrueDofs(ess_bdr, etiquetas,{});\n\t'.format(y)
        cadena += 'ess_tdof_list.Append(etiquetas);\n\t'
        return cadena
        
        
    def labels(self,string,bd='_bdr'):
        namearray = 'ess' + bd
        cadena = '\t' + namearray + ' = 0;\n\t'   
        for x in string.split():
            cadena += namearray +'[' + str(int(x)-1) +'] = 1;\n\t' 
        return cadena
    
    def linearforms(self,x,y,namef,namep):
        if x[3]:
            etiquetas = self.labels(x[3])
            res = True
        else:
            etiquetas = '\n\t'
            res = False
        cadena = etiquetas    
        cadena += 'VectorArrayCoefficient ' + namef + '{}(dim);\n\t'.format(y)
        for i,z in enumerate(x[:-1]):
            if checkempty(z):                
                if is_number(z):
                    cadena += 'ConstantCoefficient ' + namep + '{y}{i}({cte});\n\t'.format(i=i,cte=float(z),y=y)
                else:
                    cadena += 'FunctionCoefficient ' + namep + '{y}{i}(func'.format(i=i,y=y) + namep + 'per{y}{i});\n\t'.format(i=i,y=y)
                    self.analytic_definition('func'+ namep + 'per{y}{i}'.format(i=i,y=y),z)
                if res:
                    cadena += namef + '{comp}.Set({i},new RestrictedCoefficient('.format(i=i,comp=y) + namep + '{comp}{i},ess_bdr));\n\t'.format(i=i,comp=y)        
                else:
                    cadena += namef + '{comp}.Set({i},&'.format(i=i,comp=y) + namep + '{comp}{i});\n\t'.format(i=i,comp=y)
        return cadena
    
    
    def matrixforms(self,matrix,label,ind):
        if checkempty(label):
            etiquetas = self.labels(label)
        else:
            showmessage("Error","Label in Robin condition is empty. Check your data")
            return ''
        cadena = 'MatrixArrayCoefficient Mrobin{}(dim);\n'.format(ind)
        cadena += etiquetas
        
        for i,row in enumerate(matrix):
            for j,col in enumerate(row):
                if checkempty(col):
                    if is_number(col):
                        cadena += 'ConstantCoefficient m{ind}{i}{j}({cte});\n'.format(i=i,
                                                                j=j,ind=ind,cte=float(col))
                        cadena += 'Mrobin{ind}.Set({i},{j},new RestrictedCoefficient(m{ind}{i}{j},ess_bdr));\n'.format(ind=ind,i=i,j=j)
                    else:
                        showmessage("Error","Matrix of Robin must be constant. Check your data")
                        return ''
        return cadena
            

    def analytic_definition(self,name,funcion):
        
        self.analytic_header += 'double ' + name + '(const Vector &x);\n'
        self.analytic_defs += 'double ' + name + '(const Vector &x)\n\
        {\nreturn ' + funcion + ';\n}'  
        

    def computepesos(self,val):
        if len(val)<=1:
            showmessage("Error","You have choose Multiload problem but there is not enough loads. Check your data")
            return '',''
        else:
            dim = len(val)
            weights = [x[0] for x in val]
            if abs(sum([float(x) for x in weights]) - 1.0) > 1.e-2:
                showmessage("Error","Your weights does not sum 1.Check your data")
                return '',''
            coefs = ', '.join(weights)
            return (dim,coefs)
        
        
    def multiload(self,val,linearform):
        mylinearform = linearform.split('\n')
        if len(val) < 2:
            showmessage("Error","You have choose Multiload problem but there is not enough loads. Check your data")
            return ''
        newlinearform = ''
        aplies = ''
        for line in mylinearform:
            if not re.search('Integrator',line):
                newlinearform += line + '\n'
            else:
                aplies += line + '\n'

        for i,f in enumerate(val):
            cad1 = 'k_' + str(i)
            cad2 = 'kkk_' + str(i)
            for y,x in enumerate(f[1]):
                if checkempty(''.join(x[:-1])):

                    newlinearform += self.linearforms(x,y,cad1,cad2)
                    newlinearform += ('b[{i}]->AddBoundaryIntegrator(new \
                         VectorBoundaryLFIntegrator('+ cad1 + '{y}));\n').format(i=i,y=y)
                else:
                    showmessage("Error","Empty load for Multiload problem")
                    return
            newlinearform += aplies.replace('b->','b[{y}]->'.format(y=i))
        return newlinearform
            

    def compilar(self,problem,add0,add1):
        myfile = os.path.normpath(os.path.join(self.padre.mainpath,'src/makefile'))
        res = ["make","-f",myfile]
        if add0:
            res.append(add0)
        if add1:
            res.append(add1)
        res.append(problem)
        print("Compilation:")
        for i in res:
            print(i,end=' ')
        print()
        # Compiling in second plane
        r = Popen(res)
        # Show a progress window
        newframe = compilewindow.Progress("Compiling...")
        r.communicate()[0]   
        newframe.Destroy()
        return r.returncode
    

    def runner(self):
        myfile = os.path.normpath(os.path.join(self.padre.mainpath,'bin/program'))
        if self.v['problem'] == 'Volume':
            volfrac = self.v['compliance']
        else:
            volfrac = self.v['volfrac']
            
        if self.v['methodOPT'] == 'OC':
            addoption = [' -z ',self.v['zeta'],' -n ',self.v['eta']]
        else:
            addoption = []
        options = [' -r ',self.v['filter'],' -v ',volfrac,' -i ',self.v['maxiter'], ' -s ', self.v['init_cte'],
                   ' -t ',self.v['tol'], ' -bl ',self.v['betaloop'],' -bup ',self.v['betaup'], ' -ib ', self.v['iterbeta'], ' -mb ', self.v['maxbeta']]
        if self.padre.settings['visualization']:
            options[0:0] = ' -vis '
        else:
            options[0:0]= ' -no-vis '
        options.extend(addoption)
        caller = ['mpirun',' ','-np',' ',self.padre.settings['nps'],' ',myfile]
        caller.extend(options)
        print("Execution: ")
        for i in caller:
            print(i,end='')
        print()
        return caller    
