#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 19 15:44:46 2018

@author: earanda
"""

helpers = {}


helpers["Toptimiz3D"] = "toptimiz3d.png"
helpers["Elastic Constants"] = "elastic_constants.png"
helpers["Optimization's Parameters"] = "opt_parameters.png"
helpers["Parameters for Optimality Conditions"] = "ocparameters.png"
helpers["Parameters for Stress Problem"] = "stress.png"
helpers["Initial Density"] = "initial.png"
helpers["Body Loads"] = "body_loads.png"
helpers["Boundary Loads"] = "boundary_loads.png"
helpers["Robin's Conditions"] = "robin.png"
helpers["Clamped Conditions"] = "Fixed_displacements.png"
helpers["Boundary Multi Loads"] = "multiloads.png"
helpers["Passive Zones"] = "Passive_zones.png"
helpers["Self-Weight"] = "Self_Weight.png"
helpers["Mechanism Objective"] = "mechanism.png"
helpers["Results"] = "results.png"
helpers["General Settings"] = "settings.png"

