# -*- coding: utf-8 -*-
"""
Created on Thu Jul 27 11:50:38 2017

@author: earanda
"""
import wx
import os
from extfun import vtkbuilder, readdim
import convert2mfem


# ---------------------------------------------------------------------------   
class MyPanelGraphButton(wx.Panel):
    """
    Panel containing a buttons and other staff on vtk panel
    """
    def __init__(self,parent,padre):
        wx.Panel.__init__(self, parent)
        self.padre = padre
        ################################################
        # add buttons and other staff to graphicbot
        self.plotbut = wx.Button(self,-1,"Load Mesh",pos=(10,10))

        self.texto = ""
        self.textopunto = ""
        self.textbounds = ""
        self.shownt = wx.StaticText(self,-1,self.texto,pos=(130,14),style=wx.ALIGN_CENTER_VERTICAL)
        self.shownp = wx.StaticText(self,-1,self.textopunto,pos=(130,30),style=wx.ALIGN_CENTER_VERTICAL)
        self.showbounds = wx.StaticText(self,-1,self.textbounds,pos=(130,70),style=wx.ALIGN_CENTER_VERTICAL)
        self.qualities = ""
        self.showquality = wx.StaticText(self,-1,self.qualities,pos=(130,100),style=wx.ALIGN_CENTER_VERTICAL)

        font = wx.Font(7, wx.DEFAULT, wx.NORMAL, wx.NORMAL)   
        intext = wx.StaticText(self,-1,"Inner Labels",pos=(450,14))
        intext.SetFont(font)
        bdtext = wx.StaticText(self,-1,"Boundary Labels",pos=(550,14))
        bdtext.SetFont(font)
        self.innerlabel = []    
        self.innercombo =  wx.ComboBox(self,-1, pos=(450,30),\
                    size = (70,-1), \
                    choices = self.innerlabel, style = wx.ALIGN_CENTRE)
        
        self.boundlabel = []    
        self.boundcombo =  wx.ComboBox(self,-1, pos=(550,30),\
                    size = (70,-1), \
                    choices = self.boundlabel, style = wx.ALIGN_CENTRE)    
                       
        ############################################
        # bind buttons
        self.Bind(wx.EVT_BUTTON,self.plotbutton,self.plotbut)
#        self.Bind(wx.EVT_COMBOBOX,self.show_results,self.desplegable)
        self.Bind(wx.EVT_COMBOBOX,self.mesh_boundary,self.boundcombo)
        self.Bind(wx.EVT_COMBOBOX,self.mesh_inner,self.innercombo)

    def plotbutton(self,event):
        """
        Event for button: Load mesh
        Set up meshfile and path (relative to mainpath)
        """            
        dlg = wx.FileDialog(self, "Choose a mesh file", self.padre.mainpath, "", \
            "Mesh files (*.mesh;*.msh;*.mphtxt)|*.mesh;*.msh;*.mphtxt|All files (*.*)|*.*",\
            wx.FD_OPEN | wx.FD_FILE_MUST_EXIST)
        if dlg.ShowModal() == wx.ID_OK:
            filemesh = dlg.GetPath()
            self.padre.values['path'] = os.path.dirname(os.path.relpath(filemesh,self.padre.mainpath))
            name = os.path.basename(os.path.abspath(filemesh)) # name of meshfile (no path)
            self.padre.values['meshfile'] = name
            self.padre.values['namefile'] = name.split('.')[0]
#            extension = name.split('.')[-1] # extension of meshfile
            # absolute path of meshfile
            filemesh = os.path.normpath(os.path.join(self.padre.mainpath,self.padre.values['path'],name))

#            convert mesh to MFEM format if necessary
            w = convert2mfem.conversor(filemesh)
            path = os.path.dirname(filemesh)
            nuevo = os.path.join(path,w.output)
            tipo = w.tipomalla()
            i = w.convertidor(tipo,nuevo)
            if not i:               
                filemesh = nuevo
            elif i<0:
                self.padre.ShowMessage("Error","Format mesh not recognised")
                return
            self.padre.values['meshfile'] = os.path.basename(os.path.abspath(filemesh))  
            self.Plot_Mesh(filemesh)
        else:
            return
            

    def mesh_boundary(self,event):
        """
        Selection of part of the boundary button event
        """
        self.innercombo.SetSelection(-1)
        a = int(event.GetString())
        self.padre.graphicmesh.render_actor(a,1)
        
    def mesh_inner(self,event):
        """
        Selection of part of the domain button event
        """
        self.boundcombo.SetSelection(-1)
        a = int(event.GetString())
        self.padre.graphicmesh.render_actor(a)
        

    def Plot_Mesh(self,filemesh):
        """
        Show a built vtk file from a read mesh from plotbutton 
        """        
        
        # clean values if vtk windows had been used
        if self.padre.graphicmesh.isplotedbefore:
             del self.padre.graphicmesh.widget
             del self.padre.graphicmesh.ren
             del self.padre.graphicmesh.sizer
             self.padre.graphicmesh.initialize()                 
        
        self.padre.graphicmesh.isplotedbefore = True
        
        # check if there exists already a vtk file
        filename = os.path.join(self.padre.basepath,self.padre.values['namefile'] + '_mesh.vtk')
        
        datemalla = os.path.getmtime(filemesh)
        try:
            datevtk = os.path.getmtime(filename)
        except:
            datevtk = 0.
        
        if datemalla > datevtk:        
            print("Building vtk file")            
            dim,info = vtkbuilder(filemesh,filename)
        else:
            print("Reading vtk from disk")
            dim = readdim(filemesh)
        if not dim:
            self.padre.ShowMessage("Error","The mesh has not been correctly read" + '\n' + info)
            return
        
        if dim == 2 or dim == 3:
            self.padre.menus.setDim(None,dim)
        else:
            self.padre.ShowMessage("Error","Mesh dimension has not been set (" + str(dim) + ")\n" + info)
            return
        
        resultfile = os.path.join(self.padre.basepath,self.padre.values['namefile'] + '.vtk')
        
        if os.path.isfile(resultfile):   
            self.padre.Plot_Result()
            self.padre.graphicmesh.choice_widget.SetSelection(0)
        # plot the vtk file in the panel
        self.padre.graphicmesh.vtkdatos(filename)
        err = self.padre.graphicmesh.computing_mesh()
        if err:
            os.system('rm -f ' + os.path.join(self.padre.basepath,self.padre.values['namefile'] + '_mesh.vtk'))
            return
        self.padre.graphicmesh.plot_cells()
        self.padre.graphicmesh.mapper_mesh()
        

        self.padre.graphicmesh.renderthis()
        
        # put info in the graphic window and set values
        self.texto = "Number of Elements: " + str(self.padre.graphicmesh.nt)
        self.textopunto = "Number of Nodes: " + str(self.padre.graphicmesh.np)

        
        aux = self.padre.graphicmesh.bounds
        
        textbounds = "Bounding Box: ({0:5.2f},{1:5.2f}) x ({2:5.2f},{3:5.2f})".format(aux[0],aux[1],aux[2],aux[3]) 
        if self.padre.values['dimension'] == 3:
            textbounds += " x ({0:5.2f},{1:5.2f})".format(aux[4],aux[5])
        self.textbounds = textbounds
        measure = list(map(lambda x:pow(x,1./self.padre.values['dimension']),self.padre.graphicmesh.quality))
        self.qualities = 'Measures: ' + ', '.join(list(map('{:7.5f}'.format,measure)))
        
        self.shownt.SetLabel(self.texto)
        self.shownp.SetLabel(self.textopunto)
        self.showbounds.SetLabel(self.textbounds)
        self.showquality.SetLabel(self.qualities)
        
        
        self.padre.graphicmesh.desplegable.Enable(True)
        self.padre.graphicmesh.desplegable.SetSelection(0)
        
        self.innercombo.Enable(True)
        self.boundcombo.Enable(True)
        
        # fill boundaries and inner label combo's
        self.innercombo.Clear()
        self.innerlabel = list(map(str,list(self.padre.graphicmesh.innerlabels)))
        self.innercombo.SetItems(self.innerlabel)
        
        self.boundcombo.Clear()
        self.boundlabel = list(map(str,list(self.padre.graphicmesh.boundarylabels)))
        self.boundcombo.SetItems(self.boundlabel)
        
        # hack to activate the plot (it has to be clicked with the mouse)

        self.padre.graphicmesh.Refresh()
        self.padre.graphicmesh.widget.Render()
        
        self.Refresh()
        

