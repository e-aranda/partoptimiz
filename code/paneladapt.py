 # -*- coding: utf-8 -*-
"""
Created on Thu Jul 27 11:50:38 2017

@author: earanda
"""
import wx
import panelentry
import procesorcpp
import os
from dictcode import adaptacion as d
import pexpect
import time,datetime
import windowvtk


# ---------------------------------------------------------------------------   
class MyPanelAdapt(wx.Panel):
    """
    Panel for create a bash process for mesh adaptation.
    The entries are two list of filters and iterations (they must have
    the same length). The process button generates freefem files fichero{n}.edp
    for n in range(n,length of entries) for computation and adaptation{n}.edp 
    (one less) for adaptation meshes
    """
    def __init__(self,parent,padre,title):
        wx.Panel.__init__(self, parent, style=wx.BORDER_SUNKEN)
        m_sizer = wx.BoxSizer(wx.VERTICAL)
        self.padre = padre
        self.number_of_adaptation = 0
        
        self.panelent = panelentry.MyPanelEntry(self,padre,["Filters for adaptation","Iterations for adaptations","Adaptation node factor"],\
                                      ["adapt_filters","adapt_iters","adapt_nbvx"],title)

        m_sizer.Add(self.panelent,0, wx.TOP|wx.EXPAND , 1)                            
         
        # creating button panel
        paneldos = wx.Panel(self,-1)
        sizerdos = wx.BoxSizer(wx.HORIZONTAL)
        btn = wx.Button(paneldos,-1, 'Process')
        sizerdos.Add(btn, 1, wx.TOP, 0)        

        paneldos.SetSizer(sizerdos)
        m_sizer.Add(paneldos,0,wx.ALL|wx.CENTER,5)#        
        
        # panel for terminal box
        panel = wx.Panel(self,-1)
        b_sizer = wx.BoxSizer(wx.VERTICAL)
        
        style = wx.EXPAND|wx.ALL|wx.TE_MULTILINE|wx.TE_READONLY|wx.VSCROLL|wx.TE_RICH2
        self.log = wx.TextCtrl(panel,-1,style=style,size=(200,300))
        b_sizer.Add(self.log, 0, wx.ALL|wx.EXPAND, 5)
        panel.SetSizer(b_sizer)
        
        m_sizer.Add(panel,0,wx.TOP|wx.EXPAND,1)    
 
    
        # Buttons for running
                
        # panel for buttons
        paneltres = wx.Panel(self,-1)
        sizertres = wx.BoxSizer(wx.HORIZONTAL)
        botonrun = wx.Button(paneltres,-1, 'Run')
        botonstop = wx.Button(paneltres, -1, 'Stop')

        sizertres.Add(botonrun, 1, wx.TOP, 0)
        sizertres.Add(botonstop,1,wx.TOP,0)
        
        self.choices = []
        self.resultados = wx.ComboBox(paneltres,-1, choices = self.choices, style = wx.ALIGN_CENTRE)
        sizertres.Add(self.resultados,1,wx.TOP,0)      
        self.resultados.Enable(False)
  

        m_sizer.Add(paneltres,0,wx.TOP|wx.ALIGN_CENTER,1)

        paneltres.SetSizer(sizertres)
        
      
            
        # Main sizer
        self.SetSizer(m_sizer)

        # Bindings
        self.Bind(wx.EVT_BUTTON, self.onRun, botonrun)
        self.Bind(wx.EVT_BUTTON, self.onStop, botonstop)                
        self.Bind(wx.EVT_BUTTON, self.onButton, btn)
        self.Bind(wx.EVT_COMBOBOX,self.show_results,self.resultados)
        
    
    def onButton(self,event):
        """
        Processing button: creating FreeFem++ files.
        Previously, _temp.topt file is saved for the first time we use 
        this panel, and loaded in other case.
        """
        
        self.cleaner()
        
        a,b,c = self.check()
        self.number_of_adaptation = len(a)
    
        self.nombre = [['fichero'+str(i)+'.edp', 'adaptmesh'+str(i)+'.msh', 
                   os.path.join(self.padre.basepath,'resulpure'+str(i)+'.solb'),
                   os.path.join(self.padre.basepath,'resulfilter'+str(i)+'.solb'),
                   os.path.join(self.padre.basepath,'soladapt'+str(i)+'.solb'),'adaptation'+str(i)+'.edp'] \
                  for i in range(len(a))]
        
        # backup original data
        self.namefile = self.padre.values['namefile']
        self.initfile = self.padre.values['init_file']
        self.meshfile = self.padre.values['meshfile']
        self.filter = self.padre.values['filter']
        self.maxiter = self.padre.values['maxiter']
        self.resp = self.padre.values['resultadopure']
        self.resf = self.padre.values['resultadofilter']
        for i,x in enumerate(zip(a,b)):
            if i:
                self.padre.values['init_file'] = self.nombre[i-1][4]
                self.padre.values['meshfile'] = self.nombre[i][1]
                self.padre.values['namefile'] = 'adaptmesh'+str(i)
                self.nombre[i][1] = os.path.normpath(os.path.join(self.padre.mainpath,self.padre.values['path'],self.nombre[i][1]))

            else:
                self.padre.values['init_file'] = ''
                self.nombre[i][1] = os.path.normpath(os.path.join(self.padre.mainpath,self.padre.values['path'],self.padre.values['meshfile']))
                mpath = self.padre.mainpath
                bpath = self.padre.basepath
                
            self.padre.values['filter'] = x[0]
            self.padre.values['maxiter'] = x[1]
            self.padre.values['resultadopure'] = self.nombre[i][2]
            self.padre.values['resultadofilter'] = self.nombre[i][3]
            
 
            foo = procesor.Processor(mainpath = mpath, basepath=bpath, fname=self.nombre[i][0])
            procesor.showmessage = print
            foo.dothejob(self.padre.values)
            if i != len(a)-1:
                adapt = d.format(malla=self.nombre[i][1],initsol=self.nombre[i][3],initpuresol=self.nombre[i][2],
                             mallaadaptada=os.path.abspath(os.path.normpath(os.path.join(self.padre.mainpath,
                            self.padre.values['path'],self.nombre[i+1][1]))),solucionadaptada=self.nombre[i][4],nbvx=c[i])
                with open(self.nombre[i][5],'w') as fich:
                    fich.write(adapt)
        self.backup()
                    
    def backup(self):
        
        self.padre.values['namefile'] = self.namefile
        self.padre.values['init_file'] = self.initfile
        self.padre.values['meshfile'] = self.meshfile
        self.padre.values['filter'] = self.filter 
        self.padre.values['maxiter'] = self.maxiter
        self.padre.values['resultadopure'] = self.resp
        self.padre.values['resultadofilter'] = self.resf

    def onStop(self, evt):
        """
        set variable to stop line printing
        """
        self.stoper = False

    def onRun(self, evt):
                
        self.log.SetValue("")
        os.environ['OMP_NUM_THREADS'] = '1'
        self.start_time = time.time()

        for i in range(self.number_of_adaptation):
            
            cmd = 'FreeFem++ -ne fichero' + str(i) + '.edp ' 
            self.log.AppendText(cmd+'\n\n\n')
            
            self.padre.graphicbot.Plot_Mesh(self.nombre[i][1])
            
            child = pexpect.spawn(cmd,timeout=None)
            self.stoper = True
        
            # Loop for printings exit lines of running program
            while self.stoper:
                wx.Yield()
                try:
                    child.expect('\n')
                    self.log.AppendText(child.before)
                    if b'PROCESS ENDED' in child.before:
                        self.stoper = False
                except pexpect.EOF:
                    break
            if i+1 == self.number_of_adaptation:
                self.padre.Plot_Result()
                break
            else:
                cmd = 'FreeFem++ -ne adaptation' + str(i) + '.edp ' 
                child = pexpect.spawn(cmd,timeout=None)
                self.log.AppendText(cmd+'\n\n\n')
                
                self.stoper = True
        
                # Loop for printings exit lines of running program
                while self.stoper:
                    wx.Yield()
                    try:
                        child.expect('\n')
                        self.log.AppendText(child.before)
                        if b'ADAPTATION ENDED' in child.before:
                            self.stoper = False
                    except pexpect.EOF:
                        break
    
            self.padre.Plot_Result()
            self.padre.values['namefile'] = 'adaptmesh'+str(i+1)
            

        seconds = (time.time() - self.start_time)
        tim = str(datetime.timedelta(seconds=seconds))
        finaltime = "--- Total Time: {0} ---".format(tim)
        self.log.AppendText('\n\n' + finaltime)
        
        self.choices[:] = []
        self.combo()
        self.backup()
                
        self.padre.graphicbot.desplegable.Enable(False)

        
        
    def combo(self):
        self.resultados.Enable(True)
        self.choices.extend(['Original Mesh','Result 1'])
        for i in range(self.number_of_adaptation-1):
            self.choices.extend(['Mesh Adaptation '+str(i+1), 'Result '+str(i+2)])
        self.resultados.SetItems(self.choices)
        

    def check(self):
        a = self.padre.values['adapt_filters'].split()
        b = self.padre.values['adapt_iters'].split()
        c = self.padre.values['adapt_nbvx'].split()
        if a and len(a) != 1 and len(a) == len(b):
            if len(a) == len(c)+1:
                return a,b,c
            elif len(c) == 1:
                c = c*(len(a)-1)
                return a,b,c
        self.padre.ShowMessage("Error","No adaptation process can be done")
        return [],[],[]
        
    def show_results(self,event):
        pos = self.resultados.GetSelection()
        if pos ==  0:
            filename = self.namefile+'_mesh.vtk'
        elif pos == 1:
            filename = self.namefile+'.vtk'
        elif not pos%2:
            filename = 'adaptmesh'+str(pos//2) + '_mesh.vtk'
        else:
            filename = 'adaptmesh'+str(pos//2) + '.vtk'
        pathfile = os.path.join(self.padre.basepath,filename)
        
        help_frame = windowvtk.NewFrame(self,self.choices[pos],pathfile,pos,self.padre)
        help_frame.Show()

    def cleaner(self):
        cmd = 'rm -f {path}'
        path = os.path.join(self.padre.basepath,'*.vtk')
        cmd = cmd.format(path=path)
        os.system(cmd)