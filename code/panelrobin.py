"""
Created on Thu Jul 27 11:58:59 2017

@author: earanda
"""

import wx
import paneltitle
import os
import wx.grid as  gridlib
import panelentry
        
class MyPanelRobin(panelentry.MyPanelEntry):
    """
    Panel containing grid with Robin conditions (matrix and force)
    """
    def __init__(self,parent,padre,titulo,var):
        wx.Panel.__init__(self, parent, style=wx.BORDER_SUNKEN)    
        self.padre = padre
        self.var = var
        
        # Sizers
        self.m_sizer = wx.BoxSizer(wx.VERTICAL)
        
        # title panel
        title = paneltitle.MyPanelTitle(self,titulo)

        # add button panel
        addbot = wx.Panel(self)        
        tot = wx.StaticText(addbot,-1,"Add new")
        CURRENT_DIR = os.path.dirname(__file__)
        pngfile = os.path.join(CURRENT_DIR, os.path.join('icons','list-add.png'))   
        png = wx.Image(pngfile, wx.BITMAP_TYPE_PNG).ConvertToBitmap()
        bot = wx.BitmapButton(addbot, -1, png)
        bot.SetBackgroundColour(wx.Colour(240, 240, 240))       
        bot.Bind(wx.EVT_BUTTON, self.addrow)
        
        b_sizer = wx.BoxSizer(wx.HORIZONTAL)
        # adding to sizers
        b_sizer.Add(tot,2,wx.RIGHT|wx.ALIGN_CENTER_VERTICAL,1) 
        b_sizer.Add(bot,1, wx.RIGHT,5)
        # set sizer        
        addbot.SetSizer(b_sizer)
        
        
        self.paneles = []
        self.panel_sizer = []
        
        self.matrix = []
        self.force = []
        self.label = []
        self.botones = []
        
        delfile = os.path.join(CURRENT_DIR, os.path.join('icons','trash.png'))   
        self.borrar = wx.Image(delfile, wx.BITMAP_TYPE_ANY).ConvertToBitmap()
         
        # add Robin's
        self.i = 0 # number of Robin conditions

        self.paneles.append(wx.Panel(self))
        # add panel labels 
        n1 = wx.StaticText(self.paneles[0],-1,"Spring Stiffness Matrix")
        n2 = wx.StaticText(self.paneles[0],-1,"Force")
        n3 = wx.StaticText(self.paneles[0],-1,"Label")
        
        self.panel_sizer.append(wx.BoxSizer(wx.HORIZONTAL))

        self.panel_sizer[0].Add(n1,3,wx.ALL|wx.ALIGN_CENTER_VERTICAL,5)
        self.panel_sizer[0].Add(n2,1,wx.ALL|wx.ALIGN_CENTER_VERTICAL,5)
        self.panel_sizer[0].Add(n3,1,wx.ALL|wx.ALIGN_CENTER_VERTICAL,5)
        
        self.paneles[0].SetSizer(self.panel_sizer[0])

        self.m_sizer.Add(title,0, wx.TOP|wx.EXPAND,1)
        self.m_sizer.Add(addbot,0, wx.ALIGN_RIGHT,15)
        self.m_sizer.Add(self.paneles[0], 0, wx.TOP|wx.EXPAND, 5)
        
        self.SetSizer(self.m_sizer)
#        self.addrow(None)
        


    def addrow(self,event):
        
        self.i += 1
        self.paneles.append(wx.Panel(self))
        self.panel_sizer.append(wx.BoxSizer(wx.HORIZONTAL))
        
        self.matrix.append(wx.grid.Grid(self.paneles[-1]))
        self.matrix[-1].CreateGrid(3,3)
        self.matrix[-1].SetRowLabelSize(1)
        self.matrix[-1].SetColLabelSize(1)

        self.force.append(wx.grid.Grid(self.paneles[-1]))
        self.force[-1].CreateGrid(3,1)
        self.force[-1].SetRowLabelSize(1)
        self.force[-1].SetColLabelSize(1)
        self.label.append((wx.TextCtrl(self.paneles[-1],-1,'',size=(40,-1),style=wx.TE_PROCESS_ENTER|wx.TE_RICH)))     
        
        self.botones.append(wx.BitmapButton(self.paneles[-1], -1, self.borrar))
        self.botones[-1].SetBackgroundColour(wx.Colour(240, 240, 240))  
        
        self.panel_sizer[-1].Add(self.matrix[-1], 3,flag=wx.ALL,border=5)
        self.panel_sizer[-1].Add(self.force[-1],1,flag=wx.ALL,border=5)
        self.panel_sizer[-1].Add(self.label[-1],1,flag=wx.ALL,border=5)
        self.panel_sizer[-1].Add(self.botones[-1],flag=wx.ALL,border=5)
        self.paneles[-1].SetSizer(self.panel_sizer[-1])
    
        self.m_sizer.Add(self.paneles[-1], 0, wx.TOP|wx.EXPAND, 0)
        self.Layout()
        
        self.bindboton()
        
        self.padre.values['robin'].append([[['','',''],['','',''],['','','']],['','',''],''])
        
        self.set_dim(self.padre.values['dimension'])

    def bindboton(self):
        for i,xx in enumerate(self.matrix):
            self.botones[i].Bind(wx.EVT_BUTTON, 
                        lambda event, x=i: self.delrow(event,x))
            xx.Bind(gridlib.EVT_GRID_CELL_CHANGED, 
                       lambda event, x=i: self.OnCellMatrixChange(event,x))
            self.force[i].Bind(gridlib.EVT_GRID_CELL_CHANGED, 
                       lambda event, x=i: self.OnCellForceChange(event,x))
            self.label[i].Bind(wx.EVT_TEXT, 
                      lambda event, ind=i: self.set_rval(event,ind))
        

    def delrow(self,event,j):
        
        self.i -= 1
        self.matrix[j].Destroy()
        self.force[j].Destroy()
        self.label[j].Destroy()
        self.botones[j].Hide()
        
        self.matrix.pop(j)
        self.force.pop(j)
        self.label.pop(j)
        self.botones.pop(j)
        
        self.bindboton()
        self.Refresh()
        self.Layout()
        
        self.padre.values['robin'].pop(j)
        
    def OnCellMatrixChange(self, evt, j):
        """
        Update value in matrix grid
        """
        self.padre.values[self.var][j][0][evt.GetRow()][evt.GetCol()] = \
            self.matrix[j].GetCellValue(evt.GetRow(), evt.GetCol())     
            
    def OnCellForceChange(self, evt, j):
        """
        Update value in force grid
        """
        self.padre.values[self.var][j][1][evt.GetRow()] = \
            self.force[j].GetCellValue(evt.GetRow(), 0)     
        
    def set_rval(self,event,i):
        """
        Event for edition of data entries. Set values in MainFrame
        Connected to MainFrame
        """
        self.padre.values[self.var][i][2] = event.EventObject.GetValue()
        event.EventObject.SetForegroundColour(wx.BLACK)
        
    def set_dim(self,dim):
        """
        Change access to 3rd coordinate in case of changing dim
        """
        if dim == 2:
            for j in range(self.i):
                for i in range(3):
                    self.matrix[j].SetCellValue(i,2,'')
                    self.matrix[j].SetCellBackgroundColour(i, 2, (225,225,225))
                    self.matrix[j].SetReadOnly(i,2,True)
                for i in range(2):
                    self.matrix[j].SetCellValue(2,i,'')
                    self.matrix[j].SetCellBackgroundColour(2,i, (225,225,225))
                    self.matrix[j].SetReadOnly(2,i,True)
                self.force[j].SetCellValue(2,0,'')
                self.force[j].SetCellBackgroundColour(2,0,(225,225,225))
                self.force[j].SetReadOnly(2,0,True)     
        else:
            for j in range(self.i):
                for i in range(3):
                    self.matrix[j].SetCellBackgroundColour(i, 2, (255,255,255))
                    self.matrix[j].SetReadOnly(i,2,False)
                for i in range(2):
                    self.matrix[j].SetCellBackgroundColour(2,i, (255,255,255))
                    self.matrix[j].SetReadOnly(2,i,False)
                self.matrix[j].Refresh()
                self.force[j].SetCellBackgroundColour(2,0,(255,255,255))
                self.force[j].SetReadOnly(2,0,False)     
                self.force[j].Refresh()
            
            
