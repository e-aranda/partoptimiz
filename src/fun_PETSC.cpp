#include "Parfunctions.hpp"

#ifndef TOPTIMIZ_PETSC
#define TOPTIMIZ_PETSC


// Initialization

void ToptimizPETSC::Initialization()
{
    movelim = 0.2;
    Xmax = 1.;
    n = fespace->GlobalTrueVSize();
    m = 1;
    
    df0dx = HypreParVector(fespace);
    res = Vector(m);
    dres = HypreParVector(fespace);
    const char *petscrc_file = "";
    
    MFEMInitializePetsc(NULL,NULL,petscrc_file,NULL);
   
    xini = new PetscParVector(fespace);
    xini->SetVector(*Rho,0);
    VecDuplicate(*xini,&xold);

    xmax = new PetscParVector(fespace);
    *xmax = Xmax;
  
    xmin = new PetscParVector(fespace);
    *xmin = 0.0;

    lb= new PetscParVector(fespace);
    lb->SetVector(*Cotas,0);
     
    df = new PetscParVector(fespace);
    df->SetVector(df0dx,0);

    DFDX = new PetscParVector(fespace);
    DFDX->SetVector(dres,0);

    VecDuplicateVecs(*DFDX,m,&dgdx);
    VecCopy(*DFDX, *dgdx);
    
    Rho->SetData(xini->GetData());

    mma = new MMA(n, m, *xini);
}


void ToptimizPETSC::Iteration(int &maxiter, double &tol)
{
  // main iteration of MMA PETSC
  siter = 0;
  double change = 1.;
  double global_volumen = 0.;

  if (!filtro->boolheaviside)
      {
       filtro->FilMult(*dconres,dres);
       dres *= Volconstant;
       DFDX->SetVector(dres,0);
       VecCopy(*DFDX, *dgdx);
      }


  while ( (siter<maxiter) && (change > tol ))
    {
      siter++;
      global_compliance = 0.;
      // evaluation
      $filtering$
      $extra$

       $density$
       // objective
       double  compliance = Elasticity(a,b,Udisp,ess_tdof_list);

       MPI_Allreduce(&compliance, &global_compliance, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
       double obj_value = Mobj*global_compliance;

       // constraint
       vol->Update();
       vol->Assemble();
	  
       double volumen = vol->Sum();
       MPI_Allreduce(&volumen, &global_volumen, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
       res(0) = global_volumen*Volconstant - 1.;

       // derivatives
       der->Update();
       der->Assemble();
       filtro->FilMult(*der,df0dx);
       df0dx *= Mobj;
       df->SetVector(df0dx,0);
      
       $vectorD_PETSC$
 
       // printing iteration results
      if (myid == 0)
	{
	  cout << "Iteration:  " << siter << "  Obj: " << obj_value <<
	" (" << global_compliance << ")  Restr: " << res(0) << endl; 
	}

       
       GlVisit(siter,global_compliance,"Iteration","Objective");

       // MMA iteration      
       VecCopy(*xini,xold);
       mma->SetOuterMovelimit(*lb,Xmax,movelim,*xini,*xmin,*xmax);
       mma->Update(*xini,*df,res.GetData(),dgdx,*xmin,*xmax);

       change =  mma->DesignChange(*xini,xold);
       
       $betabucle$

        Rho->SetData(xini->GetData());
    }
  
}


// Volume problem

void ToptimizPETSC::IterationVol(int &maxiter, double &tol)
{
  // main iteration of MMA PETSC
  siter = 0;
  double change = 1.;
  double global_compliance = 0.;

  
  if (!filtro->boolheaviside)
      {
       filtro->FilMult(*dconres,df0dx);
       df0dx *= Volconstant;
       df->SetVector(df0dx,0);
      }



  while ( (siter<maxiter) && (change > tol ))
    {
      siter++;
      // evaluation
      $filtering$
      $extra$


       // objective
       vol->Update();
       vol->Assemble();

       global_volumen = 0.;
       double volumen = vol->Sum();
       MPI_Allreduce(&volumen, &global_volumen, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
       global_volumen *=Volconstant ;

       // gradient
       $vectorY_PETSC$


	 
	// compliance constraint
	 
	// solving elasticity system
	double  compliance = Elasticity(a,b,Udisp,ess_tdof_list);

        MPI_Allreduce(&compliance, &global_compliance, 1, MPI_DOUBLE, MPI_SUM,  MPI_COMM_WORLD);
        res(0) = Mobj*global_compliance - 1; 

        der->Update();
        der->Assemble();
        filtro->FilMult(*der,dres);
        dres *= Mobj;
        DFDX->SetVector(dres,0);
        VecCopy(*DFDX,*dgdx);
	
 
       // printing iteration results
      if (myid == 0)
	{
	  cout << "Iteration:  " << siter << "  Obj: " <<  global_volumen << "  Restr: " << res(0) << endl; 
	}

       
       GlVisit(siter,global_volumen,"Iteration","Volume");

       // MMA iteration      
       VecCopy(*xini,xold);
       mma->SetOuterMovelimit(*lb,Xmax,movelim,*xini,*xmin,*xmax);
       mma->Update(*xini,*df,res.GetData(),dgdx,*xmin,*xmax);

       change =  mma->DesignChange(*xini,xold);
       
       $betabucle$

        Rho->SetData(xini->GetData());
    }
  
}


// Mechanism problem

void ToptimizPETSCMec::Iteration(int &maxiter, double &tol)
{
  // main iteration of MMA PETSC
  siter = 0;
  double change = 1.;
  double global_volumen = 0.;

  if (!filtro->boolheaviside)
      {
       filtro->FilMult(*dconres,dres);
       dres *= Volconstant;
       DFDX->SetVector(dres,0);
       VecCopy(*DFDX, *dgdx);
      }


  while ( (siter<maxiter) && (change > tol ))
    {
      siter++;
      // evaluation
      $filtering$
      $extra$

       // objective
	global_compliance = 0.;
	double  compliance = ElasticityMec(a,b,vmec,Udisp,ess_tdof_list);

       MPI_Allreduce(&compliance, &global_compliance, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
       double obj_value = Mobj*global_compliance;

       // constraint
       vol->Update();
       vol->Assemble();
	  
       double volumen = vol->Sum();
       MPI_Allreduce(&volumen, &global_volumen, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
       res(0) = global_volumen*Volconstant - 1.;

       $vectorD_PETSC$

       // derivatives
       compliance = Elasticity(a,mc,Qdisp,ess_tdof_list);
       der->Update();
       der->Assemble();
       filtro->FilMult(*der,df0dx);
       df0dx *= Mobj;
       df->SetVector(df0dx,0);
      
 
       // printing iteration results
      if (myid == 0)
	{
	  cout << "Iteration:  " << siter << "  Obj: " << obj_value <<
	" (" << global_compliance << ")  Restr: " << res(0) << endl; 
	}

       
       GlVisit(siter,global_compliance,"Iteration","Objective");

       // MMA iteration      
       VecCopy(*xini,xold);
       mma->SetOuterMovelimit(*lb,Xmax,movelim,*xini,*xmin,*xmax);
       mma->Update(*xini,*df,res.GetData(),dgdx,*xmin,*xmax);

       change =  mma->DesignChange(*xini,xold);
       
       $betabucle$

        Rho->SetData(xini->GetData());
    }
  
}





void ToptimizPETSCML::Initialization()
{
    movelim = 0.2;
    Xmax = 1.;
    n = fespace->GlobalTrueVSize();
    m = 1;
    
    df0dx = HypreParVector(fespace);
    res = Vector(m);
    dres = HypreParVector(fespace);
    const char *petscrc_file = "";
    
    MFEMInitializePetsc(NULL,NULL,petscrc_file,NULL);
   
    xini = new PetscParVector(fespace);
    xini->SetVector(*Rho,0);
    VecDuplicate(*xini,&xold);

    xmax = new PetscParVector(fespace);
    *xmax = Xmax;
  
    xmin = new PetscParVector(fespace);
    *xmin = 0.0;

    lb= new PetscParVector(fespace);
    lb->SetVector(*Cotas,0);
     
    df = new PetscParVector(fespace);
    df->SetVector(df0dx,0);

    DFDX = new PetscParVector(fespace);
    DFDX->SetVector(dres,0);

    VecDuplicateVecs(*DFDX,m,&dgdx);
    VecCopy(*DFDX, *dgdx);
    
    Rho->SetData(xini->GetData());

    mma = new MMA(n, m, *xini);
}





void ToptimizPETSCML::Iteration(int &maxiter, double &tol)
{
  // main iteration of MMA PETSC
  siter = 0;
  double change = 1.;
  double global_volumen = 0.;

  if (!filtro->boolheaviside)
      {
       filtro->FilMult(*dconres,dres);
       dres *= Volconstant;
       DFDX->SetVector(dres,0);
       VecCopy(*DFDX, *dgdx);
      }


  while ( (siter<maxiter) && (change > tol ))
    {
      siter++;
      // evaluation
      $filtering$
      $extra$

       // objective
      global_compliance = 0.;
      a->Update();
      a->Assemble();
      for (int i=0; i<n_f; i++)
	{
	  double g_compliance;
	  double compliance = Elasticity(a,b[i],Udisp[i],ess_tdof_list,false);
	  MPI_Allreduce(&compliance, &g_compliance, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
	  global_compliance += lambdacoef[i]*g_compliance;
	}
      double obj_value = Mobj*global_compliance;
  
       // constraint
       vol->Update();
       vol->Assemble();
	  
       double volumen = vol->Sum();
       MPI_Allreduce(&volumen, &global_volumen, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
       res(0) = global_volumen*Volconstant - 1.;



       // computation of derivative
       Vector Ys[n_f];
       for (int i=0; i<n_f; i++)
	 {
	   Ys[i].SetSize(n);
	   der[i]->Update();
	   der[i]->Assemble();
	   filtro->FilMult(*der[i],Ys[i]);
	 }
       
       df0dx = 0.;
       for (int i=0; i<n; i++)
	 {
	   for (int k=0; k<n_f; k++)
	     df0dx(i) += Mobj*lambdacoef[k]*Ys[k](i);
	 }
 
       df->SetVector(df0dx,0);
      
       $vectorD_PETSC$
 
       // printing iteration results
      if (myid == 0)
	{
	  cout << "Iteration:  " << siter << "  Obj: " << obj_value <<
	" (" << global_compliance << ")  Restr: " << res(0) << endl; 
	}

       
       GlVisit(siter,global_compliance,"Iteration","Weighted Objective");

       // MMA iteration      
       VecCopy(*xini,xold);
       mma->SetOuterMovelimit(*lb,Xmax,movelim,*xini,*xmin,*xmax);
       mma->Update(*xini,*df,res.GetData(),dgdx,*xmin,*xmax);

       change =  mma->DesignChange(*xini,xold);
       
       $betabucle$

        Rho->SetData(xini->GetData());
    }
  
}


#endif
