#include "Parfunctions.hpp"
$include$

$headers$

int main(int argc, char *argv[])
  {
    
    // initialize MPI.
    int num_procs, myid;
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &num_procs);
    MPI_Comm_rank(MPI_COMM_WORLD, &myid);
    double Inicio;
    Inicio = MPI_Wtime();
    if (myid == 0)
      cout << "PROCESS STARTED \n" << endl;
    
    bool visualization = 1; // set visualization
    double Volfrac = 0.5; // Volume fraction
    double radius = 3.5; // Filter radius
    int maxiter = 500; // maximum iterations
    double tol = 1.e-8; // tolerance
    int iterbeta = 3; // Heaviside loops
    double maxbeta = 64.;
    double betaup = 2.; // update factor of Heaviside beta
    int betaloop = 200;
    double beta = 1.0, eta = 0.5; // for Heaviside projections
    double zeta = 0.2, neta = 0.5; // for OC
    double Initcte = 1.; // initialization
    
    const char *mesh_file = "$mesh$";

    $gravity$

    // reading data
    OptionsParser args(argc, argv);
    args.AddOption(&visualization, "-vis", "--visualization", "-no-vis",
                  "--no-visualization",
                  "Enable or disable GLVis visualization.");
    args.AddOption(&radius, "-r", "--radius",
                  "Filter radius");
    args.AddOption(&Volfrac, "-v", "--volfraction",
                  "Volume/Compliance fraction or Maximum Stress");
    args.AddOption(&maxiter, "-i", "--iter",
                  "Maximum number of iterations");
    args.AddOption(&tol, "-t", "--tolerance",
                  "Maximum tolerance (stopping criteria)");
    args.AddOption(&iterbeta, "-ib", "--iterbeta",
                  "Number of loops for changing beta");    
    args.AddOption(&betaup, "-bup", "--betaupdate",
                  "Factor for updating beta parameter");
    args.AddOption(&maxbeta, "-mb", "--maxbeta",
                  "Maximum value for updating beta parameter");
    args.AddOption(&betaloop, "-bl", "--betaloops",
                  "Iteration starting beta update");
    args.AddOption(&beta, "-b", "--beta",
                  "Beta parameter for Heaviside projection");
    args.AddOption(&eta, "-e", "--eta",
                  "Eta parameter for Heaviside projection");
    args.AddOption(&zeta, "-z", "--zeta",
                  "Zeta parameter for optimality conditions");
    args.AddOption(&neta, "-n", "--neta",
                  "Eta parameter for optimality conditions");
    args.AddOption(&Initcte, "-s", "--start",
		   "Initial constant for density");

    args.Parse();
    if (!args.Good())
      {
	args.PrintUsage(cout);
	MPI_Finalize();
	return 1;
      }

    // read the (serial) mesh from the given mesh file on all processors.
    Mesh *mesh = new Mesh(mesh_file);
    int dim = mesh->Dimension();
    int meshel = mesh->GetNE();
    // if (myid == 0) cout << "Number of elements: " << meshel << endl;

    // create parallel mesh
    ParMesh *pmesh = new ParMesh(MPI_COMM_WORLD, *mesh);
    delete mesh;

    // finite element spaces
    FiniteElementCollection *fec, *fec0;
    ParFiniteElementSpace *p1espace, *p0espace;
    fec = new H1_FECollection(1, dim);
    fec0 = new L2_FECollection(0, dim);
    p1espace = new ParFiniteElementSpace(pmesh, fec, dim, Ordering::byVDIM);
    p0espace = new ParFiniteElementSpace(pmesh, fec0, Ordering::byVDIM);


    // Functions    
    ConstantCoefficient one(1.0);
    // GridFunction for densities
    ParGridFunction *Rhobar;
    Rhobar = new ParGridFunction(p0espace);
    Rhobar->ProjectCoefficient(one);

    // GridFunction for displacements
    $displacements$
      
    // auxiliar function for adjoint problem (Mech/Stress Prob. only)
    $qdisp$ 

    // Linear form to compute volume
    ParLinearForm *vol;
    vol = new ParLinearForm(p0espace);
    GridFunctionCoefficient R(Rhobar);
    vol->AddDomainIntegrator(new DomainLFIntegrator(R));
    vol->Assemble();

    // computation of total volume
    double medida;
    double global_medida;
    medida = vol->Sum();
    MPI_Allreduce(&medida, &global_medida, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
    if (myid == 0) cout << "Total volume: " << global_medida << endl;

    $projectRhobar$

    // problem data

    // Dirichlet conditions
    int natr = pmesh->bdr_attributes.Max();      
    Array<int>ess_tdof_list;
    Array<int> ess_bdr(natr);
    Array<int> etiquetas;


    // *****************
    // Elasticity system
    // *****************

    // Dirichlet boundary conditions
    $dirichlet$

    // Right hand side for elasticity system
    $RHSdef$

    $linearform$

    $gravity_force$

    $assamblingb$

    $mechanism$

    
    // Bilinear form for elasticity problem
    ParBilinearForm *a;
    a = new ParBilinearForm(p1espace);
    SIMPCoeff E(*Rhobar);

    a->AddDomainIntegrator(new ElasticityIntegrator(E,lambda,mu));

    $matrixform$

    // Solving Elasticity system
    $initialsystem$

    // Initial compliance or displacement
    $initcompliance$

    // Filter definition
    Filter *filtro;
    $initialfilter$

    // constants to pass to the main class
    $mobjs$
    $volconstant$


    // Coefficients
    $prevcoef$  
    $coefficients$
      
    // Linear form to compute derivative of compliance
    $defderivative$

    // self-weight hypothesis
    $gravity_der$
    $assamblingder$

    // Linear form to compute derivative of constraint
    ParLinearForm *dconres;
    dconres = new ParLinearForm(p0espace);
    $dconres$
    dconres->Assemble();

    $passive$

    ParGridFunction *initialization;
    $initialization$

    $method$
     
    $preafter$
      
    $showresult$

    $savesol$

    $vtkwriter$

    $parvtk$


    $threshold$
    $maxstress$
      
    delete Rhobar;
    delete initialization;
    $delete$ 
    $deletearray$
    $del$
    delete filtro;
    delete a;
    delete vol;
    delete dconres;
    delete p1espace;
    delete p0espace;
    delete fec;
    delete fec0;
    delete pmesh;

    if (myid == 0)
      {
	cout << "Execution Time: " << MPI_Wtime() -Inicio << endl;
	cout << "PROCESS ENDED" << endl;
      }
    
    MPI_Finalize();
    
    return 0;
    
  }


$analyticfuns$
