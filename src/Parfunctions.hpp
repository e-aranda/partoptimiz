#include <string>
using std::string;
#include <ctime>

#include <fstream>
using std::ofstream;
using std::ifstream;

#include <iostream>
using std::cout;
using std::endl;
using std::flush;

#include <vector>
using std::vector;

using std::max;
using std::min;


#include "mfem.hpp"

using namespace mfem;

#ifndef TOPTIMIZ_GNR
#define TOPTIMIZ_GNR

// constants of Toptimiz3D
#include "constants.hpp"
#include "ParConic.hpp"

// ***************************************************
// Coefficient for SIMP interpolation
//
// Only defines eval method 
// ***************************************************
class SIMPCoeff : public Coefficient
{
private:
  GridFunction &r;	
public: 
  SIMPCoeff(GridFunction &_r) : r(_r)	{ }
  virtual double Eval(ElementTransformation &T, const IntegrationPoint &ip);
  virtual ~SIMPCoeff() { }
};

// ***************************************************
// Coefficient for compliance derivative
//
// only define Eval method 
// ***************************************************
class CoeffDerivative : public Coefficient
{
private:
  GridFunction &r;
  GridFunction &U;
  Coefficient &Psip;
  DenseMatrix eps;

public: 
  CoeffDerivative(GridFunction &_r, GridFunction &_u, 
		  Coefficient &_p) : r(_r), U(_u), Psip(_p)
  { }
  virtual double Eval(ElementTransformation &T, const IntegrationPoint &ip);
  virtual ~CoeffDerivative() { }
};

// ***************************************************
// Coefficient for mechanism derivative
//
// only define Eval method 
// ***************************************************
class CoeffDerivativeAdj : public Coefficient
{
private:
  GridFunction &r;
  GridFunction &U;
  GridFunction &Q;
  Coefficient &Psip;
  DenseMatrix eps;
  DenseMatrix qeps;
public: 
  CoeffDerivativeAdj(GridFunction &_r, GridFunction &_u, GridFunction &_q,
		     Coefficient &_p) : r(_r), U(_u), Q(_q), Psip(_p)
  { }
  virtual double Eval(ElementTransformation &T, const IntegrationPoint &ip);
  virtual ~CoeffDerivativeAdj() { }
};

// ***************************************************
// Coefficient for computing the term corresponding to
// the self-weight hypothesis (in right hand side of 
// elasticity system)
//
// only define Eval method 
// ***************************************************
class DensityCoeff : public Coefficient
{
private:
  GridFunction &r;
  double density;
public:
  DensityCoeff(GridFunction &_r, double _d) : 
    r(_r), density(_d) { }
  virtual double Eval(ElementTransformation &T, const IntegrationPoint &ip);
  virtual ~DensityCoeff() { }
};

// ***************************************************
// Coefficient for computing the term corresponding to
// the self-weight hypothesis in the derivative
//
// only define Eval method 
// ***************************************************
class DerDensityCoeff : public Coefficient
{
private:
  GridFunction &U;
  Coefficient &Psip;
  double density;
public:
  DerDensityCoeff(GridFunction &_U, Coefficient &_p, double _d) : 
    U(_U), Psip(_p), density(_d) { }
  virtual double Eval(ElementTransformation &T, const IntegrationPoint &ip);
  virtual ~DerDensityCoeff() { }
};

// ***************************************************
// Class for computing constant for Heaviside projections
//
// Only defines Updateconstant method 
// Base class for Filter and CoeffPsip	
// ***************************************************
class Heaviside
{
public:
  double beta,eta,tb,tbi;
  bool boolheaviside;
  double maxbeta;
  double betaup;
  int iterbeta;
  int betaloop;
  Heaviside(){ boolheaviside = 0;}
  Heaviside(double &_b, double &_e) : beta(_b), eta(_e)
  {
    boolheaviside = 1;
    tbi = 1./(tanh(beta*eta) + tanh(beta*(1.-eta)));
    tb = tanh(beta*eta)*tbi;
  }
  Heaviside(double &_b, double &_e, double & _m, double &_u, int &_i, int &_bu) :
    beta(_b), eta(_e),
    maxbeta(_m), betaup(_u),
    iterbeta(_i),
    betaloop(_bu)
  {
    boolheaviside = 1;
    tbi = 1./(tanh(beta*eta) + tanh(beta*(1.-eta)));
    tb = tanh(beta*eta)*tbi;
  }
  void UpdateConstant(double &b);
  ~Heaviside(){ }
};


// ***************************************************
// Coefficient for volume constraint derivative
// for Heaviside Projections (otherwise is not used)
//
// define Eval method 
// ***************************************************
class CoeffPsip : public Coefficient, public Heaviside
{
private:
  GridFunction &r;
public:
  CoeffPsip(GridFunction &_r, double _beta, double _eta) : 
    r(_r), Heaviside(_beta,_eta){ }
  virtual double Eval(ElementTransformation &T, const IntegrationPoint &ip);
  virtual ~CoeffPsip() { }
};	



// ***********************************************
// Base Class for defining Coefficients for computing 
// Von Mises stress 
//
// virtual method to be implemented:
//	Eval
// ***********************************************
class TensionVonMises : public Coefficient
{
protected:
  GridFunction &u;
  DenseMatrix eps, sigma;
  double l,m;
public:
  TensionVonMises(GridFunction &_u) : u(_u) 
  { l = E1*lambda; m = E1*mu; }
  virtual double Eval(ElementTransformation &T, const IntegrationPoint &ip)=0;
  virtual ~TensionVonMises() { }
};


// ***********************************************
// Derivative classes for defining Coefficients for 
// computing Von Mises stress 
// 3 cases: 2D (plane stress), 2Ddp plane strain, 3D	
//
// define Eval method
// ***********************************************
// ***********************************************
// ***********************************************
class VonMises2D : public TensionVonMises
{
public:
  VonMises2D(GridFunction &_u) : TensionVonMises(_u) { }
  virtual double Eval(ElementTransformation &T, const IntegrationPoint &ip);
  ~VonMises2D(){ }
};

class VonMises2Ddp : public TensionVonMises
{
public:
  VonMises2Ddp(GridFunction &_u) : TensionVonMises(_u) { }
  virtual double Eval(ElementTransformation &T, const IntegrationPoint &ip);
  ~VonMises2Ddp(){ }
};

class VonMises3D : public TensionVonMises
{
public:
  VonMises3D(GridFunction &_u) : TensionVonMises(_u) { }
  virtual double Eval(ElementTransformation &T, const IntegrationPoint &ip);	        
  ~VonMises3D(){ }
};


// ***********************************************
// Class Coefficient for computing the relaxed stress 
// following Le and Tortorelli
//
// define Eval method	
// ***********************************************
class RelaxStress : public Coefficient
{
protected:
  GridFunction &r;
  Coefficient &SVM;	
public:
  RelaxStress(GridFunction &_r, Coefficient &_s): r(_r), SVM(_s) { } 
  virtual double Eval(ElementTransformation &T, const IntegrationPoint &ip);
  virtual ~RelaxStress() { }
};

// ***********************************************
// Class Coefficient for computing the stress 
//
// define Eval method	
// ***********************************************
class Stress : public Coefficient
{
protected:
  Coefficient &r;
  Coefficient &SVM;	
public:
  Stress(Coefficient &_r, Coefficient &_s): r(_r), SVM(_s) { } 
  virtual double Eval(ElementTransformation &T, const IntegrationPoint &ip);
  virtual ~Stress() { }
};




// ***************************************************
// Abstract class for filters
//
// virtual methods to be implemented:
//	Filtering -> filter without Heaviside projection
//  FilteringWH -> filter with Heaviside projection
//  FilMult	-> Correction filter for derivatives
// ***************************************************
class Filter : public Heaviside
{
public:
  ParMesh *pmesh;
  const double radius;
  double ep;
  Filter(ParMesh *m, const double r) : 
    pmesh(m), radius(r) 
  { 
    ep = radius*radius;
  }
  Filter(ParMesh *m, const double r, double &_beta, double &_eta, double &_m, double &_u, int &_i, int &_bu) : 
    pmesh(m), radius(r), Heaviside(_beta,_eta,_m,_u,_i, _bu)  
  { 
    ep = radius*radius;
  }
  virtual void Filtering(ParGridFunction *rho, ParGridFunction &r) = 0;
  virtual void FilteringWH(ParGridFunction *rho, ParGridFunction &r);
  virtual void FilMult(const Vector &x, Vector &y) = 0; 
  ~Filter(){ }
};




// ***************************************************
// Helmholtz Filter
//
// given $\rho$, solves:
//        $-r^2\Delta u + u = \rho in \Omega$
//        $\frac{\partial u}{\partial n} = 0 in \partial\Omega$
// ***************************************************
class HelmholtzFilter : public Filter
{
public:
  FiniteElementCollection *fec1, *fec0;
  ParFiniteElementSpace *p1espace, *p0espace;
  HypreParMatrix A;
  HypreParMatrix *PI;
  HypreParMatrix *B;
  HyprePCG solver;

  HelmholtzFilter(ParMesh *m, const double r, 
		  FiniteElementCollection *_f1, FiniteElementCollection *_f0) : 
    Filter(m,r), fec1(_f1), fec0(_f0), solver(m->GetComm())
  { 
    Initializer();
  }

  HelmholtzFilter(ParMesh *m, const double r,
		  FiniteElementCollection *_f1, FiniteElementCollection *_f0,
		  double _beta, double _eta, double _b, double _u, int _i, int _bu) : Filter(m,r,_beta,_eta,_b,_u, _i,_bu),
					       fec1(_f1), fec0(_f0), solver(m->GetComm())
  {
    Initializer();
  }    
  void Initializer(); 
  void Filtering(ParGridFunction *rho, ParGridFunction &r);
  // void FilteringWH(ParGridFunction *rho, ParGridFunction &r);
  void FilMult(const Vector &x, Vector &y);
  ~HelmholtzFilter()     
  {
    delete p1espace; 
    delete p0espace; 
    delete B;
    delete PI;
  }
};


// ***************************************************
// Conic Filter
//
// given $\rho$, compute:
//        $A\rho$ where $A$ is the matrix $(a_{ij})$ with
//        $a_{ij} = K(B_i)|T_j|$ with $K$ the hat function,
//		  and $B_i$ the barycenter of the element T_i
// 	      The computation of A is made in ParConic	
// ***************************************************
class ConicFilter : public Filter
{
public:
  HypreParMatrix *filtermatrix;
  ConicFilter(ParMesh *m, const double r) : 
    Filter(m,r) 
  { 
    ParConic * filter = new ParConic(pmesh,radius); 
    filtermatrix = filter->NewConvolution();
  }

  ConicFilter(ParMesh *m, const double r,
	      double _beta, double _eta, double _b, double _u, int _i, int _bu) : Filter(m,r,_beta,_eta,_b,_u,_i,_bu)  
  { 
    ParConic * filter = new ParConic(pmesh,radius);  
    filtermatrix = filter->NewConvolution();
  }
  void Filtering(ParGridFunction *rho, ParGridFunction &r);
  void FilMult(const Vector &x, Vector &y);
  ~ConicFilter(){ delete filtermatrix;}
};



// ***************************************************
// Thresholding
//
// compute the exact value for the density to make a
// threshold corresponding to obtain a volume equal to
// the exact volume constraint. Also compute a thresholding
// function
// ***************************************************
class Thresholding 
{
public:
  int myid;
  int ndivisiones;
  ParGridFunction *Rhobar;
  ParLinearForm *vol;
  double &medida;
  double threshold;
  double mn;
  double *xabcisa;
  double *rhordenada;
  Vector copia;
  double &Volfrac;
  Thresholding(int _id, int _n, ParGridFunction *_r, 
	       ParLinearForm *_v, double &_m, double &_f,
	       string name1, string name2) : myid(_id),
					     ndivisiones(_n), Rhobar(_r), vol(_v), medida(_m),
					     Volfrac(_f)
  { 
    copia = Vector(Rhobar->Size());
    copia = *Rhobar;
    threshold = compute();
    mn = mnd();
    if (myid == 0)
      {
	cout << "--------------------------------" << endl;
	cout << "-------- Threshold: " << threshold << endl;
	cout << "--------------------------------" << endl;
	cout << "--------    MND:    " << mn*100 << "%" << endl;
	cout << "--------------------------------" << endl; 
	dumpfiles(name1,name2);
      }
  }
  Thresholding(int _id, ParGridFunction *_r, 
	       ParLinearForm *_v, double &_m, double &_f,
	       string name1, string name2, int _n) : myid(_id),
						     Rhobar(_r), vol(_v), medida(_m), Volfrac(_f), 
						     ndivisiones(_n)
  { 
    copia = Vector(Rhobar->Size());
    copia = *Rhobar;
    mn = mnd();
    fakecompute();
    if (myid == 0)
      {
	cout << "--------------------------------" << endl;
	cout << "--------    MND:    " << mn*100 << "%" << endl;
	cout << "--------------------------------" << endl;
	dumpfiles(name1,name2);
      }
  }

  double compute();
  void fakecompute();
  double volume(double d);
  double volumes(double d);
  double mnd();
  double bisection(double &a, double &b);
  void dumpfiles(string n1, string n2);
  ~Thresholding()
  {
    delete[] xabcisa;
    delete[] rhordenada;
  }
};




double Elasticity(ParBilinearForm *, ParLinearForm *, ParGridFunction *, Array<int>&, bool assembling = true);

double ElasticityMec(ParBilinearForm *, ParLinearForm *, HypreParVector *, ParGridFunction *, Array<int>&);


// ***************************************************
// Toptimiz Base Class New (for all methods)
//
// initialization of data for different problems
// method: Graphic - initialization of GLVIS graphic
// ***************************************************

class ToptimizBase
{
public:
  ParFiniteElementSpace *fespace;
  ParMesh *mesh;
  ParGridFunction *Rhobar, *Rhohat;
  Filter *filtro;
  bool &visualization;
  CoeffPsip *Psip;
  static socketstream sout;
  mutable int siter;
  int myid;
  int num_procs;
  int n;
  mutable int loopbeta;
  // Base constructor
  ToptimizBase(
	       ParFiniteElementSpace *_space,
	       ParGridFunction *_rhob,
	       Filter *_fil,
	       bool &_visual,
	       CoeffPsip *_psip = NULL,
	       ParGridFunction *_rhoht = NULL) : 
    fespace(_space),
    Rhobar(_rhob),
    filtro(_fil),
    visualization(_visual),
    Psip(_psip),
    Rhohat(_rhoht)
  {
    loopbeta = 0;
    siter = 0;
    mesh = fespace->GetParMesh();
    myid = mesh->GetMyRank();
    num_procs = mesh->GetNRanks();
    Graphic(); 
    n = Rhobar->Size();
  }
  // initialize graphic
  virtual void Graphic();
  void GlVisit(const int &, const double &, string title = "Iteration", string objname = "Objective") const;
  ~ToptimizBase() { } 
};



// ***************************************************
// Class for using Optimality Conditions (base class to 
// share with Compliance and Multiload cases)
//
// ***************************************************
class ToptimizOCBase : public ToptimizBase
{
public:
  ParBilinearForm *a;
  ParLinearForm *vol, *dconres;
  double zeta;
  double neta;
  Array<int> &ess_tdof_list;
  ParGridFunction *Rho;
  double &Mobj;
  double &Volconstant;
  Vector Y;
  Vector D;
  double global_compliance, global_volumen;
  // Constructor without Heaviside Projection
  ToptimizOCBase(
		 ParFiniteElementSpace *_space, 
		 ParBilinearForm *_a,  
		 ParLinearForm *_v,
		 ParLinearForm *_dv,
		 ParGridFunction *_rhob,
		 ParGridFunction *_init,
		 Array<int> &_ess, 
		 Filter *_fil,
		 bool &_visual,  
		 double &_obj, 
		 double &_volc,
		 double _zeta,
		 double _neta,
		 CoeffPsip *_psip = NULL,
		 ParGridFunction * _rhoht = NULL) :
    ToptimizBase(_space, _rhob, _fil, _visual, _psip, _rhoht), 
    a(_a), vol(_v), dconres(_dv), Rho(_init), Mobj(_obj), ess_tdof_list(_ess),
    Volconstant(_volc), zeta(_zeta), neta(_neta)
  {
    D = Vector(n);
    Y = Vector(n); 
    if (!filtro->boolheaviside)
      filtro->FilMult(*dconres,D);
  }

  ~ToptimizOCBase(){ }
			
  // evaluation of compliance, volume y derivative for OC
  virtual void OCevaluation()=0;
  // iteration of Optimality conditions
  void OCiteration(int &m, double &tol);
};	



// ***************************************************
// Class for using Optimality Conditions (Compliance case)
//
// ***************************************************
class ToptimizOC : public ToptimizOCBase
{
public:
  ParLinearForm *b, *der;
  ParGridFunction *Udisp;
  // Constructor without Heaviside Projection
  ToptimizOC(
	     ParFiniteElementSpace *_space,
	     ParBilinearForm *_a, 
	     ParLinearForm *_b, 
	     ParLinearForm *_d, 
	     ParLinearForm *_v,
	     ParLinearForm *_dv,
	     ParGridFunction *_rhob,
	     ParGridFunction *_init,
	     Array<int> &_ess, 
	     Filter *_fil,
	     bool &_visual,  
	     ParGridFunction *_u, 
	     double &_obj, 
	     double &_volc,
	     double _zeta,
	     double _neta,
	     CoeffPsip *_psip = NULL,
	     ParGridFunction * _rhoht = NULL) :
    ToptimizOCBase(_space, _a, _v, _dv, _rhob, _init, _ess, _fil, _visual, _obj, _volc, _zeta, _neta, _psip, _rhoht),
    b(_b), der(_d), Udisp(_u){ }
  
  ~ToptimizOC(){ }
  
  // evaluation of compliance, volume y derivative for OC
  void OCevaluation();
};	



// ***************************************************
// Class for using Optimality Conditions (Multiload case)
//
// ***************************************************
class ToptimizOCML : public ToptimizOCBase
{
public:
  ParLinearForm *b[n_f], *der[n_f];
  ParGridFunction *Udisp[n_f];
  // Constructor without Heaviside Projection
  ToptimizOCML(
	       ParFiniteElementSpace *_space, 
	       ParGridFunction *_rhob,
	       ParGridFunction *_u[n_f], 
	       ParBilinearForm *_a, 
	       ParLinearForm *_b[n_f], 
	       ParLinearForm *_d[n_f], 
	       ParLinearForm *_v,
	       ParLinearForm *_dv,
	       double &_obj, 
	       double &_volc,
	       Array<int> &_ess,
	       ParGridFunction* _init,
	       Filter *_fil,
	       bool &_visual,  
	       double _zeta,
	       double _neta,
	       CoeffPsip *_psip = NULL,
	       ParGridFunction * _rhoht = NULL) :
    ToptimizOCBase(_space, _a, _v, _dv, _rhob, _init, _ess, _fil, _visual, _obj, _volc, _zeta, _neta,  _psip, _rhoht)
  {
    for (int i=0;i<n_f;i++)
      {
	b[i] = _b[i];
	der[i] = _d[i];
	Udisp[i] = _u[i];
      }
  }

  ~ToptimizOCML(){ }
			
  // evaluation of compliance, volume y derivative for OC
  void OCevaluation();
};	





// ***************************************************
// Base Class for using NLOPT
// abstraction of myconstraint function
//
// ***************************************************
class ToptimizNLOPTBase : public ToptimizBase
{
public:
  ParLinearForm *vol, *dconres;
  Vector D;
  double global_volumen;
  Array<int> &ess_tdof_list;
  int * array_par_n;
  int * disp;
  double &Volconstant;
  ParGridFunction *Rho;

  // Constructor for compliance and volume problems
  ToptimizNLOPTBase(
		    ParFiniteElementSpace *_space,
		    ParLinearForm *_v,
		    ParLinearForm *_dv,
		    ParGridFunction *_rhob,
		    double &_volc,
		    Filter *_fil,
		    Array<int> &_ess,
		    bool &_visual,  
		    int *_ar,
		    int *_dp,
		    CoeffPsip *_psip = NULL,
		    ParGridFunction * _rhoht = NULL) :
    ToptimizBase(_space, _rhob, _fil, _visual, _psip, _rhoht),
    vol(_v), dconres(_dv), ess_tdof_list(_ess), array_par_n(_ar), disp(_dp), Volconstant(_volc)
  {
    Rho = new ParGridFunction(fespace);
    D = Vector(n);
    if (!filtro->boolheaviside)
      {
	filtro->FilMult(*dconres,D);
	D *= Volconstant;
      }	
  }
  ~ToptimizNLOPTBase(){ }
  double myconstraint(const vector<double> &x, vector<double> &grad);
};




// ***************************************************
// Class for using NLOPT (Compliance case, Mechanish and Volume case)
// new version
// ***************************************************
class ToptimizNLOPT : public ToptimizNLOPTBase
{
public:
  ParBilinearForm *a;
  ParLinearForm *b, *der;
  ParGridFunction *Qdisp, *Udisp;
  ParLinearForm *mc;
  HypreParVector *vmec;
  double &Mobj;
  double global_compliance;
  double global_volumen;
  Vector Y;

  // Constructor for compliance and volume problems
  ToptimizNLOPT(
		ParFiniteElementSpace *_space,
		ParBilinearForm *_a, 
		ParLinearForm *_b, 
		ParLinearForm *_d, 
		ParLinearForm *_v,
		ParLinearForm *_dv,
		ParGridFunction *_rhob,
		Array<int> &_ess,
		Filter *_fil,
		bool &_visual,  
		ParGridFunction *_u, 
		double &_obj, 
		double &_volc,
		int *_ar,
		int *_dp,
		CoeffPsip *_psip = NULL,
		ParGridFunction * _rhoht = NULL) :
    ToptimizNLOPTBase(_space, _v, _dv, _rhob, _volc, _fil, _ess, _visual, _ar, _dp, _psip, _rhoht),
    a(_a), b(_b), Udisp(_u), der(_d), Mobj(_obj)
  {
    Init();
  }	
  // Constructor for mechanism problem
  ToptimizNLOPT(
		ParFiniteElementSpace *_space,
		ParBilinearForm *_a, 
		ParLinearForm *_b, 
		ParLinearForm *_d, 
		ParLinearForm *_v,
		ParLinearForm *_dv,
		ParGridFunction *_rhob,
		Array<int> &_ess, 
		Filter *_fil,
		bool &_visual,  
		ParGridFunction *_u, 
		ParLinearForm *_mc,
		ParGridFunction *_qd,
		double &_obj, 
		double &_volc,
		int *_ar,
		int *_dp,
		CoeffPsip *_psip = NULL,
		ParGridFunction * _rhoht = NULL) :
    ToptimizNLOPTBase(_space, _v, _dv, _rhob, _volc, _fil, _ess, _visual, _ar, _dp, _psip, _rhoht),
    a(_a), b(_b), Udisp(_u), der(_d), Mobj(_obj), Qdisp(_qd), mc(_mc)
  {
    Init();
    vmec = mc->ParallelAssemble();
  }

  ~ToptimizNLOPT(){ }
  // objective function and constraint for NLOPT
  void Init();
  double myfunc(const vector<double> &x, vector<double> &grad);
  double myfuncVol(const vector<double> &x, vector<double> &grad);
  double myconstraintVol(const vector<double> &x, vector<double> &grad);
  double myfuncMec(const vector<double> &x, vector<double> &grad);
};	


// ***************************************************
// Class for using NLOPT (MultiLoad case)
// new version
// ***************************************************


class ToptimizNLOPTML : public ToptimizNLOPTBase
{
public:
  ParBilinearForm *a;
  ParLinearForm *b[n_f], *der[n_f];
  ParGridFunction *Udisp[n_f];
  double &Mobj;
  double global_compliance;
  // Constructor for compliance and volume problems
  ToptimizNLOPTML(
		  ParFiniteElementSpace *_space,
		  ParBilinearForm *_a, 
		  ParLinearForm *_b[n_f], 
		  ParLinearForm *_der[n_f], 
		  ParLinearForm *_v,
		  ParLinearForm *_dv,
		  ParGridFunction *_rhob,
		  Array<int> &_ess, 
		  Filter *_fil,
		  bool &_visual,  
		  ParGridFunction *_u[n_f], 
		  double &_obj, 
		  double &_volc,
		  int *_ar,
		  int *_dp,
		  CoeffPsip *_psip = NULL,
		  ParGridFunction * _rhoht = NULL) :
    ToptimizNLOPTBase(_space, _v, _dv, _rhob, _volc, _fil, _ess, _visual, _ar, _dp, _psip, _rhoht), a(_a),
    Mobj(_obj)
  { 
    for (int i=0;i<n_f;i++)
      {
	Udisp[i] = _u[i];
	b[i] = _b[i];
	der[i] = _der[i];
      }
  }
  ~ToptimizNLOPTML(){ }
  double myfunc(const vector<double> &x, vector<double> &grad);
};

// wrapper functions for NLOPT
double wrapper(const vector<double> &x, vector<double> &grad, void *other);
double conswrapper(const vector<double> &x, vector<double> &grad, void *other);



// ****************************************
// OPERATORS for constraints in HIOP
// ****************************************

// Operator to define volume constraint for HIOP

class VolumeConstraint : public Operator
{
public:
  mutable DenseMatrix grad;
  ParFiniteElementSpace *fespace;
  ParLinearForm *vol;
  ParLinearForm *dconres;
  ParGridFunction *Rhobar, *Rhohat;
  Filter *filtro;
  double &Volconstant;
  Vector gD;
  int myid;
  VolumeConstraint(
		   ParFiniteElementSpace *space,
		   ParLinearForm *_vol,
		   ParLinearForm *_dcon,
		   ParGridFunction *_rhob,
		   Filter *_fil,
		   double &Volc,
		   int _my,
		   ParGridFunction *_rhoht = NULL)  :
    Operator(1, space->TrueVSize()),
    grad(1, width), fespace(space), vol(_vol), dconres(_dcon),
    Rhobar(_rhob), filtro(_fil), Volconstant(Volc), myid(_my), Rhohat(_rhoht)
  {
    gD = Vector(Rhobar->Size());
    Vector auxgrad(width);
    if (!filtro->boolheaviside)
      {
	filtro->FilMult(*dconres,gD);
	fespace->Dof_TrueDof_Matrix()->MultTranspose(gD,auxgrad);
	for (int i=0;i<width;i++) grad(0,i) = Volconstant*auxgrad(i);
      }

  }
  ~VolumeConstraint(){ }
  virtual void Mult(const Vector &x, Vector &y) const;
  virtual Operator &GetGradient(const Vector &x) const;
};



// Operator to define compliance constraint for HIOP

class ComplianceConstraint : public Operator
{
public:
  mutable DenseMatrix grad;
  ParFiniteElementSpace *fespace;
  ParBilinearForm *a;
  Array<int> &ess_tdof_list;
  ParLinearForm *b, *der;
  ParGridFunction *Rhobar, *Rhohat;
  ParGridFunction *Udisp;
  Filter *filtro;
  double &Mobj;
  int myid;
  ComplianceConstraint(
		       ParFiniteElementSpace *space,
		       ParBilinearForm *_a,
		       ParLinearForm *_b,
		       ParLinearForm *_der,
		       ParGridFunction *_rhob,
		       ParGridFunction *_u,
		       Array<int> &_ess,
		       Filter *_fil,
		       double &_obj,
		       int _my,
		       ParGridFunction *_rhoht = NULL)  :
    Operator(1, space->TrueVSize()),
    grad(1, width), fespace(space), a(_a), b(_b), der(_der), Rhobar(_rhob),
    ess_tdof_list(_ess), filtro(_fil), Mobj(_obj), myid(_my), Rhohat(_rhoht), Udisp(_u)
  { }
  ~ComplianceConstraint(){ }
  virtual void Mult(const Vector &x, Vector &y) const;
  virtual Operator &GetGradient(const Vector &x) const;
};



// Class for using HIOP compliance

class ToptimizHIOP : public OptimizationProblem, public ToptimizBase
{
public:
  const VolumeConstraint myconstraint;
  Vector d_lo, d_hi;
  ParBilinearForm *a;
  ParLinearForm *b;
  ParLinearForm *der;
  ParGridFunction *Udisp;
  Array<int> &ess_tdof_list;
  double &Mobj;
  mutable double global_compliance;
  ToptimizHIOP(
	       ParFiniteElementSpace *space,
	       ParGridFunction *_rhob,
	       Filter *_fil,
	       ParBilinearForm *_a,
	       ParLinearForm *_b,
	       ParLinearForm *_der,
	       ParLinearForm *_vol,
	       ParLinearForm *_dcon,
	       Array<int> &_ess,
	       ParGridFunction *_u,
	       double &Volc,
	       double &_obj,
	       bool &_visual,
	       CoeffPsip *_psip = NULL,
	       ParGridFunction * _rhoht = NULL) :
    OptimizationProblem(space->TrueVSize(),NULL,NULL),
    ToptimizBase(space, _rhob, _fil, _visual, _psip, _rhoht),
    myconstraint(space, _vol, _dcon, _rhob, _fil, Volc, myid, _rhoht),
    a(_a), b(_b), Udisp(_u), d_lo(1), d_hi(1), der(_der), ess_tdof_list(_ess),
    Mobj(_obj)
  {
    global_compliance = 0;
    D = &myconstraint;
    d_lo(0) = -1.e19;
    d_hi(0) = 1.0;
    SetInequalityConstraint(d_lo,d_hi);
  }
  virtual double CalcObjective(const Vector &x) const;
  virtual void CalcObjectiveGrad(const Vector &x, Vector &grad) const ;
  ~ToptimizHIOP(){ }
};





// Class for using HIOP volume problem

class ToptimizHIOPVol : public OptimizationProblem, public ToptimizBase
{
public:
  const ComplianceConstraint myconstraint;
  Vector d_lo, d_hi;
  ParLinearForm *dconres;
  ParLinearForm *vol;
  double Volconstant;
  mutable double global_volumen;
  Vector grad;
  ToptimizHIOPVol(
		  ParFiniteElementSpace *space,
		  ParGridFunction *_rhob,
		  Array<int> &_ess,
		  Filter *_fil,
		  ParBilinearForm *_a,
		  ParLinearForm *_b,
		  ParLinearForm *_der,
		  ParLinearForm *_vol,
		  ParLinearForm *_dcon,
		  ParGridFunction *_u,
		  double &Volc,
		  double &_obj,
		  bool &_visual,
		  CoeffPsip *_psip = NULL,
		  ParGridFunction * _rhoht = NULL) :
    OptimizationProblem(space->TrueVSize(),NULL,NULL),
    ToptimizBase(space, _rhob, _fil, _visual, _psip,_rhoht),
    myconstraint(space, _a, _b, _der, _rhob, _u, _ess, _fil, _obj, myid, _rhoht),
    d_lo(1), d_hi(1), vol(_vol), dconres(_dcon), Volconstant(Volc)
  {
    global_volumen = 0;
    D = &myconstraint;
    d_lo(0) = -1.e19;
    d_hi(0) = 1.0;
    SetInequalityConstraint(d_lo,d_hi);
    Vector gD(n);
    grad = Vector(space->TrueVSize());
    if (!filtro->boolheaviside)
      {
	filtro->FilMult(*dconres,gD);
	gD *= Volconstant;
	fespace->Dof_TrueDof_Matrix()->MultTranspose(gD,grad);
      }
  }
  virtual double CalcObjective(const Vector &x) const;
  virtual void CalcObjectiveGrad(const Vector &x, Vector &grad) const ;
  ~ToptimizHIOPVol(){ }
};





// Class for using HIOP for mechanics problem

class ToptimizHIOPMec : public OptimizationProblem, public ToptimizBase
{
public:
  const VolumeConstraint myconstraint;
  Vector d_lo, d_hi;
  ParBilinearForm *a;
  ParLinearForm *b;
  ParLinearForm *der;
  ParLinearForm *mc;
  HypreParVector *vmec;
  ParGridFunction *Udisp;
  ParGridFunction *Qdisp;
  Array<int> &ess_tdof_list;
  double &Mobj;
  mutable double global_compliance;
  ToptimizHIOPMec(
		  ParFiniteElementSpace *space,
		  ParGridFunction *_rhob,
		  Filter *_fil,
		  ParBilinearForm *_a,
		  ParLinearForm *_b,
		  ParLinearForm *_der,
		  ParLinearForm *_vol,
		  ParLinearForm *_dcon,
		  ParLinearForm *_mc,
		  Array<int> &_ess,
		  ParGridFunction *_u,
		  ParGridFunction *_qd,
		  double &Volc,
		  double &_obj,
		  bool &_visual,
		  CoeffPsip *_psip = NULL,
		  ParGridFunction * _rhoht = NULL) :
    OptimizationProblem(space->TrueVSize(),NULL,NULL),
    ToptimizBase(space, _rhob, _fil, _visual, _psip,_rhoht),
    myconstraint(space, _vol, _dcon, _rhob, _fil, Volc, myid, _rhoht),
    a(_a), b(_b), mc(_mc), Udisp(_u), Qdisp(_qd), d_lo(1), d_hi(1),
    der(_der), ess_tdof_list(_ess), Mobj(_obj)
  {
    vmec = mc->ParallelAssemble(); 
    global_compliance = 0;
    D = &myconstraint;
    d_lo(0) = -1.e19;
    d_hi(0) = 1.0;
    SetInequalityConstraint(d_lo,d_hi);
  }
  virtual double CalcObjective(const Vector &x) const;
  virtual void CalcObjectiveGrad(const Vector &x, Vector &grad) const ;
  ~ToptimizHIOPMec(){ }
};




// Class for using HIOP multi load

class ToptimizHIOPML : public OptimizationProblem, public ToptimizBase
{
public:
  const VolumeConstraint myconstraint;
  Vector d_lo, d_hi;
  ParBilinearForm *a;
  ParLinearForm *b[n_f], *der[n_f];
  ParGridFunction *Udisp[n_f];
  Array<int> &ess_tdof_list;
  double &Mobj;
  mutable double global_compliance;
  ToptimizHIOPML(
		 ParFiniteElementSpace *space,
		 ParGridFunction *_rhob,
		 Filter *_fil,
		 ParBilinearForm *_a,
		 ParLinearForm *_b[n_f],
		 ParLinearForm *_der[n_f],
		 ParLinearForm *_vol,
		 ParLinearForm *_dcon,
		 Array<int> &_ess,
		 ParGridFunction *_u[n_f],
		 double &Volc,
		 double &_obj,
		 bool &_visual,
		 CoeffPsip *_psip = NULL,
		 ParGridFunction * _rhoht = NULL) :
    OptimizationProblem(space->TrueVSize(),NULL,NULL),
    ToptimizBase(space, _rhob, _fil, _visual, _psip, _rhoht),
    myconstraint(space, _vol, _dcon, _rhob, _fil, Volc, myid, _rhoht),
    a(_a), d_lo(1), d_hi(1), ess_tdof_list(_ess), Mobj(_obj)
  {
    for (int i=0;i<n_f;i++)
      {
	Udisp[i] = _u[i];
	b[i] = _b[i];
	der[i] = _der[i];
      }
    global_compliance = 0;
    D = &myconstraint;
    d_lo(0) = -1.e19;
    d_hi(0) = 1.0;
    SetInequalityConstraint(d_lo,d_hi);
  }
  virtual double CalcObjective(const Vector &x) const;
  virtual void CalcObjectiveGrad(const Vector &x, Vector &grad) const ;
  ~ToptimizHIOPML(){ }
};




// Class for using MMA (PETSC) compliance

#include "MMA.h"
class ToptimizPETSC : public ToptimizBase
{
public:
  ParGridFunction *Rho;
  ParGridFunction *Cotas;
  ParBilinearForm *a;
  ParLinearForm *b;
  ParLinearForm *der;
  ParLinearForm *vol;
  ParLinearForm *dconres;
  ParGridFunction *Udisp;
  Array<int> &ess_tdof_list;
  double &Mobj;
  double &Volconstant;
  mutable double global_compliance;
  mutable double global_volumen;
  PetscScalar movelim,Xmax;
  int m;
  HypreParVector df0dx,dres;
  Vector res;
  PetscParVector *xini;
  Vec xold;
  PetscParVector *xmax, *xmin, *lb;
  PetscParVector *df, *DFDX;
  Vec *dgdx;
  MMA *mma;
  ToptimizPETSC(
	       ParFiniteElementSpace *space,
	       ParGridFunction *_rhob,
	       Filter *_fil,
	       ParBilinearForm *_a,
	       ParLinearForm *_b,
	       ParLinearForm *_der,
	       ParLinearForm *_vol,
	       ParLinearForm *_dcon,
	       Array<int> &_ess,
	       ParGridFunction *_init,
	       ParGridFunction *_u,
	       ParGridFunction *_bound,
	       double &Volc,
	       double &_obj,
	       bool &_visual,
	       CoeffPsip *_psip = NULL,
	       ParGridFunction * _rhoht = NULL) :
    ToptimizBase(space, _rhob, _fil, _visual, _psip, _rhoht),
    a(_a), b(_b), Udisp(_u), Cotas(_bound), der(_der), vol(_vol),dconres(_dcon),
    ess_tdof_list(_ess), Rho(_init), Mobj(_obj), Volconstant(Volc)
  {
    Initialization();
  }
  void Initialization();
  void Iteration(int &, double &);
  void IterationVol(int &, double &);
  ~ToptimizPETSC()
  {
    delete xmin;
    delete xmax;
    delete df;
    delete DFDX;
    delete dgdx;
    delete mma;    
    MFEMFinalizePetsc();
  }
};

class ToptimizPETSCMec : public ToptimizPETSC
{
  ParLinearForm *mc;
  HypreParVector *vmec;
  ParGridFunction *Qdisp;
public:
  ToptimizPETSCMec(
		   ParFiniteElementSpace *space,
		   ParGridFunction *_rhob,
		   Filter *_fil,
		   ParBilinearForm *_a,
		   ParLinearForm *_b,
		   ParLinearForm *_der,
		   ParLinearForm *_vol,
		   ParLinearForm *_dcon,
		   ParLinearForm *_mc,
		   ParGridFunction *_q,
		   Array<int> &_ess,
		   ParGridFunction *_init,
		   ParGridFunction *_u,
		   ParGridFunction *_bound,
		   double &Volc,
		   double &_obj,
		   bool &_visual,
		   CoeffPsip *_psip = NULL,
		   ParGridFunction * _rhoht = NULL) :
    ToptimizPETSC(space,_rhob,_fil,_a,_b,_der,_vol,_dcon,_ess,_init,_u,_bound,Volc,_obj,_visual,_psip, _rhoht), mc(_mc), Qdisp(_q)
  {
    vmec = mc->ParallelAssemble(); 
  }
  void Iteration(int &, double &);
};




class ToptimizPETSCML : public ToptimizBase
{
public:
  ParBilinearForm *a;
  ParLinearForm *b[n_f], *der[n_f];
  ParGridFunction *Udisp[n_f];
  ParLinearForm *vol;
  ParLinearForm *dconres;
  Array<int> &ess_tdof_list;
  double &Volconstant;
  double &Mobj;
  ParGridFunction *Rho;
  ParGridFunction *Cotas;
  mutable double global_compliance;
  PetscScalar movelim,Xmax;
  int m;
  HypreParVector df0dx,dres;
  Vector res;
  PetscParVector *xini;
  Vec xold;
  PetscParVector *xmax, *xmin, *lb;
  PetscParVector *df, *DFDX;
  Vec *dgdx;
  MMA *mma;
  // Constructor for compliance and volume problems
  ToptimizPETSCML(
		  ParFiniteElementSpace *_space,
		  ParBilinearForm *_a, 
		  ParLinearForm *_b[n_f], 
		  ParLinearForm *_der[n_f], 
		  ParLinearForm *_v,
		  ParLinearForm *_dv,
		  ParGridFunction *_rhob,
		  Array<int> &_ess, 
		  Filter *_fil,
		  ParGridFunction *_init,
		  ParGridFunction *_bound,
		  bool &_visual,  
		  ParGridFunction *_u[n_f], 
		  double &_obj, 
		  double &_volc,
		  CoeffPsip *_psip = NULL,
		  ParGridFunction * _rhoht = NULL) :
    ToptimizBase(_space,  _rhob,  _fil,  _visual,  _psip, _rhoht),
    a(_a),  Mobj(_obj), vol(_v), dconres(_dv), ess_tdof_list(_ess),
    Volconstant(_volc), Cotas(_bound), Rho(_init)
  { 
    for (int i=0;i<n_f;i++)
      {
	Udisp[i] = _u[i];
	b[i] = _b[i];
	der[i] = _der[i];
      }
    Initialization();
  }
  ~ToptimizPETSCML(){ }
  void Initialization();
  void Iteration(int &, double &);
};



#endif
