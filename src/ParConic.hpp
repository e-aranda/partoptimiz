#include "mfem.hpp"
#include <fstream>
#include <iostream>
#include <HYPRE.h>
#include <_hypre_parcsr_mv.h>
#include <bits/stdc++.h> 
#include <???>
#include <vector>

using namespace std;
using namespace mfem;

// necessary to define spdim
#include "constants.hpp"

#ifndef MICONIC
#define MICONIC


// helper functions to print values of vectors and arrays (for debugging)
void Print(int *x, int n, int myid=0, const string &name = "x");
void Print(double *x, int n, int myid=0, const string &name = "x");

// struct to compile the sparse matrix information for offd part
struct miniSparse
{
  int processor;
  int nrow;
  int nnz;
  vector<int> vecI;
  vector<int> vecJ;
  vector<int> col_map;
  vector<double> vecA;
  bool check()
  {
    return (!vecJ.empty());
  }
  void stPrint(int myid)
  {
    cout << "****************************" << endl;
    if (check())
      {				
	cout << "MiniSparse structure in processor: " << myid << " for " << processor << endl;
	cout << "Rows: " << nrow << ", Nonzeros: " << nnz << endl;
	Print(vecI.data(),vecI.size(),myid,"I");
	Print(vecJ.data(),vecJ.size(),myid,"J");
	Print(vecA.data(),vecA.size(),myid,"A");
	Print(col_map.data(),col_map.size(),myid,"col_map");
      }
    else
      cout << "MiniSparse structure in processor: " << myid << " for " << processor << 
	" is NULL" << endl;
    cout << "****************************" << endl;
  }

};

// Class to define the conic filter (HypreParMatrix)
class ParConic 
{
public:
  ParMesh *mesh;
  double radius;
  int myid;
  int num_procs;
  int dim;
  double ep;
  int * sizes; // sizes[i] = numbers of neighbours for processor i
  int * cdisp; // sizes accumative
  int * allneigh;
  vector<int> level0;
  ParConic(ParMesh * _m, const double r) : mesh(_m), radius(r)  
  { 
    ep = radius*radius; //sizeindex auxiliar value: radius square
    myid = mesh->GetMyRank();
    num_procs = mesh->GetNRanks();
    dim = mesh->Dimension();
  }
  ~ParConic() { }
  HypreParMatrix * NewConvolution();

private:
  vector<int> sharedElements();
  vector<int> stripBand(vector<int> const &, vector<int> const &);
  void neighBourhood();
  void sparseTranspose(int , vector<int> const & , vector<int> const & , vector<double> const &,
		       vector<int> &, vector<int> &, vector<double> &);
  void symmeTrization(vector<int> const & , vector<int> const & , vector<double> const & ,
		      vector<int> & , vector<int> & , vector<double> & );
  void reOrder(vector<int> & , vector<int> & , vector<double> & );
  vector<int> extractUnique(vector<int> const &);
  vector<int> permuTation(vector<int> const &);
  void spliTing(vector<int> &, vector<int> &,vector<int> &);
  vector<int> Level(int);
  void computingNeighBours(vector<int> &, vector<int> &);
};




#endif
