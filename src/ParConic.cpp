#include "ParConic.hpp"

// compute the first neighbours of a processor in order and gather for all
void ParConic::neighBourhood()
  {
    // Find neighbours of each submesh
    GroupTopology gt = mesh->gtopo;
    level0 = vector<int>(gt.GetNumNeighbors());
    for (int i=0;i < gt.GetNumNeighbors(); i++)
      level0[i] = gt.GetNeighborRank(i);
    sort(level0.begin(),level0.end()); //order
    // remove myid
    level0.erase(std::remove(level0.begin(), level0.end(), myid), level0.end()); 
    int gn = level0.size();

    // sharing neighbours for all processors
    sizes = new int[num_procs];
    MPI_Allgather(&gn,1,MPI_INT, sizes, 1, MPI_INT,MPI_COMM_WORLD);
    int *disp = new int[num_procs];
    cdisp = new int[num_procs+1];
    disp[0] = 0;
    cdisp[0] = 0;
    for (int i=1;i<num_procs;i++)
    {
      disp[i] = disp[i-1] + sizes[i-1];    
      cdisp[i] = disp[i];    
    }
    cdisp[num_procs] = disp[num_procs-1] + sizes[num_procs-1];
    
    // allneigh has every neighbour of every processor
    allneigh = new int[cdisp[num_procs]];
    
    MPI_Allgatherv(level0.data(),gn,MPI_INT, allneigh, sizes, disp, MPI_INT,MPI_COMM_WORLD);

  }

// given neighbours, compute the neighbours of them up to a certain level
vector<int> ParConic::Level(int level)
{
  if (level <= 0) return level0;
  vector<int> actual = level0;
  vector<int> todos = level0;
  vector<int> nexts, nuevos;
  for (int ind=0; ind< level; ind++)
    {
      for (int i=0; i<actual.size();i++)
	for (int j=cdisp[actual[i]]; j<cdisp[actual[i]+1];j++)
	    nuevos.push_back(allneigh[j]);
    
      // extrac unique elements and ordered. Then eliminate own processor
      nuevos = extractUnique(nuevos);
      nuevos.erase(std::remove(nuevos.begin(), nuevos.end(), myid), nuevos.end());
      nexts.clear();
      // new neighbours
      set_difference(nuevos.begin(),nuevos.end(), todos.begin(),todos.end(),
		     inserter(nexts,nexts.begin()));
      // update actual with new ones and order
      nexts = extractUnique(nexts);

      todos.insert( todos.end(), nexts.begin(), nexts.end() );
      todos = extractUnique(todos);
      actual.clear();
      actual = nexts;
      nuevos.clear();
    }

  return nexts;
}

// for each process compute neighbour of neighbour collapsing in a radio distance
void ParConic::computingNeighBours(vector<int> &preneigh, vector<int> &postneigh)
  {
    // compute elements on the shared boundary per process
    vector<int> shared = sharedElements(); // elements of the boundary of processor subdomain
    int ssize = shared.size();
    // computing barycenters of shared elements
    double (*BaryCenters)[spdim] = new double[ssize][spdim];

    Vector barycenter;
    // DenseMatrix A;
    for (int k=0; k<ssize; k++)
    {
      // mesh->GetPointMatrix(shared[k],A);
      // A.GetRowSums(barycenter);
      // barycenter /= A.Size();
      mesh->GetElementCenter(k,barycenter);
      for (int j=0;j<spdim;j++)
        BaryCenters[k][j] = barycenter.Elem(j);
    }

    MPI_Status status;
 
    // for each process compute the first level neighbours 
    int level = 1;
    vector<int> news = Level(level);
    bool totaltrue = true;
    while (!news.empty())
      {
	// split in pre and post
	vector<int> pre, post;
	spliTing(news,pre,post);

	// pass data between processors (not considering last processor because it has no neighbours)
	for (int k=num_procs-1;k>=0; k--)
	  {
	    bool ret = false;
	    if (myid == k)
	      {
		for (int count=0; count < pre.size(); count++)
		  {
		    // send data from processor to pre neighbour
		    MPI_Send(&ssize,1,MPI_INT,pre[count],30,MPI_COMM_WORLD);
		    MPI_Send(BaryCenters,ssize*spdim,MPI_DOUBLE,pre[count],31,MPI_COMM_WORLD);
		  }
	      }
	    else
	      {
		for (int count=0; count < post.size(); count++)
		  {
		    if (post[count] == k)
		      {
			// receive data and compute from post neighbour
			int offnp; // number of columns of the strip
			MPI_Recv(&offnp,1, MPI_INT,post[count],30,MPI_COMM_WORLD,&status);
			double (*RecvBaryCenter)[spdim] = new double[offnp][spdim];
			MPI_Recv(RecvBaryCenter,offnp*spdim, MPI_DOUBLE,post[count],31,MPI_COMM_WORLD,&status);

			// check distance between barycenters
			double dist;
			for (int i=0; i<ssize && (!ret); i++)
			  {               
			    // fixed i-th barycenter of the boundary of my process
			    Eigen::Map<Eigen::Matrix<double, spdim, 1> > bari(BaryCenters[i]);
			    // loop by all barycenters of the neighbour
			    for (int j=0; j<offnp && (!ret); j++)
			      {
				Eigen::Map<Eigen::Matrix<double, spdim, 1> > barj(RecvBaryCenter[j]);
				// compute squared distance between baricenters
				dist = (bari-barj).squaredNorm();
				if (dist < ep)  // barycenter(j) close to barycenter(i)      
				  {
				    ret = true;
				    postneigh.push_back(k);
				  }
			      }
			  }
			delete[] RecvBaryCenter;
		      }
		  }
	      }
	  }
	news = Level(++level);
      }
    delete[] BaryCenters;

    postneigh  = extractUnique(postneigh);
    // preokupas creation: passing okupas information back
    int fake = 1;
    MPI_Request myRequest;
    
    for (int i=0; i<postneigh.size(); i++)
      MPI_Isend(&fake, 1, MPI_INT, postneigh[i], 103, MPI_COMM_WORLD, &myRequest);
    MPI_Barrier(MPI_COMM_WORLD);

    for (int p=0; p<num_procs; p++)
      if (myid != p)
	{
	  MPI_Status status;
	  int flag = 0;
	  MPI_Iprobe(p, 103, MPI_COMM_WORLD, &flag, &status);
	  if (flag)
	    preneigh.push_back(p);
	}

  }


// Compute indices of elements in a shared boundary between processors
vector<int> ParConic::sharedElements()
  {
    // necessary to have the data
    mesh->ExchangeFaceNbrData();
    int nfaces = mesh->GetNSharedFaces();
    // compute elements in the shared boundary
    vector<int> sharedelements;
    int el1_id, el2_id;
    for (int i = 0; i < nfaces; i++)
    {
      mesh->GetFaceElements(mesh->GetSharedFace(i), &el1_id, &el2_id);
      sharedelements.push_back(el1_id);
    }
    // make unique and ordered
    vector<int> shared = extractUnique(sharedelements);
    return shared;
  }


// locate all elements in a band of radius around shared boundary processors
// based in previous computation of filtering in a processor (only look for those
// appearing in I,J vectors)
vector<int> ParConic::stripBand(vector<int> const &I, vector<int> const &J)
  {
    // get elements on the boundary between processors
    vector<int> shared = sharedElements();

    // compute elements in the strip
    vector<int> strip;
    for (int i=0;i<shared.size();i++)     
      strip.insert(strip.end(),J.begin() + I[shared[i]], J.begin() + I[shared[i]+1]);
    
    // make unique and ordered
    vector<int> stripband =  extractUnique(strip);

    return stripband;
  }



// computes the transpose of a rectangular sparse matrix
// entry: number of columns of the entry matrix and sparse vectors
// output: sparse vectors of transpose
void ParConic::sparseTranspose(int ncol, vector<int> const & vecI, vector<int> const & vecJ, vector<double> const & vecA,
         vector<int> & oI, vector<int> & oJ, vector<double> & oA)
  {
    // number of input rows and nonzeros
    int nrow = vecI.size() - 1; 
    int nnz = vecI[nrow];
    if (vecJ.size() != nnz || vecA.size() != nnz)
    {
      cout << "Wrong dimensions of vectors" << endl;
      MPI_Finalize();
      exit(0);
    }

    // dimensions of output vectors
    oJ.resize(nnz);
    oA.resize(nnz); 
    oI.resize(ncol+1);     

    // auxiliar vector
    vector<int> numelem(ncol);

    // Compute number of element in each row for the output matrix
    for (int k = 0; k < nrow; k++)
      for (int i = vecI[k]; i< vecI[k+1]; i++)
        numelem[vecJ[i]] += 1;

    // Build oI
    oI[0] = 0;
    for (int k = 0; k < ncol; k++)
      oI[k+1] = oI[k] + numelem[k];

    // Build oJ, oA
    vector<int> offsets(oI); // index of first unwritten element per row
    for (int i = 0; i < nrow; i++)
      for (int k = vecI[i]; k < vecI[i+1]; k++)
      {
        int col = vecJ[k];
        int index = offsets[col];
        oJ[index] = i;
        oA[index] = vecA[k];
        offsets[col] += 1;
      }

  }

// Symmetrization of a square lower triangular sparse matrix
// entry: sparse square lower triangular matrix
// output symmetric sparse matrix
void ParConic::symmeTrization(vector<int> const & vecI, vector<int> const & vecJ, vector<double> const & vecA,
         vector<int> & oI, vector<int> & oJ, vector<double> & oA)
  {  
    // counter gives the number of new element we need in each column
    // first: counter actual extra diagonal elements
    int np = vecI.size()-1;
    int nnz = vecI[np];
    vector<int> counter(np);
    for (int i=0; i<np; i++)
    {
      counter[i] = vecI[i+1] - vecI[i]; // actual number of element in row i
      // remove diagonal element (already counted) if exists. 
      // It assumes vecJ is ordered !!!
      if( vecJ[vecI[i+1]-1]==i ) counter[i] -= 1; 
    }
    // second: add previous elements to the corresponding column
    for (int i=0; i<nnz ;i++)
      counter[vecJ[i]] += 1;

    // build oI
    oI.resize(np+1);     
    oI[0]=0;
    for (int i=0; i <np ; i++)
      oI[i+1] = oI[i] + counter[i];

    int new_nnz = oI[np];
    oJ.resize(new_nnz);
    oA.resize(new_nnz); 

    // compute pointer where assign new values
    vector<int> pointer(np);
    for (int i=0; i< np; i++)
      pointer[i] = oI[i] + (vecI[i+1]- vecI[i]);

    // set the actual index recollocated
    int point;
    for (int i=0; i< np; i++)
      for (int j=0;j<vecI[i+1]-vecI[i];++j)
      {
         oJ[oI[i]+j] = vecJ[vecI[i]+j];
         oA[oI[i]+j] = vecA[vecI[i]+j];
         // set the new ones    
         if (vecJ[vecI[i]+j] == i)
            continue; // in case is diagonal element
         else
         {
            point = pointer[vecJ[vecI[i]+j]]++;
            oJ[point] = i;                 
            oA[point] = vecA[vecI[i]+j];
         }
      }         
  }

// Hypre wants the diagonal entry to come first.
void ParConic::reOrder(vector<int> & oI, vector<int> & oJ, vector<double> & oA)
  {
    double tmp_data;
    int np = oI.size()-1;
    for (int i=0; i < np; i++)
    {
      int first_col = oI[i];
      for (int j = first_col; j < oI[i+1]; j++)
          if (oJ[j] == i)
          {
              // Swap the column indices
              oJ[j] = oJ[first_col];
              oJ[first_col] = i;

              // Swap the data
              tmp_data = oA[j];
              oA[j] = oA[first_col];
              oA[first_col] = tmp_data;
              break;
          }
    }

  }

// extract all unique elements in order of appereance and order
vector<int> ParConic::extractUnique(vector<int> const &arr)
  {
    vector<int> newv;  
    if (!arr.size()) return newv;
    unordered_set<int> s; 
    // traverse the input array 
    for (auto i=0; i<arr.size(); i++) 
        if (s.find(arr[i])==s.end()) 
        {
            s.insert(arr[i]); 
            newv.push_back(arr[i]);
        }
    sort(newv.begin(),newv.end());

    return newv;
  }



// given a sorted vector of indices, (not necessary consecutive)
// get the inverse permutation:
// e.g. [3,5,8] -> [-1,-1,-1,0,-1,1,-1,-1,2]
  vector<int> ParConic::permuTation(vector<int> const &arr)
  {
    if (!arr.size()) return arr;
    // we need the maximun index
    int maximo = arr[arr.size()-1];
    
    vector<int>perm(maximo+1,-1);
    for (int i=0;i<arr.size();i++)
      perm[arr[i]] = i;
    return perm;
  }

// split a vector of ordered indices (of processors) by myid
void ParConic::spliTing(vector<int> & neighs, vector<int> & pre,vector<int> & post)
  {
    vector<int>::iterator up,low;
    up = upper_bound(neighs.begin(),neighs.end(), myid); 
    low = lower_bound(neighs.begin(),neighs.end(), myid); 
    pre.insert(pre.begin(),neighs.begin(),low);
    post.insert(post.begin(),up,neighs.end());
  }


// *************************************************
// 
HypreParMatrix * ParConic::NewConvolution()
  {
    // STEP 0: initializing some parameters
    
    double Inicio;
    Inicio = MPI_Wtime();
    // dimension and number of points      
    int spacedim = mesh->SpaceDimension();
    int np = mesh->GetNE(); // local number of points

    // check if dimension is correct and radius positive
    if (spacedim != spdim || radius <=0.)
    {
      if (myid == 0)
        cout << "Upppss... Something is wrong" << endl;
      MPI_Finalize();
      exit(0);
    }

    int global_np = mesh->GetGlobalNE(); // global number of points

    // vector of dimension of mesh in each process 
    // vector of displacements (offset) (the same in all processors)
    int *array_psizes = new int[num_procs];
    MPI_Allgather(&np,1,MPI_INT, array_psizes, 1, MPI_INT,MPI_COMM_WORLD);
    int *disp = new int[num_procs];
    disp[0] = 0;
    for (int i=1;i<num_procs;i++)
      disp[i] = disp[i-1] + array_psizes[i-1];    
    
    // STEP 1
    // for each processor computes which processors have to be considered
    // for computing matrix elemens: distributed in preneighbour and postneighbour

    // level 0 neighbours
    neighBourhood();
    vector<int> preneighbour, postneighbour;
    spliTing(level0,preneighbour,postneighbour);

    // level + neighbours of neighbours ... 
    vector<int> pre0,post0;
    computingNeighBours(pre0,post0);
   
    if (post0.size())
      {
    	postneighbour.insert(postneighbour.end(),post0.begin(),post0.end());
    	postneighbour =  extractUnique(postneighbour);
      }
    if (pre0.size())
      {
    	preneighbour.insert(preneighbour.end(),pre0.begin(),pre0.end());
    	preneighbour = extractUnique(preneighbour);
      }
    
    // STEP 2
    // Building barycenters and measures in each processor
    double * Measure = new double[np];
    double (*BaryCenters)[spdim] = new double[np][spdim];

    Vector barycenter;
    // DenseMatrix A;
    for (int k=0; k<np; k++)
    {
      
      //      mesh->GetPointMatrix(k,A);
      // A.GetRowSums(barycenter);
      // barycenter /= A.Size();
      mesh->GetElementCenter(k,barycenter);
      for (int j=0;j<spdim;j++)
        BaryCenters[k][j] = barycenter.Elem(j);
      Measure[k] = mesh->GetElementVolume(k);
    }


    // STEP 3: Diagonal part: check between the elements of the same processor
    // includes look for distances and simmetrization
    /////////////////////////////////
    // Building diagonal part
    // Look for distances in the processor -- diag (diag_i,diag_J,diag_A)
    vector<int> diag_i(np+1);
    vector<int> diag_J;
    vector<double> diag_A;

    int ll=0;
    double dist,valor;
          
    // building lower part of the matrix
    // loop by rows
    Vector bari,barj;
    for (int i=0; i<np;i++)
    { 
      diag_i[i]=ll;
      // fixed i-th barycenter
      Eigen::Map<Eigen::Matrix<double, spdim, 1> > bari(BaryCenters[i]);
      // loop by columns (lower part only)
      for (int j=0; j<=i; j++)
      {
        Eigen::Map<Eigen::Matrix<double, spdim, 1> > barj(BaryCenters[j]);
         // compute squared distance between baricenters
        dist = (bari-barj).squaredNorm();
        if (dist < ep)
        {  // barycenter(j) close to barycenter(i)          
          valor = Measure[j] * max(0.,1-sqrt(dist)/radius);
          if (valor > 1.e-14)
          {
            // introduce value
            ++ll;
            diag_J.push_back(j);
            diag_A.push_back(valor);  
          }
        }
      }
    }
    diag_i[np]=ll;

    vector<int> new_diag_i,new_diag_J;
    vector<double> new_diag_A;
    // Symmetrization of diagonal part 
    symmeTrization(diag_i,diag_J,diag_A,new_diag_i,new_diag_J,new_diag_A);
    // put diagonal element first (hypre wants)
    reOrder(new_diag_i,new_diag_J,new_diag_A);

   // clean vectors
    diag_i.clear();
    diag_J.clear();
    diag_A.clear();

    // number of diagonal nonzeros
    int diag_nnz = new_diag_J.size();

    // STEP 4: Compute strips around border for computing distances between different processors
    // Compute strip around the border 
    vector<int> strip = stripBand(new_diag_i,new_diag_J);
    int stripsize = strip.size();

    // Create BaryCenters and Measures of the strip to pass to the other processors
    double (*StripBaryCenters)[spdim] = new double[stripsize][spdim];
    double *StripMeasure = new double[stripsize];
    for (int k=0; k<stripsize; k++)
    {
      for (int j=0;j<spdim;j++)
        StripBaryCenters[k][j] = BaryCenters[strip[k]][j];
      StripMeasure[k] = Measure[strip[k]];
    }

    delete[] BaryCenters;
    delete[] Measure;
   
    
    // STEP 5: Extra diagonal part: computing distances between different processors
    /////////////////////////////////
    // Offd part (in post neighbourghs) Upper part
    // Look for distance out the processor -- offd

    // Take Barycenters from postneighbour
    MPI_Status status;
    bool left = true, right = true;
    miniSparse *offpost = new miniSparse[postneighbour.size()];

    // Post neighbouhrs send barycenters to its parent, to compute
    // distances: that is, each processor send data to pre-neighbouhrs
    for (int k=num_procs-1; k>=0; k--)
    {
      if (myid == k)
      {
        for (int count=0; count < preneighbour.size(); count++)
        {
          // send data from processor to pre neighbour
          MPI_Send(&stripsize,1,MPI_INT,preneighbour[count],20,MPI_COMM_WORLD);
          MPI_Send(strip.data(),stripsize,MPI_INT,preneighbour[count],21,MPI_COMM_WORLD);
          MPI_Send(StripBaryCenters,spdim*stripsize,MPI_DOUBLE,preneighbour[count],22,MPI_COMM_WORLD);
          MPI_Send(StripMeasure,stripsize,MPI_DOUBLE,preneighbour[count],23,MPI_COMM_WORLD);
        }
      }
      else
      {
       for (int count=0; count < postneighbour.size(); count++)
       {
          if (postneighbour[count] == k)
          {
            // receive data and compute from post neighbour
            int offnp; // number of columns of the strip
            MPI_Recv(&offnp,1, MPI_INT,postneighbour[count],20,MPI_COMM_WORLD,&status);
            vector<int> recvstrip(offnp);
            MPI_Recv(recvstrip.data(),offnp, MPI_INT,postneighbour[count],21,MPI_COMM_WORLD,&status);
            double (*RecvBaryCenter)[spdim] = new double[offnp][spdim];
            double * RecvMeasure = new double[offnp];
            MPI_Recv(RecvBaryCenter,spdim*offnp, MPI_DOUBLE,postneighbour[count],22,MPI_COMM_WORLD,&status);
            MPI_Recv(RecvMeasure,offnp, MPI_DOUBLE,postneighbour[count],23,MPI_COMM_WORLD,&status);

            // building offd part of the matrix
            // loop by rows
            int offd_nnz = 0;
            int ll = 0;
            vector<int> off_i(np+1),offJ;
            vector<double> offA;
            Vector bari,barj;
            int index = 0;
            // Print(strip.data(),stripsize,myid);
            // Print(recvstrip.data(),offnp,myid);

            for (int i=0; i<np;i++)
            { 
              off_i[i] = ll;
              if (i == strip[index]) // only for elements in strip
              { 
                // fixed i-th barycenter of the strip
                Eigen::Map<Eigen::Matrix<double, spdim, 1> > bari(StripBaryCenters[index]);
                index++;
                // loop by all columns of the neighbour
                for (int j=0; j<offnp; j++)
                {
                  Eigen::Map<Eigen::Matrix<double, spdim, 1> > barj(RecvBaryCenter[j]);
                   // compute squared distance between baricenters
                  dist = (bari-barj).squaredNorm();
                  if (dist < ep)
                  {  // barycenter(j) close to barycenter(i)      
                    valor = RecvMeasure[j] * max(0.,1-sqrt(dist)/radius);
                    if (valor > 1.e-14)
                    {
                      // introduce value
                      ++ll;
                      offd_nnz++; 
                      offJ.push_back(recvstrip[j]);
                      offA.push_back(valor);  
                    }
                  }
                }
              }
            }
            off_i[np] = ll;
            
            // adapt col_map (the index column in each processor starts at disp[proc])
            vector<int> col_map(offd_nnz);
            for (int i=0;i<offd_nnz;i++)  
              col_map[i] = offJ[i] + disp[postneighbour[count]];

            // insert partial matrices in an array of struct
            offpost[count] = 
                {postneighbour[count],np,offd_nnz,off_i,offJ,col_map,offA};
          }
       }
      }
    }

    delete[] StripBaryCenters;
    delete[] StripMeasure;
    // Offd part (in pre neighbourghs) Lower part. These are the
    // transpose of the postneighbouhrs
    miniSparse *offpre = new miniSparse[preneighbour.size()];

    for (int k=0; k<num_procs; k++)
    {
      if (myid == k)
      {
        for (int count = 0; count < postneighbour.size(); count++)
        {
          if (offpost[count].check())
          {
            MPI_Send(&offpost[count].nrow,1,MPI_INT,postneighbour[count],10,MPI_COMM_WORLD);
            MPI_Send(&offpost[count].nnz,1,MPI_INT,postneighbour[count],12,MPI_COMM_WORLD);
            MPI_Send(offpost[count].vecI.data(),offpost[count].nrow+1,MPI_INT,postneighbour[count],13,MPI_COMM_WORLD);
            MPI_Send(offpost[count].vecJ.data(),offpost[count].nnz,MPI_INT,postneighbour[count],14,MPI_COMM_WORLD);
            MPI_Send(offpost[count].vecA.data(),offpost[count].nnz,MPI_DOUBLE,postneighbour[count],15,MPI_COMM_WORLD);
          }
          else // null sparse matrix: sending 0
          {
            int nul = 0;
            MPI_Send(&nul,1,MPI_INT,postneighbour[count],10,MPI_COMM_WORLD);
          }
        }
      }
      else 
      {
        // send struct with sparse matrix from processor to post neighbour
        for (int count=0; count < preneighbour.size(); count++)
        {
          if (preneighbour[count] == k)
          { 
            // recieved data from pre neighbour
            int rnrow;
            MPI_Recv(&rnrow,1,MPI_INT,preneighbour[count],10,MPI_COMM_WORLD,&status);
            if (rnrow)
            {
              int rnnz;
              MPI_Recv(&rnnz,1,MPI_INT,preneighbour[count],12,MPI_COMM_WORLD,&status);
              vector<int> oI(rnrow+1);
              MPI_Recv(oI.data(),rnrow+1,MPI_INT,preneighbour[count],13,MPI_COMM_WORLD,&status);
              vector<int> oJ(rnnz);
              vector<double> oA(rnnz);
              MPI_Recv(oJ.data(),rnnz,MPI_INT,preneighbour[count],14,MPI_COMM_WORLD,&status);
              MPI_Recv(oA.data(),rnnz,MPI_DOUBLE,preneighbour[count],15,MPI_COMM_WORLD,&status);
              

              vector<int> t_oI, t_oJ;
              vector<double> t_oA;
              // creating transpose sparse matrices
              sparseTranspose(np,oI,oJ,oA,t_oI,t_oJ,t_oA);

              // adapt col_map
              vector<int> col_map(rnnz);
              for (int i=0;i<rnnz;i++)  
                col_map[i] = t_oJ[i] + disp[preneighbour[count]];

              // save in struct
              offpre[count] = {preneighbour[count],array_psizes[preneighbour[count]],rnnz,t_oI,t_oJ,col_map,t_oA};
            }
            else
            { 
              // saving dummy sparse matrix
              vector<int> I,J,col;
              vector<double> A;
              offpre[count] = {preneighbour[count],0,0,I,J,col,A};
            }
          }
        }
      }
    }

    // put structs in one struct array: total
    int totalsize = preneighbour.size() + postneighbour.size();
    // delete the fake structs
    for (int i=0;i<preneighbour.size();i++)
      if (!offpre[i].check()) totalsize -= 1;
    for (int i=0;i<postneighbour.size();i++)
      if (!offpost[i].check()) totalsize -= 1;

    miniSparse *total = new miniSparse[totalsize];
    int index = 0;
    for (int i=0;i<preneighbour.size();i++)
      if (offpre[i].check()) total[index++] = offpre[i];
    for (int i=0;i<postneighbour.size();i++)
      if (offpost[i].check()) total[index++] = offpost[i];


    // STEP 6: Collecting all information and putting together
    /////////////////////////////////
    // preparing final data
    // vecI has to be sum
    // col_map has to be concatenate, unique and order in col_map_offd
    // vecJ has to be concatenate, ordered and assign to indices of col_map_offd
    // vecA has to be concatenate and order
    // sizeindex counts the number of element in each column by processor
    
    vector<int> finalI(np+1,0);
    vector<int> finalJ,col_map_offd,realJ;
    vector<double> finalA;
    int num_shared_cols = 0;
    {
      vector<double> offdA;
      vector<int> col_offd;
      vector<int> sizeindex(totalsize+1,0);
      for (int i=0;i<totalsize;i++)
      {
        sizeindex[i+1] = total[i].vecJ.size() + sizeindex[i];
        // concatenate col_map
        col_offd.insert(col_offd.end(),total[i].col_map.begin(),total[i].col_map.end());
        // concatenate A
        offdA.insert(offdA.end(),total[i].vecA.begin(),total[i].vecA.end());
      }

      // columns of offd, unique and in order
      col_map_offd = extractUnique(col_offd);
      num_shared_cols = col_map_offd.size();
      if (num_shared_cols)
      {

        // invert col_map_offd
        vector<int> perm;
        perm = permuTation(col_map_offd);
        // sum vecI and reorder col_offd (J) and A
        // by rows
        for (int j=0; j<np;j++)
          for (int i=0;i<totalsize;i++)
          {
            finalI[j+1] += total[i].vecI[j+1];
            for (int k=total[i].vecI[j]; k<total[i].vecI[j+1]; k++)
            {

              finalJ.push_back(col_offd[k+sizeindex[i]]);
              finalA.push_back(offdA[k+sizeindex[i]]);
            }
          }

        // assign indices of col_map to J
        realJ.resize(finalJ.size());
        // invert finalJ
        for (int i=0;i<finalJ.size();i++)
            realJ[i] = perm[finalJ[i]];

      }
    }

    // STEP 7: normalization of final matrix
    // normalization of rows of the matrix
      double suma;
      // diagonal part
      for (int i=0; i<np; i++)
      {
        suma = 0.;
        for (int j=new_diag_i[i];j<new_diag_i[i+1];j++)
            suma += new_diag_A[j];
        for (int j=finalI[i];j<finalI[i+1];j++)
            suma += finalA[j];
        for (int k=new_diag_i[i];k<new_diag_i[i+1];k++)
            new_diag_A[k] /= suma;
        for (int k=finalI[i];k<finalI[i+1];k++)
            finalA[k] /= suma;
      }

    // STEP 8: Creting hyprematrix
    // partitioning
    for (int i=1;i<num_procs;i++)
      array_psizes[i] += array_psizes[i-1];

    HYPRE_Int *row;
    row = new HYPRE_Int[3];
    if (myid == 0)
    {
       row[0] = 0;
       row[1] = array_psizes[0];
    }
    else
    {
      row[0] = array_psizes[myid-1];
      row[1] = array_psizes[myid];
    }
      row[2] = global_np;

    // number of nonzeros extra diags
    int offd_nnz = finalA.size();

    // building hyprematrix
    hypre_ParCSRMatrix * parallel_matrix = hypre_ParCSRMatrixCreate(
        MPI_COMM_WORLD,global_np,global_np,row,row,num_shared_cols,diag_nnz,offd_nnz);

    // In my case, I keep the partitioning arrays
    // hypre_ParCSRMatrixOwnsRowStarts(parallel_matrix) = 0;
    // hypre_ParCSRMatrixOwnsColStarts(parallel_matrix) = 0;

    // Initialize the matrix -- allocates Diag and Offd
    hypre_ParCSRMatrixInitialize(parallel_matrix);

    // Fill col_map_offd and reorder global_idx_marker
    HYPRE_Int * colmap_offd = hypre_ParCSRMatrixColMapOffd(parallel_matrix);
    
    // Copy the matrix data
    hypre_CSRMatrix * diag = hypre_ParCSRMatrixDiag(parallel_matrix),
       * offd = hypre_ParCSRMatrixOffd(parallel_matrix);

    HYPRE_Int * hdiag_i = hypre_CSRMatrixI(diag),
       * hdiag_j = hypre_CSRMatrixJ(diag);

    HYPRE_Complex * diag_data = hypre_CSRMatrixData(diag);

    HYPRE_Int * offd_i = hypre_CSRMatrixI(offd),
       * offd_j = hypre_CSRMatrixJ(offd);
    HYPRE_Complex * offd_data = hypre_CSRMatrixData(offd);

    // copy vectors to arrays
    memcpy(hdiag_i, &new_diag_i[0], sizeof(int)*new_diag_i.size());
    memcpy(hdiag_j, &new_diag_J[0], sizeof(int)*new_diag_J.size());
    memcpy(diag_data, &new_diag_A[0], sizeof(double)*new_diag_A.size());
    memcpy(colmap_offd, &col_map_offd[0], sizeof(int)*col_map_offd.size());
    memcpy(offd_i, &finalI[0], sizeof(int)*finalI.size());
    memcpy(offd_j, &realJ[0], sizeof(int)*realJ.size());
    memcpy(offd_data, &finalA[0], sizeof(double)*finalA.size());

    // define matrix
    hypre_MatvecCommPkgCreate(parallel_matrix);
    HypreParMatrix *B = new HypreParMatrix(parallel_matrix);


    delete[] offpre;
    delete[] total;
    delete[] array_psizes;
    delete[] disp;
    double time2 = MPI_Wtime(); 
    if (myid == 0)
      cout << "Time computing matrix: " << time2 -Inicio << endl;    
    return B;
  }




// for debugging
  
void Print(int *x, int n, int myid, const string &name ) {
    cout << name << ": (proc " << myid << ") " ;
    for (int i=0;i<n;i++) {
        cout << " " << x[i];
    }
    cout << endl;
}


void Print(double *x, int n,int myid, const string &name ) {
    cout << name << ": (proc " << myid << ") " ;
    for (int i=0;i<n;i++) {
        cout << " " << x[i];
    }
    cout << endl;
}


