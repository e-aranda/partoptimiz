// ***************************************************
// Personal class for extend mesh-vtkwriter
//
// methods to be implemented:	
// method (private): VTKdensity - write mesh and density 
// method (private): VTKdisplace - write mesh, density and displacement
// method: VTKConverter - write mesh and density/ mesh, density, stress and displacement
// ***************************************************
class MyMesh : public Mesh
{
public:
  MyMesh(const char *_f) : Mesh(_f) {}
  MyMesh(Mesh *_m) : Mesh(*_m) {}
  void VTKdensity(std::ostream &out, GridFunction *y);
  void VTKdisplace(std::ostream &out, GridFunction *w, GridFunction *U);
  void VTKPardisplace(std::ostream &out, GridFunction *w, GridFunction *U);
  void VTKConverter(std::ostream &out,GridFunction *y);
  void VTKConverter(std::ostream &out,GridFunction *y, GridFunction *w, GridFunction *U);
  void VTKParConverter(std::ostream &out,GridFunction *y, GridFunction *w, GridFunction *U);
  ~MyMesh(){ }
};

// Personal class for extend globalization of ParGridFunction
class MyParGridFunction : public ParGridFunction
{
public:
  MyParGridFunction(ParGridFunction _pm) : ParGridFunction(_pm) {}
  GridFunction *Globalize(Mesh *, const int *);
  ~MyParGridFunction(){ }
};


class VTKwriter
{
  const char *mesh_file;
  const char *output;
  MyParGridFunction *ParRho, *Stress, *Udisp;
  int myid;
  int num_procs;
public:
  VTKwriter(int _id, int _np, const char * _mesh, const char* _output,
	    ParGridFunction *_rho, ParGridFunction *_str, ParGridFunction *_u) :
    myid(_id), num_procs(_np), mesh_file(_mesh), output(_output)
  {
    ParRho = new MyParGridFunction(*_rho);
    Stress = new MyParGridFunction(*_str);
    Udisp = new MyParGridFunction(*_u);
  }
  VTKwriter(int _id, int _np, const char * _mesh, const char* _output,
	    ParGridFunction *_rho) :
    myid(_id), num_procs(_np), mesh_file(_mesh), output(_output)
  {
    ParRho = new MyParGridFunction(*_rho);
  }
  
  void Creator();
  void CreatorML();
  ~VTKwriter()
  {
    // delete ParRho;
    // delete Stress;
    // delete Udisp;
  }
};
