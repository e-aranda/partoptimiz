#include "Parfunctions.hpp"

#ifndef TOPTIMIZ_NLOPT
#define TOPTIMIZ_NLOPT

// return the value of the volume constraint: g(x) for NLOPT (all problems except volume)
double ToptimizNLOPTBase::myconstraint(const vector<double> &x, vector<double> &grad)
{
  vector<double> parx0(n), parg(n);
  MPI_Scatterv(x.data(), array_par_n, disp, MPI_DOUBLE, parx0.data(), n, MPI_DOUBLE, 0, MPI_COMM_WORLD);
  
  // assign vector to Rho
  for (unsigned int i = 0; i < n ; i++)
    Rho->Elem(i) = parx0[i];

 // filter
  $filtering$
  
  // compute volume				
  vol->Update();
  vol->Assemble();

  double global_cons = 1-x[0];
  double cons = (vol->Sum())*Volconstant;
  MPI_Reduce(&cons, &global_cons, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);

  // compute gradient
  $vectorD_NLOPT$
    
  for (int i=0; i < n; i++)
    parg[i] = D(i);

  // collect gradient
  MPI_Gatherv(parg.data(), n, MPI_DOUBLE, grad.data(), array_par_n, disp, MPI_DOUBLE, 0, MPI_COMM_WORLD); 	

  if (myid != 0) grad[0] = -1.;

  global_cons -= 1;
  if (myid == 0)
    cout << "  Restr: "<< global_cons << endl;

  return global_cons;
}


// returns the value of the compliance function for NLOPT
double ToptimizNLOPT::myfunc(const vector<double> &x, vector<double> &grad)
{
  vector<double> parx0(n), parg(n);
  MPI_Scatterv(x.data(), array_par_n, disp, MPI_DOUBLE, parx0.data(), n, MPI_DOUBLE, 0, MPI_COMM_WORLD); 	

  // back door to stop
  if (myid != 0)
    {
      MPI_Status status;
      MPI_Request request;
      int flag=0;
      MPI_Iprobe(0, 100, MPI_COMM_WORLD, &flag, &status);
      if (flag)
	{
	  MPI_Irecv(&flag, 1, MPI_INT, 0, 100, MPI_COMM_WORLD, &request);
	  throw std::runtime_error("forced exit");		  
	}
    }		

  // necessary to stop the last call
  if (grad.empty())
    return 0;

  // assign vector to Rho
  for (unsigned int i = 0; i < n ; i++)
    Rho->Elem(i) = parx0[i];

  // filter
  $filtering$
  $extra$ 
  
  // solving elasticity system
  $density$
  double  compliance = Elasticity(a,b,Udisp,ess_tdof_list);
  
  MPI_Reduce(&compliance, &global_compliance, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
  double obj_value = Mobj*global_compliance;
  
  // computing derivative
  der->Update();
  der->Assemble();
  filtro->FilMult(*der,Y);
  Y *= Mobj;
  for (unsigned int i=0; i < n; i++)
    parg[i] = Y(i);

  // collect gradient
  MPI_Gatherv(parg.data(), n, MPI_DOUBLE, grad.data(), array_par_n, disp, MPI_DOUBLE, 0, MPI_COMM_WORLD); 	

  if (myid != 0)
    { 
      grad[0] = log(x[0]);
      obj_value = x[0];
    }
  else
    cout << "Iteration:  " << siter << "  Obj: " << obj_value << " (" << global_compliance << ") ";
    
  GlVisit(siter,global_compliance,"Iteration:","Compliance");
		
  siter++;

  $betabucle$

  return obj_value;
}




// returns the value of the volume function for NLOPT (Volume problem)
double ToptimizNLOPT::myfuncVol(const vector<double> &x, vector<double> &grad)
{
  vector<double> parx0(n), parg(n);
  MPI_Scatterv(x.data(), array_par_n, disp, MPI_DOUBLE, parx0.data(), n, MPI_DOUBLE, 0, MPI_COMM_WORLD); 	

  // back door to stop
  if (myid != 0)
    {
      MPI_Status status;
      MPI_Request request;
      int flag=0;
      MPI_Iprobe(0, 100, MPI_COMM_WORLD, &flag, &status);
      if (flag)
	{
	  MPI_Irecv(&flag, 1, MPI_INT, 0, 100, MPI_COMM_WORLD, &request);
	  throw std::runtime_error("forced exit");		  
	}
    }		

  // necessary to stop the last call
  if (grad.empty())
    return 0;

  // assign vector to Rho
  for (unsigned int i = 0; i < n ; i++)
    Rho->Elem(i) = parx0[i];

  // filter
  $filtering$
 
  // compute volume				
  vol->Update();
  vol->Assemble();

  double obj_value;
  double value = (vol->Sum())*Volconstant;
  MPI_Reduce(&value, &obj_value, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);

  global_volumen = obj_value;
  
  // compute gradient
  $vectorD_NLOPT$

  for (int i=0; i < n; i++)
    parg[i] = D(i);

  // collect gradient
  MPI_Gatherv(parg.data(), n, MPI_DOUBLE, grad.data(), array_par_n, disp, MPI_DOUBLE, 0, MPI_COMM_WORLD); 	

  if (myid != 0)
    { 
      grad[0] = log(x[0]);
      obj_value = x[0];
    }
  else
      cout << "Iteration:  " << siter << "  Obj: " << obj_value << "  ";

  GlVisit(siter,obj_value,"Iteration:","Volume");

  siter++;
  
  $betabucle$

  return obj_value;
}



// returns the value of the compliance constraint function for NLOPT volume problem
double ToptimizNLOPT::myconstraintVol(const vector<double> &x, vector<double> &grad)
{
  vector<double> parx0(n), parg(n);
  MPI_Scatterv(x.data(), array_par_n, disp, MPI_DOUBLE, parx0.data(), n, MPI_DOUBLE, 0, MPI_COMM_WORLD); 	

  // assign vector to Rho
  for (unsigned int i = 0; i < n ; i++)
    Rho->Elem(i) = parx0[i];

  // filter
  $filtering$
  $extra$
    
  // solving elasticity system
  double  compliance = Elasticity(a,b,Udisp,ess_tdof_list);

  MPI_Reduce(&compliance, &global_compliance, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
  double cons = Mobj*global_compliance - 1; 

  // computing derivative
  der->Update();
  der->Assemble();
  filtro->FilMult(*der,Y);
  Y *= Mobj;
  
  for (unsigned int i=0; i < n; i++)
    parg[i] = Y(i);

  // collect gradient
  MPI_Gatherv(parg.data(), n, MPI_DOUBLE, grad.data(), array_par_n, disp, MPI_DOUBLE, 0, MPI_COMM_WORLD); 	

  if (myid != 0)
    {
      cons = -x[0];
      grad[0] = -1.;
    }
  else
    cout << "  Restr: "<< cons  << " (" << global_compliance << ")" << endl;

  return cons;
}




// returns the value of the mechanism objective function for NLOPT
double ToptimizNLOPT::myfuncMec(const vector<double> &x, vector<double> &grad)
{
  vector<double> parx0(n), parg(n);
  MPI_Scatterv(x.data(), array_par_n, disp, MPI_DOUBLE, parx0.data(), n, MPI_DOUBLE, 0, MPI_COMM_WORLD); 	

  // back door to stop
  if (myid != 0)
    {
      MPI_Status status;
      MPI_Request request;
      int flag=0;
      MPI_Iprobe(0, 100, MPI_COMM_WORLD, &flag, &status);
      if (flag)
	{
	  MPI_Irecv(&flag, 1, MPI_INT, 0, 100, MPI_COMM_WORLD, &request);
	  throw std::runtime_error("forced exit");	
	}
    }		

  // necessary to stop the last call
  if (grad.empty())
    return 0;

  // assign vector to Rho
  for (unsigned int i = 0; i < n ; i++)
    Rho->Elem(i) = parx0[i];

  // filter
  $filtering$
  $extra$

  // solving elasticity system and displacement evaluation
  double compliance = ElasticityMec(a,b,vmec,Udisp,ess_tdof_list);
  MPI_Reduce(&compliance, &global_compliance, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
  double obj_value = Mobj*global_compliance;

  // computing derivative
  // adjoint problem
  compliance = Elasticity(a,mc,Qdisp,ess_tdof_list);

  der->Update();
  der->Assemble();
  filtro->FilMult(*der,Y);
  Y *= Mobj;

  for (unsigned int i=0; i < n; i++)
    parg[i] = Y(i);

  // collect gradient
  MPI_Gatherv(parg.data(), n, MPI_DOUBLE, grad.data(), array_par_n, disp, MPI_DOUBLE, 0, MPI_COMM_WORLD); 	

  if (myid != 0)
    { 
      grad[0] = log(x[0]);
      obj_value = x[0];
    }
  else
    cout << "Iteration:  " << siter << "  Obj: " << obj_value << " (" << global_compliance << ") ";
   
  GlVisit(siter,global_compliance);
		
  siter++;

  $betabucle$
      
  return obj_value;
}




// returns the value of the compliance function for NLOPT multiload problem
double ToptimizNLOPTML::myfunc(const vector<double> &x, vector<double> &grad)
{
  vector<double> parx0(n), parg(n);
  MPI_Scatterv(x.data(), array_par_n, disp, MPI_DOUBLE, parx0.data(), n, MPI_DOUBLE, 0, MPI_COMM_WORLD); 	

  // back door to stop
  if (myid != 0)
    {
      MPI_Status status;
      MPI_Request request;
      int flag=0;
      MPI_Iprobe(0, 100, MPI_COMM_WORLD, &flag, &status);
      if (flag)
	{
	  MPI_Irecv(&flag, 1, MPI_INT, 0, 100, MPI_COMM_WORLD, &request);
	  throw std::runtime_error("forced exit");	
	}
    }		

  // necessary to stop the last call
  if (grad.empty())
    return 0;
  
  // assign vector to Rho
  for (unsigned int i = 0; i < n ; i++)
    Rho->Elem(i) = parx0[i];

  // filter
  $filtering$
  $extra$

  global_compliance = 0.;
  a->Update();
  a->Assemble();
  for (int i=0; i<n_f; i++)
    {
      double compliance = Elasticity(a,b[i],Udisp[i],ess_tdof_list,false);
      double g_compliance;
      MPI_Allreduce(&compliance, &g_compliance, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
      global_compliance += lambdacoef[i]*g_compliance;
    }
  double obj_value = Mobj*global_compliance;

  // computation of derivative
  Vector Ys[n_f];
  for (int i=0; i<n_f; i++)
    {
      Ys[i].SetSize(n);
      der[i]->Update();
      der[i]->Assemble();
      filtro->FilMult(*der[i],Ys[i]);
    }
  
  for (unsigned int i=0; i<n; i++)
    {
      parg[i] = 0.;
      for (int k=0; k<n_f; k++)
	parg[i] += Mobj*lambdacoef[k]*Ys[k](i);
    }
  
  // collect gradient
    MPI_Gatherv(parg.data(), n, MPI_DOUBLE, grad.data(), array_par_n, disp, MPI_DOUBLE, 0, MPI_COMM_WORLD); 	
    
  if (myid != 0)
    { 
      grad[0] = log(x[0]);
      obj_value = x[0];
    }
  else
    cout << "Iteration:  " << siter << "  Obj: " << obj_value << " (" << global_compliance << ") ";

  GlVisit(siter,global_compliance,"Iteration:","Weighted Compliance");
		
  siter++;

  $betabucle$
    
  return obj_value;
}



// wrapper functions for NLOPT
double wrapper(const vector<double> &x, vector<double> &grad, void *other) 
{
  return reinterpret_cast<$problem$>(other)->$funcion$(x, grad);
}
double conswrapper(const vector<double> &x, vector<double> &grad, void *other) 
{
  return reinterpret_cast<$problem$>(other)->$restriccion$(x, grad);
}

#endif
