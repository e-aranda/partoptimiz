#include "Parfunctions.hpp"

#ifndef TOPTIMIZ_OC
#define TOPTIMIZ_OC


// OC iteration	
void ToptimizOCBase::OCiteration(int &maxiter, double &tol)
{
  // recover values of density in vector
  Vector x((Vector) *Rho);
  
  // main iteration of OC
  int k = 0;
  double global_change = 1.;
  double change = 1.;
  double l1, l2, lmid, volumen;
  Vector rl(n);

  while ( k<maxiter && global_change > tol )
    { 
      k++;
      OCevaluation();
      l1=0.; 
      l2=1.e9; 
      for (int i=0; i<n; i++)
	rl(i) = x(i)*pow(max(0.,-Mobj*Y(i)/D(i)),neta);	
      while ( (l2-l1)/(l1+l2) > 1.e-3) 
	{
	  lmid = 0.5*(l1+l2);
	  double plmid = 1./pow(lmid,neta);
	  for (int i=0; i<n; i++)
	    {
	      Rho->Elem(i) = max(0.,
	      max(x(i)*(1-zeta), min(1., min(x(i)*(1+zeta), rl(i)*plmid) ) )
				 );       
	    }

	  $filtering$

	  vol->Update();
	  vol->Assemble();
	  
	  double volumen = vol->Sum();
	  MPI_Allreduce(&volumen, &global_volumen, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
    (global_volumen > Volconstant) ? l1 = lmid : l2 = lmid;
	}
      rl = x - (Vector) *Rho;
      change = rl.Normlinf();
      MPI_Allreduce(&change, &global_change, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD); 
      // update
      x = (Vector) *Rho;

      $betabucle$
    }
  
}




// evaluate compliance and volume in OC
void ToptimizOC::OCevaluation() 
{
  // solving elasticity system
  // filter

  $filtering$
  $extra$

  $density$
  // system
  double  compliance = Elasticity(a,b,Udisp,ess_tdof_list);
  
  MPI_Allreduce(&compliance, &global_compliance, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
  double obj_value = Mobj*global_compliance;

  // volume evaluation
  vol->Update();
  vol->Assemble();
  double volumen = vol->Sum();
  MPI_Allreduce(&volumen, &global_volumen, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
  global_volumen /= Volconstant;

  // computation of derivative
  der->Update();
  der->Assemble();
  filtro->FilMult(*der,Y);

  $vectorD_OC$
  
  // printing iteration results
  if (myid == 0){
    cout << "Iteration:  " << siter << "  Obj: " << obj_value <<
      " (" << global_compliance << ")  Restr: " << global_volumen << endl; 
  }

  GlVisit(siter,global_compliance,"Iteration","Objective");

  siter++;
}	


// evaluate compliance and volume in OC
void ToptimizOCML::OCevaluation() 
{
  // solving elasticity system
  // filter
  $filtering$
  $extra$

  // system
  global_compliance = 0.;
  a->Update();
  a->Assemble();
  for (int i=0; i<n_f; i++)
    {
      double g_compliance;
      double compliance = Elasticity(a,b[i],Udisp[i],ess_tdof_list,false);
      MPI_Allreduce(&compliance, &g_compliance, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
      global_compliance += lambdacoef[i]*g_compliance;
    }
  double obj_value = Mobj*global_compliance;
  
  // volume evaluation
  vol->Update();
  vol->Assemble();
  double volumen = vol->Sum();
  MPI_Allreduce(&volumen, &global_volumen, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
  global_volumen /= Volconstant;

  // computation of derivative
  Vector Ys[n_f];
  for (int i=0; i<n_f; i++)
    {
      Ys[i].SetSize(n);
      der[i]->Update();
      der[i]->Assemble();
      filtro->FilMult(*der[i],Ys[i]);
    }

  Y = 0.;
  for (int i=0; i<n; i++)
    {
      for (int k=0; k<n_f; k++)
	Y(i) += Mobj*lambdacoef[k]*Ys[k](i);
    }
 	
  $vectorD_OC$

  // printing iteration results
  if (myid == 0){
    cout << "Iteration:  " << siter << "  Obj: " << obj_value <<
      " (" << global_compliance << ")  Restr: " << global_volumen << endl; 
  }

  GlVisit(siter,global_compliance,"Iteration","Weighted Compliance");

  siter++;
}	


#endif
