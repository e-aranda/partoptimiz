#include "Parfunctions.hpp"



// ***************************************************
// Methods for ToptimizBase class
// ***************************************************
// Graphic method: define initialization parameters
// for GLVIS graphic
// ***************************************************

socketstream ToptimizBase::sout;
void ToptimizBase::Graphic()
{
  if (visualization)
    {
      char vishost[] = "localhost";
      int  visport   = 19916;
      sout.open(vishost, visport);
      if (myid==0) cout << "Socket creado" << endl;
      sout << "parallel " << num_procs << " " << myid << "\n";
      sout.precision(8);
      sout << "solution\n" << *mesh << *Rhobar;
      sout << "valuerange 0.0 1.0\n";
      sout << "window_title |Density " << '|' ;
      sout << "window_size 800 600\n";
      sout << "view 0 0\n"; // view from top
      sout << "keys jl\n" ; // turn off perspective and light
      sout << "palette 36\n"
	"palette_repeat -1" << endl;
      sout << "zoom 2.0\n" << endl;
    }
}

void ToptimizBase::GlVisit(const int & iter, const double & value, string title, string objname) const
{
  if (visualization && sout.good())
    {
      sout << "parallel " << num_procs << " " << myid << "\n";
      sout << "solution\n" << *mesh << *Rhobar << endl; 
      sout << "plot_caption '" << title << " " << iter <<
	"  " << objname << ": " << value << "'" << endl;    
    }
}


void ToptimizNLOPT::Init()
{
  global_compliance = 0.;
  Y = Vector(n);
}


double Elasticity(ParBilinearForm *a, ParLinearForm *b, ParGridFunction *Udisp, Array<int> &ess_tdof_list, bool assembling) 
{
  if (assembling)
    {
      a->Update();
      a->Assemble();
    }
  // system
  HypreParMatrix A;
  Vector B, U;
  a->FormLinearSystem(ess_tdof_list, *Udisp, *b, A, U, B);
  HyprePCG *hsolver;
  HypreBoomerAMG *amg;		
  // preconditioner
  amg = new HypreBoomerAMG(A);
  amg->SetPrintLevel(0);
  amg->SetSystemsOptions(spdim);
  // solver
  hsolver = new HyprePCG(A);
  hsolver->SetTol(TOL);
  hsolver->SetMaxIter(ITER);
  hsolver->SetPrintLevel(-1);
  hsolver->SetPreconditioner(*amg);
  hsolver->Mult(B, U);
  a->RecoverFEMSolution(U, *b, *Udisp);   
  delete amg;
  delete hsolver;

  // compliance evaluation
  return B * U;
}

double ElasticityMec(ParBilinearForm *a, ParLinearForm *b, HypreParVector *vmec, ParGridFunction *Udisp, Array<int> &ess_tdof_list) 
{
  a->Update();
  a->Assemble();
  // system
  HypreParMatrix A;
  Vector B, U;
  a->FormLinearSystem(ess_tdof_list, *Udisp, *b, A, U, B);
  HyprePCG *hsolver;
  HypreBoomerAMG *amg;		
  // preconditioner
  amg = new HypreBoomerAMG(A);
  amg->SetPrintLevel(0);
  amg->SetSystemsOptions(spdim);
  // solver
  hsolver = new HyprePCG(A);
  hsolver->SetTol(TOL);
  hsolver->SetMaxIter(ITER);
  hsolver->SetPrintLevel(-1);
  hsolver->SetPreconditioner(*amg);
  hsolver->Mult(B, U);
  a->RecoverFEMSolution(U, *b, *Udisp);   
  delete amg;
  delete hsolver;

  // compliance evaluation
  return (*vmec) * U;
}





// ***************************************************
// Methods for SIMPCoeff class
// ***************************************************
// Eval method computes
// $E_0 + (E_1-E_0)\rho^p$
// ***************************************************
double SIMPCoeff::Eval(ElementTransformation &T, 
	const IntegrationPoint &ip)
	{
		  double rho = r.GetValue(T.ElementNo, ip);
	      return E0 + E10*pow(rho,p);
	}


// ***************************************************
// Method for CoeffDerivative class
// ***************************************************
// Eval method computes:
// $-p(E_1-E_0)\rho^{p-1}\tilde\rho sigma(u):sigma(u)$ 	
// ***************************************************
double CoeffDerivative::Eval(ElementTransformation &T, 
	const IntegrationPoint &ip)
	{
		  U.GetVectorGradient(T, eps);  
	      eps.Symmetrize();
	      double div = U.GetDivergence(T);
	      double sig = 2*mu*(eps*eps) + lambda*div*div;
	      double rho = r.GetValue(T.ElementNo, ip);
	      double psip = Psip.Eval(T,ip);
	      sig *= psip*p*E10*pow(rho,p-1);
	      return -sig;
	}


// ***************************************************
// Method for CoeffDerivativeAdj class
// ***************************************************
// ***************************************************
//Eval method computes:
// $-p(E_1-E_0)\rho^{p-1}\tilde\rho sigma(u):sigma(q)$  
// ***************************************************
double CoeffDerivativeAdj::Eval(ElementTransformation &T, 
  const IntegrationPoint &ip)
  {
    U.GetVectorGradient(T, eps);  
    Q.GetVectorGradient(T, qeps);
    eps.Symmetrize();
    qeps.Symmetrize();
    double div = U.GetDivergence(T);
    double divq = Q.GetDivergence(T);
    double sig = 2*mu*(eps*qeps) + lambda*div*divq;
    double rho = r.GetValue(T.ElementNo, ip);
    double psip = Psip.Eval(T,ip);
    sig *= psip*p*E10*pow(rho,p-1);
    return -sig;
  }
// ***************************************************
// Method for CoeffPsip class
// ***************************************************
// Eval method computes
// derivative of Heaviside function
// ***************************************************	
double CoeffPsip::Eval(ElementTransformation &T, 
	const IntegrationPoint &ip)
	{
	      double rhh = r.GetValue(T.ElementNo,ip);
	      double psip = beta*tbi*(1-pow(tanh(beta*(rhh-eta)),2));
	      return psip;
	}


// ***************************************************
// Method for DensityCoeff class
// ***************************************************    
// Eval method computes:
// $\rho \density$
// ***************************************************    
double DensityCoeff::Eval(ElementTransformation &T, 
  const IntegrationPoint &ip)
  {
        double rhh = r.GetValue(T.ElementNo,ip);
        return density*rhh;
  } 


// ***************************************************
// Method for DerDensityCoeff class
// ***************************************************    
// Eval method computes:
// $2\rho \density \tilde\rho$
// ***************************************************    
double DerDensityCoeff::Eval(ElementTransformation &T, 
  const IntegrationPoint &ip)
  {
        double rhh = U.GetValue(T.ElementNo,ip,2);
        double psip = Psip.Eval(T,ip);
        return 2*density*rhh*psip;
  } 


// ***************************************************
// Methods for VonMisesXX classes
// Computation of VonMises Stresses	
// ***************************************************
// 2D Case (plane stress):	
// Eval method computes:
// $\sqrt{\sum_{i=1}^2 \sigma_{ii}^2 + 3\sigma_{12}^2 
//	- \sigma_{11}\sigma_{22}}$
// ********************************************************

double VonMises2D::Eval(ElementTransformation &T, const IntegrationPoint &ip)
   {
      u.GetVectorGradient(T, eps);  // eps = grad(u)
      eps.Symmetrize();             // eps = (1/2)*(grad(u) + grad(u)^t)
      sigma.Diag(l*eps.Trace(), eps.Size()); // sigma = lambda*trace(eps)*I
      sigma.Add(2*m, eps);          // sigma += 2*mu*eps
      double sig=0.;
      for (int i=0;i<2; i++)
         sig += sigma(i,i) * sigma(i,i);  
      sig += 3.* sigma(0,1) * sigma(0,1) - sigma(0,0) * sigma(1,1); 

      return sqrt(sig);
   }

// ***************************************************
// 2Ddp Case (plane strain):	
// Eval method computes:
// $\sqrt{\sum_{i=1}^2 \sigma_{ii}^2 + 3\sigma_{12}^2 
//	- \sigma_{11}\sigma_{22} + (1-1/nu)(\nu(\sigma_{11} + \sigma_{22})^2 }$   
// ********************************************************
double VonMises2Ddp::Eval(ElementTransformation &T, const IntegrationPoint &ip)
   {
      u.GetVectorGradient(T, eps);  // eps = grad(u)
      eps.Symmetrize();             // eps = (1/2)*(grad(u) + grad(u)^t)
      sigma.Diag(l*eps.Trace(), eps.Size()); // sigma = lambda*trace(eps)*I
      sigma.Add(2*m, eps);          // sigma += 2*mu*eps
      double sig=0.;
      for (int i=0;i<2; i++)
         sig += sigma(i,i) * sigma(i,i);  
      sig += 3.* sigma(0,1) * sigma(0,1) - sigma(0,0) * sigma(1,1); 
      sig += pow( nu*(sigma(0,0) + sigma(1,1)),2 )*(1.-1./nu);

      return sqrt(sig);
   }

// ***************************************************
// 3D Case:	
// Eval method computes:
// $\sqrt{\sum_{i=1}^3 \sigma_{ii}^2 + 3\sigma_{12}^2 + 3\sigma_{13}^2
//	+ \sigma_{23}^2 - \sigma_{11}\sigma_{22} -sigma_{11}\sigma_{33} -
//  \sigma_{22}\sigma_{33} }$   
// ********************************************************
double VonMises3D::Eval(ElementTransformation &T, const IntegrationPoint &ip)
   {
      u.GetVectorGradient(T, eps);  // eps = grad(u)
      eps.Symmetrize();             // eps = (1/2)*(grad(u) + grad(u)^t)
      sigma.Diag(l*eps.Trace(), eps.Size()); // sigma = lambda*trace(eps)*I
      sigma.Add(2*m, eps);          // sigma += 2*mu*eps
      double sig=0.;
      int j=0;
      for (int i=0;i<3; i++)
      	{
      	 j = (i+1)%3;
         sig += sigma(i,i) * sigma(i,i);  
         sig += 3.* sigma(i,j) * sigma(i,j) - sigma(i,i) * sigma(j,j); 
        }
      
      return sqrt(sig);
   }


// ***************************************************
// Method for RelaxStress classes
// ***************************************************
// Eval method computes:
// $\rho^q \sigma_VM$ where $\sigma_VM$ is the Von Misses Stress
// ********************************************************
double RelaxStress::Eval(ElementTransformation &T, const IntegrationPoint &ip)
   {
   	double svm = SVM.Eval(T,ip);
   	double res = pow(r.GetValue(T.ElementNo,ip),q)*svm;
   	return res;
   }


// ***************************************************
// Method for Stress classes
// ***************************************************
// Eval method computes:
// $E(\rho) \sigma_VM$ where $\sigma_VM$ is the Von Misses Stress
// and E(\rho) the simp interpolation   
// ********************************************************
double Stress::Eval(ElementTransformation &T, const IntegrationPoint &ip)
   {
   	return r.Eval(T,ip)*SVM.Eval(T,ip);
   }   


// ***************************************************
// Methods for Heaviside class
// ***************************************************
// UpdateConstant method: updates constants for 
// computation when beta changes	
// ***************************************************
void Heaviside::UpdateConstant(double &b)
	{
		beta = b;
		tbi = 1./(tanh(beta*eta) + tanh(beta*(1.-eta)));
		tb = tanh(beta*eta)*tbi;
	}



// ***************************************************
// Methods for Filter class
// ***************************************************
// FilteringWH method: is the same for all filters. 
// Computes filter with Heaviside projection.  
// ***************************************************
void Filter::FilteringWH(ParGridFunction *rho, ParGridFunction &ret)
  {
    // computation with normal filtering
    this->Filtering(rho,ret);

    // Heaviside projection
    for (int i=0;i<ret.Size();i++)
      ret[i] = tb + tbi*tanh(beta*(ret(i)-eta));
  } 



// ***************************************************
// Methods for HelmholtzFilter class
// ***************************************************
// Initializer method: Initialize all necessary matrices
// for computing the filter 
// ***************************************************
void HelmholtzFilter::Initializer()
{
    p1espace = new ParFiniteElementSpace(pmesh,fec1,Ordering::byVDIM);
    p0espace = new ParFiniteElementSpace(pmesh,fec0,Ordering::byVDIM);
    
    // Matrix for solving Helmholz equation
    ParBilinearForm *a = new ParBilinearForm(p1espace);
    ConstantCoefficient E(ep);
    ConstantCoefficient one(1.0);
    a->AddDomainIntegrator(new DiffusionIntegrator(E));
    a->AddDomainIntegrator(new MassIntegrator(one));
    a->Assemble();
    Array<int> ess;
    a->FormSystemMatrix(ess, A);

    // preconditioner for solving A matrix system
    HypreBoomerAMG *amg = new HypreBoomerAMG(A);
    amg->SetPrintLevel(0);
    amg->SetSystemsOptions(spdim);
    
    // settings for solver
    solver.iterative_mode = false;
    solver.SetTol(1e-8);
    solver.SetAbsTol(0.0);
    solver.SetMaxIter(500);
    solver.SetPrintLevel(0);
    solver.SetPreconditioner(*amg);		
    solver.SetOperator(A);
		
    // Interpolation matrix between spaces
    ParDiscreteLinearOperator pi(p1espace,p0espace);
    pi.AddDomainInterpolator(new IdentityInterpolator());
    pi.Assemble();
    pi.Finalize();
    PI = pi.ParallelAssemble();
    
    // Auxiliar matrix B
    ParMixedBilinearForm b(p0espace,p1espace);
    b.AddDomainIntegrator(new MassIntegrator(one));
    b.Assemble();
    b.Finalize();
    B = b.ParallelAssemble();
}

// ***************************************************
// Filtering method: Computes filter without Heaviside
// projections. Computes AU=R and then PI*U
// ***************************************************
void HelmholtzFilter::Filtering(ParGridFunction *rho, ParGridFunction &ret)
{
  // create RHS for Helmholz equation
  GridFunctionCoefficient R(rho);
  ParLinearForm rhs(p1espace);
  rhs.AddDomainIntegrator(new DomainLFIntegrator(R));
  rhs.Assemble();
  HypreParVector *RHS = rhs.ParallelAssemble();
  // Solve system A*U=RHS
  Vector U(RHS->Size());
  solver.Mult(*RHS, U);
  // interpolation from P1 -> P0
  PI->Mult(U,ret);
}


// ***************************************************
// FilMult method: Adjusting filtering computation for
// derivatives. Computes B^T*A^-1*PI^T
// ***************************************************
void HelmholtzFilter::FilMult(const Vector &x, Vector &y)
	{
		// interpolation P0 -> P1
		Vector aux(p1espace->GetTrueVSize());
		PI->MultTranspose(x,aux);
		
		// Solves A*U = aux
		Vector U(p1espace->GetTrueVSize());
    solver.Mult(aux, U);

    // Multiply by B^T
		B->MultTranspose(U,y);
	}



// ***************************************************
// Methods for ConicFilter class
// ***************************************************
// Computes filter without Heaviside
// projections. Computes Ar=R
// ***************************************************
void ConicFilter::Filtering(ParGridFunction *rho, ParGridFunction &ret)
  {
    filtermatrix->Mult(*rho,ret);
  }

// ***************************************************
// FilMult method: Adjusting filtering computation for
// derivatives.
// ***************************************************
void ConicFilter::FilMult(const Vector &x, Vector &y)
  {
    y = 0.0;
    filtermatrix->Mult(x, y);
  }




// ***************************************************
// Methods for Thresholding computation
// ***************************************************
// compute: compute the optimal threshold
// fakecompute: only compute volume
// mnd: compute measure of non discreteness
// volume: compute volume
// volumes: compute normalized volume
// bisection: implement a bisection search to find the threshold
// dumpfiles: generate a file for VTK and LaTeX visualization 
// ********************************************************
double Thresholding::compute()
{
  xabcisa = new double[ndivisiones+1], 
    rhordenada = new double[ndivisiones+1];
  double cpaso = 1./ndivisiones, auxd;
  for (int i = 0; i<=ndivisiones; i++)
    {
      auxd = i*cpaso;
      xabcisa[i] = auxd;
      rhordenada[i] =1./medida*volume(auxd);
    }
  double extinf,extsup, thres=0.;
  for (int i=0; i<ndivisiones; i++)
    {
      if ( (rhordenada[i] >= Volfrac) &&
	   (rhordenada[i+1]< Volfrac) )
	{
	  extinf = xabcisa[i];
	  extsup = xabcisa[i+1];
	  thres = bisection(extinf,extsup);
	  break;
	}
    }
  return thres;
}


void Thresholding::fakecompute()
{
  xabcisa = new double[ndivisiones+1], 
    rhordenada = new double[ndivisiones+1];
  double cpaso = 1./ndivisiones, auxd;
  for (int i = 0; i<=ndivisiones; i++)
    {
      auxd = i*cpaso;
      xabcisa[i] = auxd;
      rhordenada[i] =1./medida*volume(auxd);
    }
}


double Thresholding::mnd()
{	
  ParGridFunction &u = *Rhobar;
  for (int i=0; i<Rhobar->Size(); i++)
    u(i) = 4.*copia(i)*(1.-copia(i));
  vol->Update();
  vol->Assemble();
  double aux = vol->Sum();
  double gaux;
  MPI_Allreduce(&aux, &gaux, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
  return 1./medida * gaux;
}

double Thresholding::volume(double d)
{
  ParGridFunction &u = *Rhobar;
  for (int i=0;i<Rhobar->Size(); i++)
    u(i) = (copia(i) >= d) ? 1. : 0.;
  vol->Update();
  vol->Assemble(); 
  double aux =  vol->Sum();
  double gaux;
  MPI_Allreduce(&aux, &gaux, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
  return gaux;
}

double Thresholding::volumes(double d)
{
  return 1./medida*volume(d) - Volfrac;
}

double Thresholding::bisection(double &a, double &b)
{
  if (volumes(a) < 1.e-4)
    return a;
  int iter = 0;
  double medio = (a+b)/2.;
  while (abs(volumes(medio)) > 1.e-4 and iter<100)
    {
      if (volumes(medio)>0)
	a = medio;
      else
	b = medio;

      medio = (a+b)/2.;
      iter++;
    }
  return medio;
}

void Thresholding::dumpfiles(string n1, string n2)
{	
  
  string doc1 = "# vtk DataFile Version 4.1\n\
Generated by C++\n\
ASCII\n\
DATASET TABLE\n\
ROW_DATA\n";
  {
    ofstream gnu(n1);
    {
      gnu << doc1 << 3*(ndivisiones+1)+1 << endl;
      gnu << "FIELD FieldData 4"<< endl;
      gnu << "X 1 " << ndivisiones+1 << " float" << endl;
      for (int i=0;i<=ndivisiones;i++)
	gnu << xabcisa[i] << "  " ;
      gnu << endl;
      gnu << "Volume 1 " << ndivisiones+1 << " float" << endl; 
      for (int i=0;i<=ndivisiones;i++)
	gnu << rhordenada[i] << "  ";   
      gnu << endl;
      gnu << "Fraction%20Volume 1 " << ndivisiones+1 << " float" << endl; 
      for (int i=0;i<=ndivisiones;i++)
	gnu << Volfrac << "  ";   
      gnu << endl;
      gnu << "Threshold 1 1 float" << endl;
      gnu << threshold << endl;
    }
  }

  {
    ofstream gnu(n2);
    gnu << "x        y1      y2    " << threshold <<  endl;
    for (int i=0;i<=ndivisiones;i++)
      gnu << xabcisa[i] << "  " << rhordenada[i] << "  " << Volfrac << "  " << "nan" << endl;
  }


}	

