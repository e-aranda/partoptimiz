#include "Parfunctions.hpp"

#ifndef TOPTIMIZ_HIOP
#define TOPTIMIZ_HIOP

// return the value of the volume constraint:
void VolumeConstraint::Mult(const Vector &x, Vector &y) const
{
  // assign input x to density
  ParGridFunction *Rho = new ParGridFunction(fespace);
  fespace->GetProlongationMatrix()->Mult(x, *Rho);
  // filter
  $filtering$

  // compute volume
  vol->Update();
  vol->Assemble();
  Vector aux(x.Size());
  fespace->GetProlongationMatrix()->MultTranspose((Vector) *vol, aux);
  double C = aux.Sum()*Volconstant;
  MPI_Allreduce(&C, &y(0), 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);
  // print restriction value
  if (myid == 0) cout << "  Restr: " << y(0) << endl;
  delete Rho;
} 

Operator & VolumeConstraint::GetGradient(const Vector &x) const
{
  $vectorD_HIOP$
  return grad; 
}


// return the value of the compliance constraint:
void ComplianceConstraint::Mult(const Vector &x, Vector &y) const
{
  // assign input x to density
  ParGridFunction *Rho = new ParGridFunction(fespace);
  fespace->GetProlongationMatrix()->Mult(x, *Rho);
  // filter
  $filtering$
  $extra$

  // solve elasticicty system
  double compliance, global_compliance;
  compliance = Elasticity(a,b,Udisp,ess_tdof_list);  
  MPI_Allreduce(&compliance, &global_compliance, 1, MPI_DOUBLE, MPI_SUM,  MPI_COMM_WORLD);
  y(0) = Mobj*global_compliance;
  if (myid == 0)
    cout << "  Restr: "<< y(0) << " (" << global_compliance << ")" << endl;    
  delete Rho;
}

Operator & ComplianceConstraint::GetGradient(const Vector &x) const
{
  der->Update();
  der->Assemble();
  Vector Y(der->Size());
  filtro->FilMult(*der,Y);
  Vector auxgrad(width);
  fespace->Dof_TrueDof_Matrix()->MultTranspose(Y,auxgrad);
  for (int i=0;i<width;i++) grad(0,i) = Mobj*auxgrad(i);
  return grad; 
}



// Compliance objective value
double ToptimizHIOP::CalcObjective(const Vector &x) const
{
  // assign input to density
  ParGridFunction *Rho = new ParGridFunction(fespace);
  fespace->GetProlongationMatrix()->Mult(x,*Rho);
  // filter
  $filtering$
  $extra$

  // solving elasticity problem
  $density$
  double compliance = Elasticity(a,b,Udisp,ess_tdof_list);  
  MPI_Allreduce(&compliance, &global_compliance, 1, MPI_DOUBLE, MPI_SUM,  MPI_COMM_WORLD);

  
  double obj_value = Mobj*global_compliance;
  if (myid == 0)
    cout << "Iteration: " << siter << "  obj: " << obj_value << " (" << global_compliance << ")" ;
  siter++;

  $betabucle$
    
  GlVisit(siter,global_compliance);
  delete Rho;
  return obj_value;
}

// Derivative Compliance Objective
void ToptimizHIOP::CalcObjectiveGrad(const Vector &x, Vector &y) const
{
  der->Update();
  der->Assemble();
  Vector Y(n);
  filtro->FilMult(*der,Y);
  Y *= Mobj;
  fespace->Dof_TrueDof_Matrix()->MultTranspose(Y,y);
}


// Volume objective function
double ToptimizHIOPVol::CalcObjective(const Vector &x) const
{
  // assign input to density
  ParGridFunction *Rho = new ParGridFunction(fespace);
  fespace->GetProlongationMatrix()->Mult(x,*Rho);
  // filter
  $filtering$

  // compute volume
  vol->Update();
  vol->Assemble();
  Vector aux(x.Size());
  fespace->GetProlongationMatrix()->MultTranspose((Vector) *vol, aux);
  double value = aux.Sum()*Volconstant;

  MPI_Allreduce(&value, &global_volumen, 1, MPI_DOUBLE, MPI_SUM, MPI_COMM_WORLD);

  // compute gradient
  if (myid == 0)
    cout << "Iteration: " << siter << "  obj: " << global_volumen << "  ";

  siter++;
  $betabucle$

  GlVisit(siter,global_volumen,"Iteration: ","Volume: ");
  delete Rho;
  return global_volumen;
}


// Derivative Volumen Objective function
void ToptimizHIOPVol::CalcObjectiveGrad(const Vector &x, Vector &y) const
{
  $vectorY_HIOP$
}


// Compliance objective value
double ToptimizHIOPMec::CalcObjective(const Vector &x) const
{
  // assign input to density
  ParGridFunction *Rho = new ParGridFunction(fespace);
  fespace->GetProlongationMatrix()->Mult(x,*Rho);
  // filter
  $filtering$
  $extra$

  // solving elasticity problem
  double compliance = ElasticityMec(a,b,vmec,Udisp,ess_tdof_list);  
  MPI_Allreduce(&compliance, &global_compliance, 1, MPI_DOUBLE, MPI_SUM,  MPI_COMM_WORLD);

  
  double obj_value = Mobj*global_compliance;
  if (myid == 0)
    cout << "Iteration: " << siter << "  obj: " << obj_value << " (" << global_compliance << ")" ;

  siter++;
  $betabucle$
    
  GlVisit(siter,global_compliance);
  delete Rho;
  return obj_value;
}

// Derivative Compliance Objective
void ToptimizHIOPMec::CalcObjectiveGrad(const Vector &x, Vector &y) const
{
  double compliance = Elasticity(a,mc,Qdisp,ess_tdof_list);
  der->Update();
  der->Assemble();
  Vector Y(n);
  filtro->FilMult(*der,Y);
  Y *= Mobj;
  fespace->Dof_TrueDof_Matrix()->MultTranspose(Y,y);
}




// Compliance objective value
double ToptimizHIOPML::CalcObjective(const Vector &x) const
{
  // assign input to density
  ParGridFunction *Rho = new ParGridFunction(fespace);
  fespace->GetProlongationMatrix()->Mult(x,*Rho);
  // filter
  $filtering$
  $extra$
  
  // solving elasticity problem
  global_compliance = 0.;
  for (int i=0; i<n_f; i++)
    {
      double compliance = Elasticity(a,b[i],Udisp[i],ess_tdof_list);
      double g_compliance;
      
      MPI_Allreduce(&compliance, &g_compliance, 1, MPI_DOUBLE, MPI_SUM,  MPI_COMM_WORLD);
      global_compliance += lambdacoef[i]*g_compliance;
    }

  double obj_value = Mobj*global_compliance;
  if (myid == 0)
    cout << "Iteration: " << siter << "  obj: " << obj_value << " (" << global_compliance << ")" ;
  siter++;

  $betabucle$
    
  GlVisit(siter,global_compliance,"Iteration:","Weighted Compliance");
  delete Rho;
  return obj_value;
}

// Derivative Compliance Objective
void ToptimizHIOPML::CalcObjectiveGrad(const Vector &x, Vector &y) const
{
  Vector Ys[n_f];
  for (int i=0;i<n_f;i++)
    {
      der[i]->Update();
      der[i]->Assemble();
      Ys[i].SetSize(n);
      filtro->FilMult(*der[i],Ys[i]);
    }
  Vector Y(n);
  Y = 0.;
  for (int k=1;k<n;k++)
    {
      for (int i=0;i<n_f;i++)
	Y(k) += Mobj*lambdacoef[i]*Ys[i](k);
    }
  fespace->Dof_TrueDof_Matrix()->MultTranspose(Y,y);

}





#endif
